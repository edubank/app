package com.edubank.view.admin

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.MainActivity
import org.hamcrest.core.IsAnything.anything
import org.junit.*

class EditUserDataTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

        @Before
        fun setUp() {
            // Login
            onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
            onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
            onView(withId(R.id.btn_login_login)).perform(click())
            waitForServer()

            // Select "Manage users"
            onView(withId(R.id.userManagerButton)).perform(click())
        }

    // Change First Name - Valid
    @Test
    fun testFirstNameValid() {
        // Add user
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change first name
        onView(withId(R.id.firstNameEdit)).perform(replaceText("Felix"))
        onView(withId(R.id.saveChangesBtn)).perform(click())

        // Check if name has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.firstNameEdit)).check(matches(withText("Felix")))
        pressBack()
    }

    // Change First Name - Invalid
    @Test
    fun testFirstNameInvalid() {
        // Add user
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change first name
        onView(withId(R.id.firstNameEdit)).perform(replaceText(""))
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.emptyFirstNameToast)))
        pressBack()

        // Check if role name changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.firstNameEdit)).check(matches(withText("To")))
        pressBack()
    }

    //Change Last Name - Valid
    @Test
    fun testLastNameValid() {
        // Add user
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change last name
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Müller"))
        onView(withId(R.id.saveChangesBtn)).perform(click())

        // Check if name has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.lastNameEdit)).check(matches(withText("Müller")))
        pressBack()
    }

    //Change Last Name - Invalid
    @Test
    fun testLastNameInvalid() {
        // Add user
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change last name
        onView(withId(R.id.lastNameEdit)).perform(replaceText(""))
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.emptyLastNameToast)))
        pressBack()

        // Check if name has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.lastNameEdit)).check(matches(withText("Edit")))
        pressBack()
    }

    // Change email - valid
    @Test
    fun testEmailValid() {
        // Add user
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change email
        onView(withId(R.id.emailEdit)).perform(replaceText("felix@edubank.de"))
        onView(withId(R.id.saveChangesBtn)).perform(click())

        // Check if email has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.emailEdit)).check(matches(withText("felix@edubank.de")))
        pressBack()
    }

    // Change email - invalid
    @Test
    fun testEmailInvalid() {
        // Add user
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change email
        onView(withId(R.id.emailEdit)).perform(replaceText("tea@"))
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.invalidEmail)))
        pressBack()

        // Check if email has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.emailEdit)).check(matches(withText("")))
        pressBack()
    }

    // Change role of student
    @Test
    fun testChangeStudent() {
        // Add student
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(0).perform(click())
        onView(withId(R.id.addBtn)).perform(click())

        // Select Student
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change role to teacher
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(1).perform(click())
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.changeStudentRoleToast)))

        // Change role to admin
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(2).perform(click())
        Thread.sleep(2000)
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.changeStudentRoleToast)))
        pressBack()

        // Check if role has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.roleSpinner)).check(matches(withSpinnerText("Schüler")))
        pressBack()
    }

    // Change role of teacher
    @Test
    fun testChangeTeacher() {
        // Add teacher
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(1).perform(click())
        onView(withId(R.id.addBtn)).perform(click())


        // Select Teacher
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change role to student
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(0).perform(click())
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.changeToStudentToast)))

        // Change role to admin
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(2).perform(click())
        Thread.sleep(2000)
        onView(withId(R.id.saveChangesBtn)).perform(click())

        // Check if role has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.roleSpinner)).check(matches(withSpinnerText("Administrator")))
        pressBack()
    }

    // Change admin to student
    @Test
    fun testChangeAdmin() {
        // Add admin
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Edit"))
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(2).perform(click())
        onView(withId(R.id.addBtn)).perform(click())


        // Select Admin
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Change role to student
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(0).perform(click())
        onView(withId(R.id.saveChangesBtn)).perform(click())
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.changeToStudentToast)))

        // Change role to teacher
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(1).perform(click())
        waitForServer(2000)
        onView(withId(R.id.saveChangesBtn)).perform(click())

        // Check if role has changed
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.roleSpinner)).check(matches(withSpinnerText("Lehrer")))
        pressBack()
    }

    @After
    fun tearDown() {
        // Select user
        waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        // Delete admin
        onView(withId(R.id.deleteUserBtn)).perform(click())
        // Dialog click confirm
        onView(withId(android.R.id.button1)).perform(click())
    }
}

