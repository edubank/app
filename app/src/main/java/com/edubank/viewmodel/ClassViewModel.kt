package com.edubank.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edubank.model.dto.request.SchoolClassRequestDTO
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.model.retrofitclient.ClassService
import com.edubank.model.retrofitclient.ServiceBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

/**
 * Das ViewModel liefert die Funktionalität, die für die Verwaltung einer Klasse gebraucht werden.
 * Dazu gehört die Kommunkation mit dem Server, um Klassen anzufragen oder zu bearbeiten.
 *
 * @author Team EduBank
 * @property service Dem ViewModel kann ein ClassService übergeben werden, wenn ein spezieller Service gebraucht wird (Andernfalls wird der Default-Service verwendet).
 * @property classes Die Read-Only Klassendaten können durch classes gelesen werden.
 * @see SchoolClassResponseDTO Die Klassendaten des ViewModels werden in einer Liste vom Typ SchoolClassResponseDTO gehalten.
 * @see ClassService Retrofit-Service, der für die Kommunikation mit dem Server verwendet wird.
 */
class ClassViewModel : ViewModel() {

    /**
     * Daten-Container für die Klassen-Daten. Nur _classes kann modifiziert werden.
     * classes macht die Klassen-Daten nach außen sichtbar und enthält die gleichen Daten wie _classes.
     */
    private var _classes : MutableLiveData<List<SchoolClassResponseDTO>> =
        MutableLiveData<List<SchoolClassResponseDTO>>()
    var classes : LiveData<List<SchoolClassResponseDTO>> = _classes
    /**
     * Retrofit-Service für Klassen-Anfragen.
     */
    private var requestService = ServiceBuilder.buildService(ClassService::class.java)

    /**
     * Die setService-Funktion setzt den Class-Service. Wenn setService nicht aufgerufen wird,
     * dann wird der Default-Service gesetzt. Sonst service.
     * Das Initialisieren mit einem spezifischen Service wird für die Tests benötigt.
     */
    fun setService(service : ClassService){
        requestService = service
    }

    /**
     * Die Funktion getAllClasses frägt die Daten aller Klassen an.
     * Die Klassen werden in classes gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält classes eine leere Liste.
     *
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getAllClasses() : Int {
        var responseCode = 0
        val call = requestService.getAllClasses()
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _classes.value = response.body()
            } else {
                _classes.value = mutableListOf()
            }
            Log.d("ClassVM:getAllClasses", response.message())
        } catch (error : Throwable) {
            _classes.value = mutableListOf()
            error.message?.let { Log.e("ClassVM:getAllClasses", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getClass frägt die Daten einer Klasse an.
     * Die Klassen werden in classes gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält classes eine leere Liste.
     *
     * @param classId Die Klassen-ID der Klasse, die angefragt werden sollen.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getClass(classId : Int) : Int {
        var responseCode = 0
        val call = requestService.getClass(classId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _classes.value = listOfNotNull(response.body())
            } else {
                _classes.value = mutableListOf()
            }
            Log.d("ClassVM:getClass", response.message())
        } catch (error : Throwable) {
            _classes.value = mutableListOf()
            error.message?.let { Log.e("ClassVM:getClass", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion updateClass aktualisiert die Klassendaten einer Klasse auf dem Server.
     * Die aktualiserten Klassen werden in classes gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält classes eine leere Liste.
     *
     * @param classId Die Klassen-ID der Klasse, welche aktualisert werden soll.
     * @param name Neuer Name der Klasse.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun updateClass(classId : Int, name : String) : Int {
        var responseCode = 0
        val call = requestService.updateClass(classId, SchoolClassRequestDTO(name))
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _classes.value = listOfNotNull(response.body())
            } else {
                _classes.value = mutableListOf()
            }
            Log.d("ClassVM:updateClass", response.message())
        } catch (error : Throwable) {
            _classes.value = mutableListOf()
            error.message?.let { Log.e("ClassVM:updateClass", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion deleteClass löscht die Daten einer Klasse auf dem Server.
     *
     * @param classId Die Klassen-ID der Klasse, die gelöscht werden sollen.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun deleteClass(classId : Int) : Int {
        var responseCode = 0
        val call = requestService.deleteClass(classId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            Log.d("ClassVM:deleteClass", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("ClassVM:deleteClass", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion createClass erstellt eine Klasse auf dem Server.
     * Die erstellte Klassen wird in classes gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält classes eine leere Liste.
     *
     * @param name Name der Klasse, die erzeugt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun createClass(name : String) : Int {
        var responseCode = 0
        val call = requestService.createClass(SchoolClassRequestDTO(name))
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _classes.value = listOfNotNull(response.body())
            } else {
                _classes.value = mutableListOf()
            }
            Log.d("ClassVM:createClass", response.message())
        } catch (error : Throwable) {
            _classes.value = mutableListOf()
            error.message?.let { Log.e("ClassVM:createClass", it) }
            error.printStackTrace()
        }
        return responseCode
    }
}