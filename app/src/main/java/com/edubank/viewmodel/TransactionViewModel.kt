package com.edubank.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edubank.model.dto.request.EventRequestDTO
import com.edubank.model.dto.request.TransactionRequestDTO
import com.edubank.model.dto.response.TransactionResponseDTO
import com.edubank.model.retrofitclient.ServiceBuilder
import com.edubank.model.retrofitclient.TransactionService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

/**
 * Das ViewModel liefert die Funktionalität, die für die Verwaltung eines Transaction gebraucht werden.
 * Dazu gehört die Kommunkation mit dem Server, um Transaktionen anzufragen oder zu bearbeiten.
 * Weiter können Events erstellt werden.
 *
 * @author Team EduBank
 * @property service Dem ViewModel kann ein TransactionService übergeben werden, wenn ein spezieller Service gebraucht wird (Andernfalls wird der Default-Service verwendet).
 * @property transactions Die Read-Only Transaktionsdaten können durch transactions gelesen werden.
 * @see TransactionResponseDTO Die Transaktionsdaten des ViewModels werden in einer Liste vom Typ TransactionResponseDTO gehalten.
 * @see TransactionService Retrofit-Service, der für die Kommunikation mit dem Server verwendet wird.
 */
class TransactionViewModel : ViewModel() {

    /**
     * Daten-Container für die Transaktions-Daten. Nur _transactions kann modifiziert werden.
     * transactions macht die Transaktions-Daten nach außen sichtbar
     * und enthält die gleichen Daten wie _transactions.
     */
    private var _transactions : MutableLiveData<List<TransactionResponseDTO>> =
        MutableLiveData<List<TransactionResponseDTO>>()
    var transactions : LiveData<List<TransactionResponseDTO>> = _transactions
    /**
     * Retrofit-Service für Transaktions-Anfragen.
     */
    private var requestService = ServiceBuilder.buildService(TransactionService::class.java)

    /**
     * Die setService-Funktion setzt den Transaction-Service. Wenn setService nicht aufgerufen wird,
     * dann wird der Default-Service gesetzt. Sonst service.
     * Das Initialisieren mit einem spezifischen Service wird für die Tests benötigt.
     */
    fun setService(service : TransactionService){
        requestService = service
    }

    /**
     * Die Funktion getTransactions frägt die Transaktionen eines Kontos an.
     * Die Transaktionen werden in transactions gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält transactions eine leere Liste.
     *
     * @param accountId Die ID des Kontos, dessen Transaktionsverlauf angefragt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getTransactions(accountId : Int) : Int {
        var responseCode = 0
        val call = requestService.getTransactions(accountId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _transactions.value = response.body()
            } else {
                _transactions.value = mutableListOf()
            }
            Log.d("TransactVM:getTransact", response.message())
        } catch (error : Throwable) {
            _transactions.value = mutableListOf()
            error.message?.let { Log.e("TransactVM:getTransact", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getTransactionById frägt eine Transaktion an.
     * Die Transaktion wird in transactions gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält transactions eine leere Liste.
     *
     * @param transactionId Die ID der Transaktion, die angefragt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getTransactionById(transactionId : Int) : Int  {
        var responseCode = 0
        val call = requestService.getTransactionById(transactionId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _transactions.value = listOfNotNull(response.body())
            } else {
                _transactions.value = mutableListOf()
            }
            Log.d("TransactVM:getTransById", response.message())
        } catch (error : Throwable) {
            _transactions.value = mutableListOf()
            error.message?.let { Log.e("TransactVM:getTransById", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion executeTransaction führt eine Transaktion auf dem Server durch.
     * Die Transaktion wird in transactions gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält transactions eine leere Liste.
     *
     * @param accountId Die Id des Kontos auf dem die Transaktion durchgefüurt werden soll.
     * @param transaction Transaktion die durchgeführt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     * @see TransactionRequestDTO Datenobjekt für die REST-Anfrage.
     */
    suspend fun executeTransaction(accountId : Int, transaction : TransactionRequestDTO) : Int {
        var responseCode = 0
        val call = requestService.executeTransaction(accountId, transaction)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _transactions.value = listOfNotNull(response.body())
            } else {
                _transactions.value = mutableListOf()
            }
            Log.d("TransactVM:execTransact", response.message())
        } catch (error : Throwable) {
            _transactions.value = mutableListOf()
            error.message?.let { Log.e("TransactVM:execTransact", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion addEvent führt ein Event auf dem Server durch.
     * Die Transaktionen des Events werden in transactions gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält transactions eine leere Liste.
     *
     * @param event Event das durchgeführt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun addEvent(event : EventRequestDTO) : Int {
        var responseCode = 0
        val call = requestService.addEvent(event)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _transactions.value = response.body()
            } else {
                _transactions.value = mutableListOf()
            }
            Log.d("TransactVM:addEvent", response.message())
        } catch (error : Throwable) {
            _transactions.value = mutableListOf()
            error.message?.let { Log.e("TransactVM:addEvent", it) }
            error.printStackTrace()
        }
        return responseCode
    }
}