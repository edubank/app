package com.edubank.model.retrofitclient

import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.model.dto.response.AccountTransactionsResponseDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Scnittstellen-Beschreibung für den Retrofit-Service AccountService.
 * Die Schnittstelle beschreibt die Konto-Anfragen an den Server.
 *
 * @author Team EduBank
 */
interface AccountService {

    /**
     * Die Funktion getSubjectAccounts frägt die Konten eines Faches an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subjectId Die Fach-ID, dessen Konten angefragt werden sollen.
     * @return Call-Objekt mit einer Konto-Daten-Liste.
     */
    @GET("accounts")
    fun getSubjectAccounts(@Query("subject_id") subjectId : Int) : Call<List<AccountResponseDTO>>

    /**
     * Die Funktion getStudentAccounts frägt die Konten des Benutzers mit der ID userId an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param studentId Die Benutzer-ID der Konten, die angefragt werden sollen.
     * @return Call-Objekt mit einer Konto-Daten-Liste.
     */
    @GET("accounts")
    fun getStudentAccounts(@Query("student_id") studentId : Int) : Call<List<AccountResponseDTO>>

    /**
     * Die Funktion getAccount frägt das Konto mit der ID accountId an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param accountId Die Konto-ID des Kontos, welches angefragt werden soll.
     * @return Call-Objekt mit den Konto-Daten.
     */
    @GET("accounts/{id}")
    fun getAccount(@Path("id") accountId : Int) : Call<AccountTransactionsResponseDTO>

}