package com.edubank.view.admin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.ClassItemBinding
import com.edubank.model.dto.response.SchoolClassResponseDTO

/**
 * Adapter für die Anzeige von Klassen im ClassOverview-Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see ClassOverview
 * @property classes Liste mit alle Klassen die in der RecyclerView gezeigt werden
 * @author Team EduBank
 */
class ClassAdapter(private val listener: OnItemClickListener):
    RecyclerView.Adapter<ClassAdapter.ClassViewHolder>(){

   var classes = mutableListOf<SchoolClassResponseDTO>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ClassViewHolder {

        // Erstellt eine neue Ansicht, die die Benutzeroberfläche des Listenelements definiert
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.class_item, viewGroup, false)

        return ClassViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager)
     */
    override fun onBindViewHolder(holder: ClassViewHolder, position: Int) {

        // Hole das Element an dieser Position
        // und ersetze den Inhalt der Ansicht durch dieses Element
        val currentItem = classes[position]
        holder.className.text = currentItem.name
    }

    /**
     * Gibt die Anzahl der Klassen zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = classes.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class ClassViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding = ClassItemBinding.bind(itemView)
        val className = binding.className

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onItemClick(position)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}