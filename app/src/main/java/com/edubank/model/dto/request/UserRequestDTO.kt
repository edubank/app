package com.edubank.model.dto.request

/**
 * UserRequestDTO ist eine Daten-Klasse für Benutzer-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property role Benutzertyp des Benutzers
 * @property firstName Vorname des Benutzers
 * @property lastName Nachname des Benutzers
 * @property email Email-Adresse des Benutzers
 */
data class UserRequestDTO(
    val role : String,
    val firstName : String,
    val lastName : String,
    val email : String
)

