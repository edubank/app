package com.edubank.view.shared

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.AccountItemBinding
import com.edubank.model.dto.response.AccountResponseDTO


/**
 * Adapter für die Anzeige von Accounts im Subject Fragment. Weitere Erklärungen zu den einzelnen Methoden finden
 * sich in der Dokumentation von [RecyclerView.Adapter].
 *
 * @property subject Das [Subject], dessen Accounts der Adapter verwaltet.
 * @author Team EduBank
 */
class AccountAdapter(private val subject: Subject) : RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    /**
     * Die Liste der Accounts, die von diesem Adapter angezeigt werden.
     */
    var accounts = mutableListOf<AccountResponseDTO>()

    /**
     * Der interne ViewHolder, der die AccountItems hält und repräsentiert.
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val binding = AccountItemBinding.bind(view)

        val lblName = binding.lblAccountItemName
        val lblBalance = binding.lblAccountItemBalance

        init {
            view.setOnClickListener { // opens the account this view holds
                val position = bindingAdapterPosition
                subject.showMemberAccount(accounts[position].id)
            }
        }

    }

    /**
     * Erstellt einen neuen ViewHolder für ein AccountItem.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.account_item, parent, false)

        return ViewHolder(view)
    }

    /**
     * Initialisiert den gegebenen [ViewHolder] mit dem Account an der gegebenen position.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val account = accounts[position]
        val firstName = account.student.firstName
        val lastName = account.student.lastName
        val balance = account.balance

        holder.apply {
            lblName.text = String.format("$firstName $lastName")
            lblBalance.text = String.format("$balance €")

            when {
                balance < 0 -> {
                    lblName.setTextColor(Color.parseColor("#ca5e5e"))
                    lblBalance.setTextColor(Color.parseColor("#ca5e5e"))
                }
                else        -> {
                    lblName.setTextColor(Color.parseColor("#6fbf72"))
                    lblBalance.setTextColor(Color.parseColor("#6fbf72"))
                }
            }
        }
    }

    /**
     * Gibt die Anzahl der Accounts zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = accounts.size
}