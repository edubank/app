package com.edubank.view.admin

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DisplayClassesTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        // Login
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        TestData.waitForServer()
    }

    @Test
    fun testClassesOverviewIsDisplayed() {

        // View elements are shown
        onView(withId(R.id.welcomeMessage)).check(matches(isDisplayed()))
        onView(withId(R.id.adminName)).check(matches(isDisplayed()))
        onView(withId(R.id.classManagerButton)).check(matches(isDisplayed()))
        onView(withId(R.id.userManagerButton)).check(matches(isDisplayed()))
        onView(withId(R.id.classes)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerClassView)).check(matches(isDisplayed()))

    }
}

