package com.edubank.view.shared

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData.Companion.adminEmail
import com.edubank.view.TestData.Companion.adminName
import com.edubank.view.TestData.Companion.adminNewEmail
import com.edubank.view.TestData.Companion.adminNewPassword
import com.edubank.view.TestData.Companion.adminPassword
import com.edubank.view.TestData.Companion.adminUsername
import com.edubank.view.TestData.Companion.waitForServer
import org.junit.Rule
import org.junit.Test

/**
 * Beinhaltet allgemeine UI-Tests für Settings.
 *
 * @see Settings
 * @author Team EduBank
 */
class SettingsTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Admin ein und navigiert zum Fragment Settings über das Menü.
     */
    private fun setUpAdmin() { //Login
        onView(withId(R.id.txt_login_username)).perform(replaceText(adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()

        // Click on Settings
        onView(withId(R.id.settings)).perform(click())

        waitForServer()
    }

    /**
     * Prüft, ob das Fragment für einen Admin richtig angezeigt wird.
     */
    @Test
    fun test_admin_fragmentIsDisplayedCorrectly() {
        setUpAdmin()

        onView(withId(R.id.lbl_settings_title)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_settings_userInfo)).check(matches(withText("$adminName  ($adminUsername)")))
        onView(withId(R.id.lbl_settings_password)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_settings_newPassword)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_settings_newPasswordRepeat)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_settings_email)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_settings_email)).check(matches((withText(adminEmail))))
        onView(withId(R.id.btn_settings_save)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_settings_deleteAccount)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Admin seine Einstellungen ändern kann und diese richtig übernommen werden.
     */
    @Test
    fun test_admin_changeSettings() {
        setUpAdmin()

        //change settings
        onView(withId(R.id.txt_settings_newPassword)).perform(replaceText(adminNewPassword))
        onView(withId(R.id.txt_settings_newPasswordRepeat)).perform(replaceText(adminNewPassword))
        onView(withId(R.id.txt_settings_email)).perform(replaceText(adminNewEmail))
        onView(withId(R.id.btn_settings_save)).perform(click())

        waitForServer()

        //logout
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        waitForServer(2000)

        //login
        onView(withId(R.id.txt_login_username)).perform(replaceText(adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(adminNewPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()

        // click on settings
        onView(withId(R.id.settings)).perform(click())

        waitForServer()

        //check email
        onView(withId(R.id.txt_settings_email)).check(matches((withText(adminNewEmail))))

        //reset email
        onView(withId(R.id.txt_settings_newPassword)).perform(replaceText(adminPassword))
        onView(withId(R.id.txt_settings_newPasswordRepeat)).perform(replaceText(adminPassword))
        onView(withId(R.id.txt_settings_email)).perform(replaceText(adminEmail))
        onView(withId(R.id.btn_settings_save)).perform(click())
    }
}