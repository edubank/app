package com.edubank.view.shared

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.edubank.R
import com.edubank.databinding.SettingsFragmentBinding
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet den Settings-Screen und stellt die benötigten Funktionalitäten zur Verfügung.
 *
 * @author Team EduBank
 */
class Settings(private val initialLogin: Boolean = false) : Fragment() {

    private val userVM: UserViewModel by activityViewModels()
    private lateinit var binding: SettingsFragmentBinding


    /**
     * Die Methode wird aufgerufen, wenn das Fragment neu gebaut wird. Hier werden die Listener für die UI-Elemente
     * festgelegt und die View an das ViewModel per DataBinding angebunden.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val dialogBuilder: AlertDialog.Builder? = activity?.let { AlertDialog.Builder(it) }

        binding = SettingsFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userVM
        binding.lifecycleOwner = this


        loadUserData()

        binding.btnSettingsSave.setOnClickListener {
            updateUserData()
        }

        binding.btnSettingsDeleteAccount.setOnClickListener {
            dialogBuilder!!.setMessage(getString(R.string.deleteAccountConfirmation))
                .setTitle(R.string.deleteDialogTitle)
                .setPositiveButton(R.string.yesIAm) { _, _ -> deleteUser() }
                .setNegativeButton(R.string.cancel) { _, _ -> }.create().show()
        }

        if (initialLogin) {
            dialogBuilder!!.setMessage(R.string.initialLoginMessage).setTitle(R.string.initialLogin)
                .setPositiveButton(android.R.string.ok) { _, _ -> }.create()?.show()
        }


        return binding.root
    }

    /**
     * Die Methode verwendet das UserViewModel, um die Einstellungen des Benutzers zu ändern. Dazu gehört die
     * E-Mail-Adresse und das Passwort.
     */
    private fun updateUserData() {
        val newPassword = binding.txtSettingsNewPassword.text.toString()
        val newPasswordRep = binding.txtSettingsNewPasswordRepeat.text.toString()
        val email = binding.txtSettingsEmail.text.toString()

        if (!MainActivity.isEmail(email)) {
            Snackbar.make(requireView(), getString(R.string.invalidEmail), Snackbar.LENGTH_LONG).show()
            return
        } else if (newPassword != newPasswordRep) {
            Snackbar.make(requireView(), getString(R.string.passwordsDoNotMatch), Snackbar.LENGTH_LONG).show()
            return
        }

        if (initialLogin) {
            if (newPassword.isBlank() || email.isBlank()) {
                Snackbar.make(requireView(), getString(R.string.invalidData), Snackbar.LENGTH_LONG).show()
                return
            } else {
                lifecycleScope.launch {
                    userVM.setInitialData(email, newPassword)
                    requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, Login())
                        .commit()
                }

            }
        } else {
            lifecycleScope.launch {
                userVM.updateSettings(email, newPassword)
            }
        }

        Snackbar.make(requireView(), getString(R.string.successfulDataSave), Snackbar.LENGTH_LONG).show()
    }

    /**
     * Die Methode initialisiert die Felder der Einstellungen mit den richtigen Werten (Name, Benutzername, Email)
     */
    private fun loadUserData() {
        lifecycleScope.launch {
            userVM.getMyUser()
            val user =
                userVM.users.value?.get(0) ?: return@launch // abort if null (should never happen)
            val userInfo = "${user.firstName} ${user.lastName} (${user.username})"

            binding.lblSettingsUserInfo.text = userInfo
            binding.txtSettingsEmail.setText(user.email)

            if (user.role == UserRole.ROLE_ADMIN) binding.btnSettingsDeleteAccount.visibility =
                View.VISIBLE
        }
    }

    /**
     * Die Methode löscht den aktuellen User, wenn möglich.
     */
    private fun deleteUser() {
        lifecycleScope.launch {
            userVM.getMyUser()
            val user = userVM.users.value?.get(0) ?: return@launch // abort if null (should never happen)

            val serverResponse = userVM.deleteUser(user.id)

            if (serverResponse == 400) {
                Snackbar.make(requireView(), getString(R.string.cantDeleteUser), Snackbar.LENGTH_LONG).show()
            } else {
                (requireActivity() as MainActivity).toggleMenuVisibility()
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, Login())
                    .commit()
            }
        }
    }
}