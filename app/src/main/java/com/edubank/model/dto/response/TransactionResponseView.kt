package com.edubank.model.dto.response

import java.util.*

/**
 * TransactionResponseView ist eine Daten-Klasse für Transaktions-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id der Transaktion
 * @property title Titel der Transaktion
 * @property amount Betrag der Transaktion
 * @property initiator Initiator der Transaktion
 * @property createdAt Zeitpunkt der Durchführung der Transaktion
 */
data class TransactionResponseView (
    val id : Int,
    val title : String,
    val amount : Double,
    val initiator : UserResponseDTO,
    val createdAt : Date
)
