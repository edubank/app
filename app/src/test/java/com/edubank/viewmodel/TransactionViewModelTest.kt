package com.edubank.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.edubank.model.dto.request.EventRequestDTO
import com.edubank.model.dto.request.TransactionRequestDTO
import com.edubank.model.dto.response.TransactionResponseDTO
import com.edubank.model.retrofitclient.TransactionService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class TransactionViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var transactionVM : TransactionViewModel
    @get:Rule
    private val mockWebServer = MockWebServer()
    private val transactionAPI by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(TransactionService::class.java)
    }
    private val httpClient = OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .build()
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Before
    fun setup() {
        mockWebServer.start()
        transactionVM = TransactionViewModel()
        transactionVM.setService(transactionAPI)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getTransactions_success() = runBlocking {
        val jsonData = getDataObject("transaction_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(transactionVM.getTransactions(1) == 200)
        assertTrue(transactionVM.transactions.value == gson.fromJson<List<TransactionResponseDTO>>(
            jsonData, object : TypeToken<List<TransactionResponseDTO>>() {}.type))
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getTransactions_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(transactionVM.getTransactions(1) == 400)
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getTransactionById_success() = runBlocking {
        val jsonData = getDataObject("transaction_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(transactionVM.getTransactionById(1) == 200)
        assertTrue(transactionVM.transactions.value == listOf(gson.fromJson<TransactionResponseDTO>(
            jsonData, object : TypeToken<TransactionResponseDTO>() {}.type)))
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getTransactionById_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(transactionVM.getTransactionById(1) == 400)
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun executeTransaction_success() = runBlocking {
        val jsonData = getDataObject("transaction_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val response = gson.fromJson<TransactionResponseDTO>(
            jsonData, object : TypeToken<TransactionResponseDTO>() {}.type)
        val request = TransactionRequestDTO(response.title, response.amount)

        assertTrue(transactionVM.executeTransaction(1, request) == 200)
        assertTrue(transactionVM.transactions.value == listOf(response))
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun executeTransaction_failed() = runBlocking {
        val jsonData = getDataObject("transaction_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))
        val response = gson.fromJson<TransactionResponseDTO>(
            jsonData, object : TypeToken<TransactionResponseDTO>() {}.type)
        val request = TransactionRequestDTO(response.title, response.amount)

        assertTrue(transactionVM.executeTransaction(1, request) == 400)
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun addEvent_success() = runBlocking {
        val jsonData = getDataObject("event_response.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val response = gson.fromJson<List<TransactionResponseDTO>>(
            jsonData, object : TypeToken<List<TransactionResponseDTO>>() {}.type)

        assertTrue(transactionVM.addEvent(EventRequestDTO("Buch Faust", listOf(2, 3), 6.99)) == 200)
        assertTrue(transactionVM.transactions.value == response)
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 2)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun addEvent_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(transactionVM.addEvent(EventRequestDTO("Buch Faust", listOf(2, 3), 6.99)) == 400)
        if (transactionVM.transactions.value != null) {
            assertTrue(transactionVM.transactions.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }
}