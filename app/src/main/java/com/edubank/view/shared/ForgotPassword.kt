package com.edubank.view.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.edubank.R
import com.edubank.databinding.ForgotPasswordFragmentBinding
import com.edubank.viewmodel.LoginViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet den Screen der angezeigt wird, wenn man im Login-Screen "Passwort vergessen?" wählt und bietet
 * Funktionen um ein neues Passwort anzufordern.
 *
 * @author Team EduBank
 */
class ForgotPassword : Fragment() {

    private val loginVM : LoginViewModel by activityViewModels()
    private lateinit var binding : ForgotPasswordFragmentBinding

    /**
     * Die Methode wird aufgerufen, wenn das Fragment neu gebaut wird. Hier werden die Listener für die UI-Elemente
     * festgelegt.
     */
    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View {
        binding = ForgotPasswordFragmentBinding.inflate(inflater, container, false)
        binding.loginVM = loginVM
        binding.lifecycleOwner = this

        binding.btnForgotPasswordRequestPassword.setOnClickListener {
            requestPassword()
        }

        return binding.root
    }

    /**
     * Die Methode ruft die forgotPassword-Methode des LoginViewModel auf, um ein neues Passwort anzufordern.
     *
     * @see LoginViewModel.forgotPassword
     */
    private fun requestPassword() {
        val email = binding.txtForgotPasswordEmail.text.toString()

        lifecycleScope.launch {
            if (MainActivity.isEmail(email)) {
                loginVM.forgotPassword(email)
                Snackbar.make(requireView(), getString(R.string.newPasswordSent), Snackbar.LENGTH_LONG).show()
            } else {
                Snackbar.make(requireView(), getString(R.string.invalidEmail), Snackbar.LENGTH_LONG).show()
            }
        }
    }
}