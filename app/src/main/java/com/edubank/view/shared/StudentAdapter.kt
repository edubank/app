package com.edubank.view.shared


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.StudentItemBinding
import com.edubank.model.dto.response.AccountResponseDTO

/**
 * Adapter für die Anzeige von Schülern im EventManger Fragment. Weitere Erklärungen zu den einzelnen Methoden finden
 * sich in der Dokumentation von [RecyclerView.Adapter]
 *
 * @property eventManager Der [EventManager], der den Adapter für die Anzeige der Schüler benutzt.
 * @author Team EduBank
 */
class StudentAdapter(private val eventManager: EventManager) : RecyclerView.Adapter<StudentAdapter.ViewHolder>() {

    /**
     * Die Accounts, die der Adapter anzeigt.
     */
    var accounts = mutableListOf<AccountResponseDTO>()

    /**
     * Das Flag, das anzeigt, ob im [EventManager] angeklickt wurde, dass alle Schüler vom Event betroffen sind.
     */
    var selectedAllStudents = false

    /**
     * Der interne ViewHolder, der die StudentItems hält und repräsentiert.
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val binding = StudentItemBinding.bind(view)

        val cbStudent = binding.cbStudentItemItem

        init {

            cbStudent.setOnCheckedChangeListener { _, isChecked ->
                val position = bindingAdapterPosition
                eventManager.wasClicked(accounts[position].id, isChecked)
            }
        }
    }

    /**
     * Erstellt einen neuen ViewHolder für ein StudentItem.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.student_item, parent, false)

        return ViewHolder(view)
    }

    /**
     * Initialisiert den gegebenen [ViewHolder] mit dem Account an der gegebenen position.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val account = accounts[position]
        val student = account.student
        val firstName = student.firstName
        val lastName = student.lastName

        holder.apply {
            cbStudent.text = String.format("$firstName $lastName")
            cbStudent.isChecked = selectedAllStudents
        }
    }

    /**
     * Gibt die Anzahl der Accounts zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = accounts.size
}