package com.edubank.view.admin

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers.anything
import org.hamcrest.Matchers.containsString
import org.junit.*
import kotlin.random.Random

class CreateUserTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUpAdmin() {
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        TestData.waitForServer()

    }

    /**
     * Legt einen neuen Benutzer im System an und überprüft, ob dieser angelegt wurde.
     * TF140
     */
    @Test
    fun test_createUser() {
        val randomId = Random.Default.nextInt(100)
        val firstName = "Test${randomId}"
        val lastName = " User${randomId}"
        val randomEmail = "testuser${randomId}@edubankmail.com"
        val roleId = 1 // 0 student, 1 teacher, 2 admin


        // open user manager
        onView(withId(R.id.userManagerButton)).perform(click())
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        TestData.waitForServer()

        // type user data
        onView(withId(R.id.firstNameEdit)).perform(replaceText(firstName))
        onView(withId(R.id.lastNameEdit)).perform(replaceText(lastName))
        onView(withId(R.id.emailEdit)).perform(replaceText(randomEmail))
        TestData.waitForServer()

        // select role
        onView(withId(R.id.roleSpinner)).perform(click())
        onData(anything()).atPosition(roleId).perform(click())

        // add
        onView(withId(R.id.addBtn)).perform(click())
        TestData.waitForServer()

        // check if user is in recycler view
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(1))
            .check(matches(hasDescendant(withText(containsString("$firstName $lastName")))))
    }

    @After
    fun cleanUpAccount() {
        // select and delete created user
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<UserAdapter.UserViewHolder>(0, click())
        )
        onView(withId(R.id.deleteUserBtn)).perform(click())

        // dialog click confirm
        onView(withId(android.R.id.button1)).perform(click())
        TestData.waitForServer()

        // logout
        Espresso.openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())
        TestData.waitForServer()
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}