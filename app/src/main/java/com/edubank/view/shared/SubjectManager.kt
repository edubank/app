package com.edubank.view.shared

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectManagerFragmentBinding
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.AccountViewModel
import com.edubank.viewmodel.SubjectViewModel
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.color.MaterialColors
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment SubjectManager stellt die "Fach-Bearbeiten" Ansicht dar.
 *
 * Wichtig: Vor jedem Aufruf sollte im SubjectViewModel das Fach, welches angezeigt werden soll, in 'subjects'
 * an erster Stelle hinterlegt werden.
 *
 * @see SubjectViewModel
 * @author Team EduBank
 */
class SubjectManager : Fragment(), SubjectManagerAdapter.OnItemClickListener {

    //View-Models of this fragment
    private val subjectVM: SubjectViewModel by activityViewModels()
    private val userVM: UserViewModel by activityViewModels()
    private val accountVM: AccountViewModel by activityViewModels()

    private lateinit var binding: SubjectManagerFragmentBinding
    private lateinit var studentAdapter: SubjectManagerAdapter

    private lateinit var userAccountList: MutableList<AccountResponseDTO>
    private var userList: MutableList<UserResponseDTO> = mutableListOf()
    private var modifyList: MutableList<Int> = mutableListOf()

    private var managerModeDelete: Boolean = false
    private var subjectId = -1

    /**
     * Die Methode wird aufgerufen, wenn das Fragment neu gebaut wird.
     * Hier werden die Listener für die UI-Elemente
     * festgelegt. Mit Databinding wird die View an das ViewModel angebunden.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = SubjectManagerFragmentBinding.inflate(inflater, container, false)
        binding.subjectVM = subjectVM
        binding.accountVM = accountVM
        binding.userVM = userVM
        studentAdapter = SubjectManagerAdapter(this)

        binding.apply {
            deleteTab.setOnClickListener {
                resetRecyclerView()
                setRemoveMode()
                clearSearchBox()
            }
            addTab.setOnClickListener {
                resetRecyclerView()
                setAddMode()
                clearSearchBox()
            }
            confirmButton.setOnClickListener {
                confirmChanges()
                clearSearchBox()
            }
            recyclerSubjectView.apply {
                layoutManager = LinearLayoutManager(this.context)
                adapter = studentAdapter
                setHasFixedSize(true)
            }

            init()
            userSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText != null) {
                        filterUsers(newText)
                    }
                    return false
                }
            })
        }

        binding.lifecycleOwner = this
        return binding.root
    }

    /**
     * Diese Methode initialisiert die Ansicht des Fragments beim ersten Aufrufen des Fragments.
     * Es wird die Ansicht zum Hinzufügen angezeigt.
     */
    private fun init() {
        setAddMode()
        val subject =
            "${subjectVM.subjects.value!![0].name} - ${subjectVM.subjects.value!![0].teacher.firstName} " + "${subjectVM.subjects.value!![0].teacher.lastName} " + "(${subjectVM.subjects.value!![0].teacher.username})"
        binding.subjectName.text = subject
    }

    /**
     * Diese Methode initialisiert die Ansicht der Listenelemente beim Löschmodus,
     * die Liste enthält die Schüler in dem Fach.
     */
    private fun initSubjectAccountList() {

        lifecycleScope.launch {
            subjectId = subjectVM.subjects.value?.get(0)?.id!!
            accountVM.getSubjectAccounts(subjectId)
            userAccountList = (accountVM.accounts.value as MutableList<AccountResponseDTO>?)!!
            userList = mutableListOf()

            for (userAccounts in userAccountList) {
                userList.add(userAccounts.student)
            }

            userList = sortUsers(userList).toMutableList()
            studentAdapter.onStudentsChanged(userList)
        }
    }

    /**
     * Diese Methode initialisiert die Ansicht der Listenelemente beim "Hinzufügen"-Modus,
     * die Liste enthält Schüler, die nicht in dem Fach sind.
     */
    private fun initUserAddList() {
        lifecycleScope.launch {
            subjectId = subjectVM.subjects.value?.get(0)?.id!!
            accountVM.getSubjectAccounts(subjectId)
            userAccountList = (accountVM.accounts.value as MutableList<AccountResponseDTO>?)!!
            userVM.getUserByRole(UserRole.ROLE_STUDENT.toString())
            userList = (userVM.users.value as MutableList<UserResponseDTO>?)!!

            // entferne Schüler aus der Benutzerliste, die schon in dem Fach angemeldet sind
            val availableAccounts = mutableListOf<UserResponseDTO>()
            for (accounts in userAccountList) {
                for (users in userList) {
                    if (users.id == accounts.student.id) {
                        if (!availableAccounts.contains(users)) {
                            availableAccounts.add(users)
                        }
                    }
                }
            }
            for (user in availableAccounts) {
                userList.remove(user)
            }

            userList = sortUsers(userList).toMutableList()
            studentAdapter.onStudentsChanged(userList)
        }
    }


    /**
     * Diese Methode setzt die Ansicht auf Hinzufügen und initialisiert die Liste
     */
    private fun setAddMode() {
        initUserAddList()
        managerModeDelete = false
        val defaultColor = MaterialColors.getColor(requireContext(), android.R.attr.colorBackground, Color.BLACK)
        val addTabColor = MaterialColors.getColor(requireContext(), R.attr.buttonBarPositiveButtonStyle, Color.BLACK)
        val confirmButtonColor = MaterialColors.getColor(requireContext(), R.attr.colorPrimary, Color.BLACK)

        binding.addTab.setBackgroundColor(addTabColor)
        binding.deleteTab.setBackgroundColor(defaultColor)
        binding.confirmButton.setBackgroundColor(confirmButtonColor)
    }

    /**
     * Diese Methode setzt die Ansicht auf Löschen und initialisiert die Liste
     */
    private fun setRemoveMode() {
        initSubjectAccountList()
        managerModeDelete = true
        val defaultColor = MaterialColors.getColor(requireContext(), android.R.attr.colorBackground, Color.BLACK)
        val deleteTabColor = MaterialColors.getColor(requireContext(), R.attr.buttonBarNegativeButtonStyle, Color.BLACK)
        val confirmButtonColor = requireContext().getColor(R.color.back_red)

        binding.addTab.setBackgroundColor(defaultColor)
        binding.deleteTab.setBackgroundColor(deleteTabColor)
        binding.confirmButton.setBackgroundColor(confirmButtonColor)
    }

    /**
     * Wird aufgerufen, wenn der Knopf zum bestätigen angeklickt wird.
     * Bestätigt die ausgewählten Änderungen und führt diese aus.
     * Setzt die Listen-Ansicht zurück.
     */
    private fun confirmChanges() {
        lifecycleScope.launch {
            modifyList = studentAdapter.modifyList
            if (managerModeDelete) {
                if (modifyList.isNotEmpty()) {
                    subjectId = subjectVM.subjects.value?.get(0)?.id!!
                    subjectVM.getSubjectById(subjectId)
                    subjectVM.dropStudents(subjectId, modifyList)
                    Snackbar.make(
                        requireView(), getString(R.string.removed_student), Snackbar.LENGTH_SHORT).show()
                    resetRecyclerView()
                    initSubjectAccountList()
                }
            } else {
                if (modifyList.isNotEmpty()) {
                    subjectId = subjectVM.subjects.value?.get(0)?.id!!
                    subjectVM.getSubjectById(subjectId)
                    subjectVM.addStudents(subjectId, modifyList)
                    Snackbar.make(
                        requireView(), getString(R.string.added_student), Snackbar.LENGTH_SHORT).show()
                    resetRecyclerView()
                    initUserAddList()
                }
            }
        }
    }

    /**
     * Diese Methode setzt die Adapterwerte der RecyclerView zurück.
     * Es werden auch die Listen des SubjectManagers zurückgesetzt.
     */
    private fun resetRecyclerView() {
        modifyList = mutableListOf()
        userList = mutableListOf()
        userAccountList = mutableListOf()
        studentAdapter.reset()
    }

    /**
     * Diese Methode überschreibt onItemClick im ViewHolder.
     */
    override fun onItemClick(position: Int) { // nichts wird ausgeführt, evtl für Erweiterung
    }

    /**
     * Diese Methode sortiert die Schülerliste alphabetisch nach Nachnamen
     *
     * @param unsortedAccountList Liste, die Sortiert werden soll
     * @return sortierte Liste als Ergebnis
     */
    private fun sortUsers(unsortedAccountList: List<UserResponseDTO>): List<UserResponseDTO> {
        return unsortedAccountList.sortedBy { it.lastName }
    }

    /**
     * Diese Methode bereinigt die Eingabe im Suchfeld
     */
    private fun clearSearchBox() {
        binding.userSearch.setQuery("", true)
        binding.userSearch.clearFocus()
        binding.recyclerSubjectView.Recycler().clear()
    }

    /**
     * Diese Methode filtert die Schülerliste nach der Eingabe, anhand des Prefix,
     * und teilt die Änderungen dem Adapter mit, um die neue gefilterte Liste anzuzeigen.
     *
     * @param newName Die Eingabe als String
     */
    private fun filterUsers(newName: String) {
        val filteredList = mutableListOf<UserResponseDTO>()
        for (user in userList) {
            val fullName = "${user.firstName.lowercase()} ${user.lastName.lowercase()}"
            if (user.firstName.lowercase().startsWith(newName.lowercase()) || user.lastName.lowercase()
                    .startsWith(newName.lowercase()) || fullName.startsWith(newName.lowercase())
            ) {
                filteredList.add(user)
            }
        }
        if (newName == "") {
            studentAdapter.onStudentsChanged(userList)
        } else {
            studentAdapter.onStudentsChanged(filteredList)
        }
    }
}
