package com.edubank.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.model.dto.response.AccountTransactionsResponseDTO
import com.edubank.model.retrofitclient.AccountService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class AccountViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var accountVM : AccountViewModel
    @get:Rule
    private val mockWebServer = MockWebServer()
    private val accountAPI by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(AccountService::class.java)
    }
    private val httpClient = OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .build()
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Before
    fun setup() {
        mockWebServer.start()
        accountVM = AccountViewModel()
        accountVM.setService(accountAPI)

    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getStudentAccounts_success() = runBlocking {
        val jsonData = getDataObject("account_response_list.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(accountVM.getStudentAccounts(1) == 200)
        assertTrue(accountVM.accounts.value == gson.fromJson<List<AccountResponseDTO>>(
            jsonData, object : TypeToken<List<AccountResponseDTO>>() {}.type))
        if (accountVM.accounts.value != null) {
            assertTrue(accountVM.accounts.value!!.size == 2)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getStudentAccounts_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(accountVM.getStudentAccounts(1) == 400)
        if (accountVM.accounts.value != null) {
            assertTrue(accountVM.accounts.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectAccounts_success() = runBlocking {
        val jsonData = getDataObject("account_response_list.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(accountVM.getSubjectAccounts(1) == 200)
        assertTrue(accountVM.accounts.value == gson.fromJson<List<AccountResponseDTO>>(
            jsonData, object : TypeToken<List<AccountResponseDTO>>() {}.type))
        if (accountVM.accounts.value != null) {
            assertTrue(accountVM.accounts.value!!.size == 2)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectAccounts_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(accountVM.getSubjectAccounts(1) == 400)
        if (accountVM.accounts.value != null) {
            assertTrue(accountVM.accounts.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getAccount_success() = runBlocking {
        val jsonData = getDataObject("account_transaction_response.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))
        val response = accountVM.getAccount(1)

        assertTrue(response == gson.fromJson(jsonData, AccountTransactionsResponseDTO::class.java))
        if (accountVM.accounts.value != null) {
            assertTrue(accountVM.accounts.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getAccount_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(accountVM.getAccount(1) == null)
        if (accountVM.accounts.value != null) {
            assertTrue(accountVM.accounts.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }
}