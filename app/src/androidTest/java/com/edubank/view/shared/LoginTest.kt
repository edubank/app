package com.edubank.view.shared

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import org.junit.Rule
import org.junit.Test

/**
 * Beinhaltet allgemeine UI-Tests für Login.
 *
 * @see Login
 * @author Team EduBank
 */
class LoginTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Prüft, ob das Fragment richtig angezeigt wird.
     */
    @Test
    fun test_fragmentIsDisplayedCorrectly() {
        onView(withId(R.id.lbl_login_title)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_login_subtitle)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_login_info)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_login_infoText)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_login_username)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_login_password)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_login_login)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_login_forgotPassword)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob beim Klick auf den Knopf "Passwort vergessen?" auch das richtige Fragment geöffnet wird.
     */
    @Test
    fun test_ForgotPassword() { //Click on button "Passwort vergessen"
        onView(withId(R.id.btn_login_forgotPassword)).perform(click())

        //Navigate to the due fragment
        onView(withId(R.id.forgot_password_fragment)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Admin sich anmelden kann und die richtige Ansicht bekommt.
     */
    @Test
    fun test_LoginAdmin() { //Log in as admin
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        //Check if the homescreen for admins is displayed
        onView(withId(R.id.class_overview_fragment)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Admin sich abmelden kann und dann wieder im Login-Screen landet.
     */
    @Test
    fun test_logoutAdmin() { // login as admin
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        // select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Lehrer sich anmelden kann und die richtige Ansicht bekommt.
     */
    @Test
    fun test_LoginTeacher() { //Log in as teacher
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.teacherUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.teacherPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        //Check if the homescreen for teachers is displayed
        onView(withId(R.id.subject_overview_teacher_fragment)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Lehrer sich abmelden kann und dann wieder im Login-Screen landet.
     */
    @Test
    fun test_logoutTeacher() { //Log in as teacher(setup)
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.teacherUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.teacherPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        //Select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())


        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Schüler sich anmelden kann und die richtige Ansicht bekommt.
     */
    @Test
    fun test_LoginStudent() { //Log in as student
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.studentUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.studentPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        //Check if the homescreen for students is displayed
        onView(withId(R.id.subject_overview_student_fragment)).check(matches(isDisplayed()))
    }

    /**
     * Prüft, ob ein Schüler sich abmelden kann und dann wieder im Login-Screen landet.
     */
    @Test
    fun test_logoutStudent() { //Log in as student(setup)
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.studentUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.studentPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()


        //Select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())


        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}