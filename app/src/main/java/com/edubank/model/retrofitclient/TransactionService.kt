package com.edubank.model.retrofitclient

import com.edubank.model.dto.request.EventRequestDTO
import com.edubank.model.dto.request.TransactionRequestDTO
import com.edubank.model.dto.response.TransactionResponseDTO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Scnittstellen-Beschreibung für den Retrofit-Service TransactionService.
 * Die Schnittstelle beschreibt die Transaktions-Anfragen an den Server.
 *
 * @author Team EduBank
 */
interface TransactionService {

    /**
     * Die Funktion getTransactions frägt die Transaktionen eines Kontos an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param accountId Die ID des Kontos, dessen Transaktionsverlauf angefragt werden soll.
     * @return Call-Objekt mit einer Transaktions-Daten-Liste.
     */
    @GET("accounts/{id}/transactions")
    fun getTransactions(@Path("id") accountId : Int) : Call<List<TransactionResponseDTO>>

    /**
     * Die Funktion getTransactionById frägt eine Transaktion an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param transactionId Die ID der Transaktion, die angefragt werden soll.
     * @return Call-Objekt mit den Transaktions-Daten.
     */
    @GET("transactions/{id}")
    fun getTransactionById(@Path("id") transactionId : Int) : Call<TransactionResponseDTO>

    /**
     * Die Funktion executeTransaction führt eine Transaktion auf dem Server durch.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param accountId Die Id des Kontos auf dem die Transaktion durchgefüurt werden soll.
     * @param transaction Transaktion die durchgeführt werden soll.
     * @return Call-Objekt mit den Transaktions-Daten.
     */
    @POST("accounts/{id}/transactions")
    fun executeTransaction(
        @Path("id") accountId : Int,
        @Body transaction : TransactionRequestDTO
    ) : Call<TransactionResponseDTO>

    /**
     * Die Funktion addEvent führt ein Event auf dem Server durch.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param event Event das durchgeführt werden soll.
     * @return Call-Objekt mit einer Transaktions-Daten-Liste des Events.
     */
    @POST("initiate-event")
    fun addEvent(@Body event : EventRequestDTO) : Call<List<TransactionResponseDTO>>

}