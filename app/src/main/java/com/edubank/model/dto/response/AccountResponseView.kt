package com.edubank.model.dto.response

/**
 * AccountResponseView ist eine Daten-Klasse für Konto-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Kontos
 * @property student Eigentümer des Kontos
 * @property balance Kontostand des Kontos
 */
data class AccountResponseView (
    val id : Int,
    val student : UserResponseDTO,
    val balance : Double
)
