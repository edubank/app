package com.edubank.view.shared

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectAccountFragmentBinding
import com.edubank.model.dto.request.EventRequestDTO
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.model.dto.response.TransactionResponseDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.AccountViewModel
import com.edubank.viewmodel.TransactionViewModel
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment SubjectAccount stellt die Ansicht für einen Schüleraccount dar.
 *
 * Wichtig: Vor jedem Aufruf sollte im AccountViewModel der Schüleraccount, dessen Transaktionen
 * angezeigt werden sollen, in 'accounts' an erster Stelle hinterlegt werden.
 *
 * @see AccountViewModel
 * @author Team EduBank
 */
class SubjectAccount : Fragment(), SubjectAccountAdapter.OnItemClickListener {

    //View-Model of this fragment
    private val accountViewModel: AccountViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private val transactionViewModel: TransactionViewModel by activityViewModels()

    private lateinit var binding: SubjectAccountFragmentBinding

    private lateinit var accountAdapter: SubjectAccountAdapter
    private lateinit var transactionList: List<TransactionResponseDTO>
    private lateinit var account: AccountResponseDTO

    private var clickedAccountId = -1


    /**
     * Die Methode wird aufgerufen, wenn das Fragment neu gebaut wird.
     * Hier werden die Listener für die UI-Elemente
     * festgelegt. Mit Databinding wird die View an das ViewModel angebunden.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = SubjectAccountFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userViewModel
        binding.transactionVM = transactionViewModel
        binding.accountVM = accountViewModel
        binding.lifecycleOwner = this


        accountAdapter = SubjectAccountAdapter(this)

        // RecyclerView aufbauen
        binding.recyclerTransactionView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = accountAdapter
            setHasFixedSize(true)
        }

        binding.transactionButton.setOnClickListener {
            binding.transactionEventLayout.visibility = View.VISIBLE
            binding.commitButton.visibility = View.VISIBLE
            binding.transactionButton.visibility = View.GONE

        }

        // Wenn eine Transaktion bestätigt wird führe diese aus
        binding.commitButton.setOnClickListener {
            binding.transactionEventLayout.visibility = View.GONE
            binding.commitButton.visibility = View.GONE
            binding.transactionButton.visibility = View.VISIBLE


            lifecycleScope.launch {
                createTransaction()
                requestTransactions()
                showBalance()
            }
        }

        // initiale Ansicht laden
        showHeadlines()
        requestTransactions()
        return binding.root
    }

    /**
     * Fordert die Transaktionen des Schüleraccouts vom Server an. Zeigt danach diese in der
     * Recycler View.
     */
    private fun requestTransactions() {
        lifecycleScope.launch {
            launch {
                transactionViewModel.getTransactions(clickedAccountId)
                showTransactions()
            }.join()
            binding.pbSubjectAccountTransactions.visibility = View.GONE
        }
    }

    /**
     * Erstellt eine Transaktion, nachdem das Feld zum Bestätigen angeklickt wird,
     * nur wenn die Eingabe korrekt ist. (!= null, !=0, nicht leer)
     */
    private suspend fun createTransaction() {
        var eventName = binding.transactionName.text.toString()
        var amount = binding.transactionAmount.text.toString().toDoubleOrNull()
        val selectedStudentId = mutableListOf<Int>()
        selectedStudentId.add(accountViewModel.accounts.value?.get(0)?.id!!)
        val deposit = binding.cbTransactionDeposit.isChecked

        if ((eventName.isBlank() && !deposit) || amount == null || amount == 0.0) {
            Snackbar.make(requireView(), R.string.invalidData, Snackbar.LENGTH_LONG).show()
            return
        }

        if (deposit) {
            if (eventName.isBlank()) eventName = getString(R.string.deposit)
        } else {
            amount *= -1
        }

        transactionViewModel.addEvent(EventRequestDTO(eventName, selectedStudentId, amount))
        Snackbar.make(requireView(), R.string.successfulTransactionCreation, Snackbar.LENGTH_SHORT).show()

        binding.transactionName.text.clear()
        binding.transactionAmount.text.clear()
        binding.cbTransactionDeposit.isChecked = false

    }

    /**
     * Setzt die Liste der Transaktionen
     * und übergibt diese an den Adapter der RecyclerView für die Darstellung.
     * Sortiert chronologisch
     */
    private fun showTransactions() {
        transactionList = transactionViewModel.transactions.value!!
        accountAdapter.onTransactionsChanged(transactionList.toMutableList().asReversed())
    }

    /**
     * Diese Methode setzt die Überschriften der Ansicht.
     */
    private fun showHeadlines() {
        lifecycleScope.launch {
            // fordere account des angeklickten kontos an
            clickedAccountId = accountViewModel.accounts.value?.get(0)?.id!!
            account = accountViewModel.accounts.value?.get(0)!!
            pickHeadline()
            showBalance()
        }
    }

    /**
     * Diese Methode setzt die Überschrift der Ansicht entweder für Schüler, oder für
     * Admin/Lehrer.
     */
    private suspend fun pickHeadline() {
        // Überprüfe, ob ein Schüler oder Lehrer die Ansicht geöffnet hat
        userViewModel.getMyUser()
        if (userViewModel.users.value?.get(0)?.role?.equals(UserRole.ROLE_STUDENT) == true) {
            // Zeige Lehrernamen an
            val teacherFirstName = account.subject.teacher.firstName
            val teacherLastName = account.subject.teacher.lastName
            val teacherUsername = account.subject.teacher.username
            val teacherName = "$teacherFirstName $teacherLastName ($teacherUsername)"
            binding.headerRoleName.text = teacherName
        } else {
            // Zeige Schülernamen an
            val studentFirstName = account.student.firstName
            val studentLastName = account.student.lastName
            val studentUsername = account.student.username
            val className = account.subject.schoolClass.name
            val studentName =
                "$className - $studentFirstName $studentLastName ($studentUsername)"
            binding.headerRoleName.text = studentName
            binding.transactionButton.visibility = View.VISIBLE
        }

        // Fach
        binding.subjectName.text = account.subject.name
    }

    /**
     * Diese Methode setzt den Guthabenwert des angeklickten Schülers auf den aktuellen Wert
     * und ändert dessen Farbe je nach Kontostand.
     */
    private suspend fun showBalance() {

        // Update account
        accountViewModel.getAccount(clickedAccountId)
        account = accountViewModel.accounts.value?.get(0)!!

        // Guthaben setzen
        val balance = account.balance
        val balanceText = "${(getString(R.string.balanceCaption))} ${balance}€"
        binding.balanceCaption.text = balanceText

        // Guthabenfarbe setzen
        when {
            balance < 0 -> binding.balanceCaption.setTextColor(Color.parseColor("#ca5e5e"))
            else -> context?.let { binding.balanceCaption.setTextColor(it.getColor(R.color.text_green)) }
        }
    }

    /**
     * Diese Methode kann für Erweiterungen benutzt werden, evtl. Transaktionsansicht.
     */
    override fun onItemClick(position: Int) {
        // nichts wird ausgeführt, evtl für Erweiterung
    }


}