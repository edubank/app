package com.edubank.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.edubank.model.dto.request.UserRequestDTO
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.retrofitclient.UserService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class UserViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var userVM : UserViewModel
    @get:Rule
    private val mockWebServer = MockWebServer()
    private val userAPI by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(UserService::class.java)
    }
    private val httpClient = OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .build()
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Before
    fun setup() {
        mockWebServer.start()
        userVM = UserViewModel()
        userVM.setService(userAPI)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getAllUsers_success() = runBlocking {
        val jsonData = getDataObject("user_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.getAllUsers() == 200)
        assertTrue(userVM.users.value == gson.fromJson<List<UserResponseDTO>>(
            jsonData, object : TypeToken<List<UserResponseDTO>>() {}.type))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 4)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getAllUsers_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.getAllUsers() == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getMyUser_success() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.getMyUser() == 200)
        assertTrue(userVM.users.value == listOf(gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getMyUser_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.getMyUser() == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateSettings_success() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.updateSettings("musterschueler@kit.edu", "admin") == 200)
        assertTrue(userVM.users.value == listOf(gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateSettings_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.updateSettings("musterschueler@kit.edu", "admin") == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getUserById_success() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.getUserById(2) == 200)
        assertTrue(userVM.users.value == listOf(gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getUserById_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.getUserById(2) == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getUserByRole_success() = runBlocking {
        val jsonData = getDataObject("user_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.getUserByRole("ROLE_STUDENT") == 200)
        assertTrue(userVM.users.value == gson.fromJson<List<UserResponseDTO>>(
            jsonData, object : TypeToken<List<UserResponseDTO>>() {}.type))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 4)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getUserByRole_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.getUserByRole("ROLE_STUDENT") == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getUserBySubject_success() = runBlocking {
        val jsonData = getDataObject("user_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.getUserBySubject(1) == 200)
        assertTrue(userVM.users.value == gson.fromJson<List<UserResponseDTO>>(
            jsonData, object : TypeToken<List<UserResponseDTO>>() {}.type))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 4)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getUserBySubject_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.getUserBySubject(1) == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun createClass_success() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))
        val resp = gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)
        val user = UserRequestDTO(resp.role.toString(), resp.firstName, resp.lastName, resp.email)

        assertTrue(userVM.createUser(user) == 200)
        assertTrue(userVM.users.value == listOf(resp))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun createClass_failed() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))
        val resp = gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)
        val user = UserRequestDTO(resp.role.toString(), resp.firstName, resp.lastName, resp.email)

        assertTrue(userVM.createUser(user) == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateUser_success() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val resp = gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)
        val user = UserRequestDTO(resp.role.toString(), resp.firstName, resp.lastName, resp.email)

        assertTrue(userVM.updateUser(user, 2) == 200)
        assertTrue(userVM.users.value == listOf(resp))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateUser_failed() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))
        val resp = gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)
        val user = UserRequestDTO(resp.role.toString(), resp.firstName, resp.lastName, resp.email)

        assertTrue(userVM.updateUser(user, 2) == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun deleteSubject_success() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody("\"true\""))
        assertTrue(userVM.deleteUser(2) == 200)
    }

    @Test
    fun deleteSubject_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400)
            .setBody("\"false\""))
        assertTrue(userVM.deleteUser(2) == 400)
    }

    @Test
    fun setInitialData_success() = runBlocking {
        val jsonData = getDataObject("user_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(userVM.setInitialData("musterschueler@kit.edu", "admin") == 200)
        assertTrue(userVM.users.value == listOf(gson.fromJson<UserResponseDTO>(
            jsonData, object : TypeToken<UserResponseDTO>() {}.type)))
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun setInitialData_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(userVM.setInitialData("musterschueler@kit.edu", "admin") == 400)
        if (userVM.users.value != null) {
            assertTrue(userVM.users.value!!.isEmpty())
        } else {
            assertTrue(false)
        }
    }
}