package com.edubank.model.dto.response

import com.google.gson.annotations.SerializedName

/**
 * SubjectClassResponseView ist eine Daten-Klasse für Fächer-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Faches
 * @property name Name des Faches
 * @property schoolClass Klasse des Faches
 * @property teacher Lehrer des Faches
 */
data class SubjectClassResponseView (
    val id : Int,
    val name : String,
    @SerializedName("class")
    val schoolClass : SchoolClassResponseView,
    val teacher : UserResponseDTO
)
