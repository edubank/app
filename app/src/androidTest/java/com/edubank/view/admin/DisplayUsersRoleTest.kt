package com.edubank.view.admin

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Testet das Anzeigen von Benutzern einer spezifischen Rolle
 * TF150
 */
class DisplayUsersRoleTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUpAdmin() {
        // login
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        waitForServer(1000)

        // open user overview
        onView(withId(R.id.userManagerButton)).perform(click())
        waitForServer()
    }

    /**
     * Testet das Anzeigen von Benutzern der Rolle Lehrer
     * TF150
     */
    @Test
    fun test_showTeachers(){
        //filter teachers
        onView(withId(R.id.filterBtn)).perform(click())
        onView(withId(R.id.teachers)).perform(click())

        waitForServer(1000)
        //check teacher

        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(0))
            .check(matches(hasDescendant(withText("Lehrer"))))
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(1))
            .check(matches(hasDescendant(withText("Lehrer"))))
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(2))
            .check(matches(hasDescendant(withText("Lehrer"))))
    }

    /**
     * Testet das Anzeigen von Benutzern der Rolle Schüler
     * TF150
     */
    @Test
    fun test_showStudents(){
        //filter students
        onView(withId(R.id.filterBtn)).perform(click())
        onView(withId(R.id.students)).perform(click())

        waitForServer(1000)

        //check some student entries
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(0))
            .check(matches(hasDescendant(withText("Schüler"))))
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(1))
            .check(matches(hasDescendant(withText("Schüler"))))
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(2))
            .check(matches(hasDescendant(withText("Schüler"))))
    }

    /**
     * Testet das Anzeigen von Benutzern der Rolle Administrator
     * TF150
     */
    @Test
    fun test_showAdmins(){
        //filter admins
        onView(withId(R.id.filterBtn)).perform(click())
        onView(withId(R.id.admins)).perform(click())

        waitForServer(1000)

        // check some admin entries
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(0))
            .check(matches(hasDescendant(withText("Administrator"))))
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(1))
            .check(matches(hasDescendant(withText("Administrator"))))
        onView(withId(R.id.recyclerUserView))
            .perform(RecyclerViewActions.scrollToPosition<UserAdapter.UserViewHolder>(2))
            .check(matches(hasDescendant(withText("Administrator"))))

    }


    @After
    fun logout() {
        // wait just in case
        waitForServer()

        //Select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // wait for server to respond
        waitForServer()

        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}