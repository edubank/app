package com.edubank.view.shared

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectFragmentBinding
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.viewmodel.AccountViewModel
import com.edubank.viewmodel.ClassViewModel
import com.edubank.viewmodel.SubjectViewModel
import kotlinx.coroutines.launch

/**
 * Das Fragment Subject dient zur Ansicht bei Fächern.
 *
 * Wichtig: Vor jedem Aufruf sollte im SubjectViewModel das Fach, welches angezeigt werden soll, in 'subjects'
 * an erster Stelle hinterlegt werden.
 *
 * @see SubjectViewModel
 * @author Team EduBank
 */
class Subject : Fragment() {

    private val accountVM: AccountViewModel by activityViewModels()
    private val subjectVM: SubjectViewModel by activityViewModels()
    private val classVM: ClassViewModel by activityViewModels()

    private val accAdapter = AccountAdapter(this)

    private lateinit var binding: SubjectFragmentBinding

    private var subjectID = -1


    /**
     * Legt die Listener für UI-Elemente  fest und bindet die View an das ViewModel per DataBinding.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = SubjectFragmentBinding.inflate(inflater, container, false)
        binding.accountVM = accountVM
        binding.subjectVM = subjectVM
        binding.classVM = classVM
        binding.lifecycleOwner = this

        lifecycleScope.launch { // if subjects is not empty assume the first subject is the one up for display
            classVM.getClass(subjectVM.subjects.value!![0].schoolClass.id)
            subjectID = subjectVM.subjects.value!![0].id
            val teacher = subjectVM.subjects.value!![0].teacher
            val subjectName =
                "${subjectVM.subjects.value?.get(0)?.name} - ${teacher.firstName} ${teacher.lastName} (${teacher.username})"

            binding.apply {
                lblSubjectClass.text = classVM!!.classes.value?.get(0)?.name // set the name of the class
                lblSubjectSubject.text = subjectName

                btnSubjectEditSubject.setOnClickListener {
                    openSubjectManager()
                }

                btnSubjectEventManager.setOnClickListener {
                    openEventManager()
                }
            }

            binding.rvSubjectStudents.apply {
                layoutManager = LinearLayoutManager(this@Subject.context)
                adapter = accAdapter
            }
            updateAccountList()
        }

        return binding.root
    }

    /**
     * Holt die Liste der Accounts des Subjects vom Server und übergibt diese an den Adapter der RecyclerView für die
     * Darstellung.
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun updateAccountList() {
        lifecycleScope.launch {
            lifecycleScope.launch {
                accountVM.getSubjectAccounts(subjectID)
                val currentAccounts = mutableListOf<AccountResponseDTO>()

                for (account in accountVM.accounts.value!!) {
                    currentAccounts.add(account)
                }

                accAdapter.accounts = currentAccounts.sortedBy { it.student.lastName }.toMutableList()

            }.join()
            accAdapter.notifyDataSetChanged()
            binding.pbSubjectStudents.visibility = View.GONE
        }
    }

    /**
     * Wird aufgerufen, wenn eines der Konten angeklickt wird und öffnet die Ansicht für dieses Konto.
     *
     * @see SubjectAccount
     */
    fun showMemberAccount(accountID: Int) {
        lifecycleScope.launch {
            accountVM.getAccount(accountID)
            requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, SubjectAccount())
                .addToBackStack(null).commit()
        }
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Button für den Event-Managerangeklickt wird. Die Methode tauscht die
     * aktuelle Ansicht mit der Event-Manager-Ansicht für das aktuelle Fach aus.
     */
    private fun openEventManager() {
        lifecycleScope.launch {
            subjectVM.getSubjectById(subjectID)
            requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, EventManager())
                .addToBackStack(null).commit()
        }
    }

    /**
     * Die Methode wird aufgerufen, wenn der Button angeklickt wird, um dasFach zu bearbeiten. Die Methode tauscht die
     * aktuelle Ansicht mit der SubjectManager-Ansicht für das aktuelle Fach aus.
     */
    private fun openSubjectManager() {
        requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, SubjectManager())
            .addToBackStack(null).commit()
    }
}