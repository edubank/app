package com.edubank.view.admin

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DisplayAccountDataTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        // Login
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        // Select Class
        TestData.waitForServer()
        onView(withId(R.id.recyclerClassView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        // Select Subject
        TestData.waitForServer()
        onView(withId(R.id.recyclerSubjectView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        //Select Account
        TestData.waitForServer()
        onView(withId(R.id.rv_subject_students)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
    }

    @Test
    fun testSubjectAccountIsDisplayed() {

        // View elements are shown
        onView(withId(R.id.subjectName)).check(matches(isDisplayed()))
        onView(withId(R.id.headerRoleName)).check(matches(isDisplayed()))
        onView(withId(R.id.balanceCaption)).check(matches(isDisplayed()))
        onView(withId(R.id.transactionButton)).check(matches(isDisplayed()))
        onView(withId(R.id.history)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerTransactionView)).check(matches(isDisplayed()))

    }
}

