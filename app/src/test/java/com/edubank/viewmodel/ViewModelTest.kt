package com.edubank.viewmodel

import java.io.File
import java.io.InputStreamReader
import java.nio.file.Paths

private val rootPath : String = "\\src\\test\\java\\com\\edubank\\test_res"

/**
 * Liest eine json-Datei als String ein. Der String kann dann als Body
 * eines MockResponse-Objektes gesetzt werden und so eine Server-Antwort imitieren.
 *
 * @param file Name der Datei
 * @return Dateiinhalt als String
 */
fun getDataObject(file: String) : String {
    val path = Paths.get("").toAbsolutePath().toString() + rootPath
    val reader = InputStreamReader(File(path).resolve(file).inputStream())
    val content = reader.readText()
    reader.close()
    return content
}