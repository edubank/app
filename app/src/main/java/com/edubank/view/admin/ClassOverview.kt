package com.edubank.view.admin

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.ClassOverviewFragmentBinding
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.viewmodel.ClassViewModel
import com.edubank.viewmodel.UserViewModel
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine Übersicht mit allen Klassen.
 *
 * @author Team EduBank
 */
class ClassOverview : Fragment(), ClassAdapter.OnItemClickListener {

    private val classViewModel: ClassViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var classList: List<SchoolClassResponseDTO>
    private lateinit var binding : ClassOverviewFragmentBinding
    private lateinit var classAdapter: ClassAdapter

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {
        binding = ClassOverviewFragmentBinding.inflate(inflater, container, false)
        binding.classVM = classViewModel
        binding.userVM = userViewModel
        binding.lifecycleOwner = this

        // Die anfänglichen Daten zeigen
        getAdminName()
        requestClasses()

        // SetOnClickListeners
        binding.apply {
            classManagerButton.setOnClickListener { openClassManager()}
            userManagerButton.setOnClickListener { openUserManager() }
        }

        // RecyclerView aufbauen
        classAdapter = ClassAdapter(this)
        binding.recyclerClassView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = classAdapter
            setHasFixedSize(true)
        }

        return binding.root
    }

    /**
     * Fordert die Benutzerdaten des angemeldeten Admins vom Server an und setzt diese.
     */
    private fun getAdminName() {
        lifecycleScope.launch {
            userViewModel.getMyUser()
            val adminData = userViewModel.users.value?.get(0)!!
            val adminName = "${adminData.firstName} ${adminData.lastName} (${adminData.username})"
            binding.adminName.text = adminName
        }
    }

    /**
     * Eine Liste mit allen Klassen wird vom Server angefordert,
     * alphabetisch sortiert und in der Recyclerview gezeigt.
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun requestClasses() {
        lifecycleScope.launch {
            launch {
                classViewModel.getAllClasses()
                classList = classViewModel.classes.value!!.sortedBy { it.name.lowercase() }
                classAdapter.classes = classList.toMutableList()
                classAdapter.notifyDataSetChanged()
            }.join()
            binding.pbClassOverviewClasses.visibility = View.GONE
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der SubjectOverviewAdmin-Ansicht
     * der ausgewählten Klasse aus.
     */
    override fun onItemClick(position: Int) {
        lifecycleScope.launch {
            classViewModel.getClass(classList[position].id)
            openSubjectOverview()
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der SubjectOverviewAdmin-Ansicht aus.
     */
    private fun openSubjectOverview() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, SubjectOverviewAdmin())
            .addToBackStack(null).commit()
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der UserOverview-Ansicht aus.
     */
    private fun openUserManager() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, UserOverview())
            .addToBackStack(null).commit()
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der ClassManager-Ansicht aus.
     */
    private fun openClassManager() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, ClassManager())
            .addToBackStack(null).commit()
    }

}