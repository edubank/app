package com.edubank.view.admin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.TeacherItemBinding
import com.edubank.model.dto.response.UserResponseDTO

/**
 * Adapter für die Anzeige von Lehrer im SubjectOverviewManager-Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see SubjectOverviewManager
 * @property teachers Liste mit alle Lehrer die in der RecyclerView gezeigt werden
 * @author Team EduBank
 */
class TeacherAdapter(private val listener: OnItemClickListener):
    RecyclerView.Adapter<TeacherAdapter.TeacherViewHolder>(){

    var teachers = mutableListOf<UserResponseDTO>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TeacherViewHolder {

        // Erstellt eine neue Ansicht, die die Benutzeroberfläche des Listenelements definiert
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.teacher_item, viewGroup, false)

        return TeacherViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager)
     */
    override fun onBindViewHolder(holder: TeacherViewHolder, position: Int) {

        // Hole das Element an dieser Position
        // und ersetze den Inhalt der Ansicht durch dieses Element
        val currentItem = teachers[position]
        val name = "${currentItem.firstName} ${currentItem.lastName} (${currentItem.username})"
        holder.teacherName.text = name
    }

    /**
     * Gibt die Anzahl der Lehrer zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = teachers.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class TeacherViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding = TeacherItemBinding.bind(itemView)
        val teacherName = binding.teacherName

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onTeacherClick(position)
        }
    }

    interface OnItemClickListener {
        fun onTeacherClick(position: Int)
    }
}