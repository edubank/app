package com.edubank.view.teacher

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DisplayTransactionsTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUpTeacher() {
        onView(withId(R.id.txt_login_username))
            .perform(replaceText(TestData.teacherUsername))
        onView(withId(R.id.txt_login_password))
            .perform(replaceText(TestData.teacherPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        TestData.waitForServer()
    }

    /**
     * Fachansicht anzeigen - Lehrer
     * TF131
     */
    @Test
    fun test_showSubjectView() {
        // open subjects view
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        TestData.waitForServer()

        // check if subject view is opened
        onView(withId(R.id.rv_subject_students)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_students)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_class)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_subject)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_subject_editSubject)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_subject_eventManager)).check(matches(isDisplayed()))
    }

    /**
     * Transaktionsverlauf eines Schülers anzeigen - Lehrer
     * TF132
     */
    @Test
    fun test_showTransactionsOfStudent() {
        TestData.waitForServer()
        // open subjects view
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        TestData.waitForServer()

        // open new subject account view for student 0
        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        TestData.waitForServer()

        // check if recycler view for transactions is displayed
        onView(withId(R.id.recyclerTransactionView))
            .check(matches(isDisplayed()))

        // check if the student has at least one transaction shown
        onView(withId(R.id.recyclerTransactionView)).perform(
            RecyclerViewActions
                .scrollToPosition<RecyclerView.ViewHolder>(0)
        ).check(matches(isDisplayed()))

    }

    @After
    fun logout() {
        // wait just in case
        TestData.waitForServer()

        //Select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // wait for server to respond
        TestData.waitForServer()

        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}