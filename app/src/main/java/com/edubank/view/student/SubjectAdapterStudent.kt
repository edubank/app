package com.edubank.view.student

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.SubjectItemStudentBinding
import com.edubank.model.dto.response.SubjectClassResponseView
import com.edubank.model.dto.response.TransactionResponseDTO

/**
 * Adapter für die Anzeige von Fächern im SubjectOverviewStudent Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see SubjectOverviewStudent
 * @author Team EduBank
 */
class SubjectAdapterStudent(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<SubjectAdapterStudent.SubjectViewHolder>() {

    /**
     * Die Fächer, die der Adapter anzeigt.
     */
    private var subjectsView = mutableListOf<SubjectClassResponseView>()

    /**
     * Die Guthaben der jeweiligen Fächer, die der Adapter anzeigt.
     */
    private var balance = mutableListOf<Double>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SubjectViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.subject_item_student, viewGroup, false)
        return SubjectViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager)
     */
    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {

        // Fachname setzen
        val currentSubject = subjectsView[position]
        holder.subjectNameTextView.text = currentSubject.name

        // Lehrer Name setzen
        val teacherFirstName = currentSubject.teacher.firstName
        val teacherLastName = currentSubject.teacher.lastName
        val teacherUsername = currentSubject.teacher.username
        val teacherName = "$teacherFirstName $teacherLastName ($teacherUsername)"
        holder.teacherNameTextView.text = teacherName

        // Guthaben setzen
        val balanceText = "${balance[position]}€"
        holder.balanceTextView.text = balanceText

        // Farbe des Guthaben setzen
        if (balance[position] < 0) {
            holder.balanceTextView.setTextColor(Color.parseColor("#ca5e5e"))
        } else {
            holder.balanceTextView.setTextColor(Color.parseColor("#6fbf72"))
        }
    }

    /**
     * Die Methode überschreibt subjectsView und balance und benachrichtigt Android,
     * dass sich die Daten geändert haben.
     *
     * @param updatedSubjects neue Liste der Fächer
     * @param updatedBalance neue Liste der Guthaben
     */
    fun onSubjectsChanged(
        updatedSubjects: MutableList<SubjectClassResponseView>,
        updatedBalance: MutableList<Double>
    ) {
        subjectsView = updatedSubjects
        balance = updatedBalance
        notifyDataSetChanged()
    }

    /**
     * Gibt die Anzahl der Fächer zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = subjectsView.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class SubjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        private val binding = SubjectItemStudentBinding.bind(itemView)

        val subjectNameTextView = binding.subjectName
        val teacherNameTextView = binding.teacherName
        val balanceTextView = binding.saldo

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onSubjectClick(position)
        }
    }

    interface OnItemClickListener {
        fun onSubjectClick(position: Int)
    }
}