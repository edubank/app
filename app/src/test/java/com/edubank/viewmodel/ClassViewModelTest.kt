package com.edubank.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.model.retrofitclient.ClassService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ClassViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var classVM : ClassViewModel
    @get:Rule
    private val mockWebServer = MockWebServer()
    private val classAPI by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(ClassService::class.java)
    }
    private val httpClient = OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .build()
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Before
    fun setup() {
        mockWebServer.start()
        classVM = ClassViewModel()
        classVM.setService(classAPI)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getAllClasses_success() = runBlocking {
        val jsonData = getDataObject("class_response_list.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(classVM.getAllClasses() == 200)
        assertTrue(classVM.classes.value == gson.fromJson<List<SchoolClassResponseDTO>>(
            jsonData, object : TypeToken<List<SchoolClassResponseDTO>>() {}.type))
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 3)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getAllClasses_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(classVM.getAllClasses() == 400)
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getClass_success() = runBlocking {
        val jsonData = getDataObject("class_response_item.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(classVM.getClass(1) == 200)
        assertTrue(classVM.classes.value == listOf(gson.fromJson<SchoolClassResponseDTO>(
            jsonData, object : TypeToken<SchoolClassResponseDTO>() {}.type)))
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getClass_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(classVM.getClass(1) == 400)
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateClass_success() = runBlocking {
        val jsonData = getDataObject("class_response_item.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(classVM.updateClass(1, "5a") == 200)
        assertTrue(classVM.classes.value == listOf(gson.fromJson<SchoolClassResponseDTO>(
            jsonData, object : TypeToken<SchoolClassResponseDTO>() {}.type)))
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateClass_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(classVM.updateClass(1, "5a") == 400)
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun deleteClass_success() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody("\"true\""))
        assertTrue(classVM.deleteClass(1) == 200)
    }

    @Test
    fun deleteClass_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400)
            .setBody("\"false\""))
        assertTrue(classVM.deleteClass(1) == 400)
    }

    @Test
    fun createClass_success() = runBlocking {
        val jsonData = getDataObject("class_response_item.json")
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(classVM.createClass("5a") == 200)
        assertTrue(classVM.classes.value == listOf(gson.fromJson<SchoolClassResponseDTO>(
            jsonData, object : TypeToken<SchoolClassResponseDTO>() {}.type)))
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun createClass_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400))

        assertTrue(classVM.createClass("5a") == 400)
        if (classVM.classes.value != null) {
            assertTrue(classVM.classes.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }
}