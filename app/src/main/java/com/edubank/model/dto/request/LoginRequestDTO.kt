package com.edubank.model.dto.request

/**
 * LoginRequestDTO ist eine Daten-Klasse für Login-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property username Benutzername
 * @property password Passwort des Benutzers
 */
data class LoginRequestDTO(
    val username : String,
    val password : String
)
