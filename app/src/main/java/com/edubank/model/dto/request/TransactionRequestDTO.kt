package com.edubank.model.dto.request

/**
 * TransactionRequestDTO ist eine Daten-Klasse für Transactions-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property title Titel der Transaktion
 * @property amount Betrag des Transaktion
 */
data class TransactionRequestDTO(
    val title : String,
    val amount : Double
)
