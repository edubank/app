package com.edubank.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.model.dto.response.AccountTransactionsResponseDTO
import com.edubank.model.dto.response.SubjectResponseDTO
import com.edubank.model.retrofitclient.AccountService
import com.edubank.model.retrofitclient.ServiceBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

/**
 * Das ViewModel liefert die Funktionalität, die für die Verwaltung einer Kontos gebraucht werden.
 * Dazu gehört die Kommunkation mit dem Server, um Konten anzufragen oder zu bearbeiten.
 *
 * @author Team EduBank
 * @property accounts Die Read-Only Kontodaten können durch accounts gelesen werden.
 * @see AccountResponseDTO Die Kontodaten des ViewModels werden in einer Liste vom Typ AccountResponseDTO gehalten.
 * @see AccountService Retrofit-Service, der für die Kommunikation mit dem Server verwendet wird.
 */
class AccountViewModel : ViewModel() {

    /**
     * Daten-Container für die Konto-Daten. Nur _accounts kann modifiziert werden.
     * accounts macht die Konto-Daten nach außen sichtbar und enthält die gleichen Daten wie _accounts.
     */
    private var _accounts : MutableLiveData<List<AccountResponseDTO>> = MutableLiveData<List<AccountResponseDTO>>()
    var accounts : LiveData<List<AccountResponseDTO>> = _accounts
    /**
     * Retrofit-Service für Konto-Anfragen.
     */
    private var requestService = ServiceBuilder.buildService(AccountService::class.java)

    /**
     * Die setService-Funktion setzt den Account-Service. Wenn setService nicht aufgerufen wird,
     * dann wird der Default-Service gesetzt. Sonst service.
     * Das Initialisieren mit einem spezifischen Service wird für die Tests benötigt.
     */
    fun setService(service : AccountService){
        requestService = service
    }

    /**
     * Die Funktion getStudentAccounts frägt die Konten des Benutzers mit der ID userId an.
     * Die Kontodaten werden in accounts gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält accounts eine leere Liste.
     *
     * @param userId Die Benutzer-ID der Konten, die angefragt werden sollen
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getStudentAccounts(userId : Int) : Int {
        var responseCode = 0
        val call = requestService.getStudentAccounts(userId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _accounts.value = response.body()
            } else {
                _accounts.value = mutableListOf()
            }
            Log.d("AccountVM:getStudentAcc", response.message())
        } catch (error : Throwable) {
            _accounts.value = mutableListOf()
            error.message?.let { Log.e("AccountVM:getStudentAcc", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getSubjectAccounts frägt die Konten eines Faches an.
     * Die Kontodaten werden in accounts gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält accounts eine leere Liste.
     *
     * @param subjectId Die Fach-ID, dessen Konten angefragt werden sollen.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getSubjectAccounts(subjectId: Int) : Int {
        var responseCode = 0
        val call = requestService.getSubjectAccounts(subjectId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _accounts.value = response.body()
            } else {
                _accounts.value = mutableListOf()
            }
            Log.d("AccountVM:getSubjectAcc", response.message())
        } catch (error : Throwable) {
            _accounts.value = mutableListOf()
            error.message?.let { Log.e("AccountVM:getSubjectAcc", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getAccount frägt das Konto mit der ID accountId an.
     * Die Kontodaten werden in AccountResponseDTO-Objekte konvertiert und in accounts gespeichert,
     * wenn die Anfrage erfolgreich war, sonst enthält accounts eine leere Liste.
     *
     * @param accountId Die Konto-ID des Kontos, welches angefragt werden soll.
     * @return AccountTransactionResponseDTO-Objekt des Fachs, wenn die Anfrage erfolreich war, sonst null.
     * @see AccountTransactionsResponseDTO Kontodaten mit Transaktionsverlauf.
     */
    suspend fun getAccount(accountId : Int) : AccountTransactionsResponseDTO? {
        var dto : AccountTransactionsResponseDTO? = null
        val call = requestService.getAccount(accountId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            if (response.isSuccessful) {
                dto = response.body()
                val accountDTO = dto?.let { AccountResponseDTO(it.id, dto.subject, dto.student, dto.balance) }
                _accounts.value = listOfNotNull(accountDTO)
            } else {
                _accounts.value = mutableListOf()
            }
            Log.d("AccountVM:getAccount", response.message())
        } catch (error : Throwable) {
            _accounts.value = mutableListOf()
            error.message?.let { Log.e("AccountVM:getAccount", it) }
            error.printStackTrace()
        }
        return dto
    }
}