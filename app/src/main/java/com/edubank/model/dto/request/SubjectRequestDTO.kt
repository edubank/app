package com.edubank.model.dto.request

/**
 * SubjectRequestDTO ist eine Daten-Klasse für Fächer-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property name Name des Faches.
 * @property classId Id des Klasse, der das Fach zugewiesen ist.
 * @property teacherId Benutzer-Id des Lehrers des Faches
 */
data class SubjectRequestDTO(
    val name : String,
    val classId : Int,
    val teacherId : Int
)
