package com.edubank.model.dto.response

/**
 * AccountTransactionsResponseDTO ist eine Daten-Klasse für den Transaktionsverlauf eines Kontos.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Kontos
 * @property subject Fach zu dem das Konto gehört
 * @property student Eigentümer des Kontos
 * @property balance Kontostand des Kontos
 * @property transactions Transaktionsverlauf des Kontos
 */
data class AccountTransactionsResponseDTO (
    val id : Int,
    val subject : SubjectClassResponseView,
    val student : UserResponseDTO,
    val balance : Double,
    val transactions : List<TransactionResponseView>
)
