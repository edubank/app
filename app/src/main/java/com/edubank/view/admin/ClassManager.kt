package com.edubank.view.admin

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.ClassManagerFragmentBinding
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.viewmodel.ClassViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine "Klasse-Bearbeiten" Ansicht.
 *
 * @author Team EduBank
 */
class ClassManager : Fragment(), ClassAdapter.OnItemClickListener {

    private val classViewModel: ClassViewModel by activityViewModels()
    private lateinit var binding: ClassManagerFragmentBinding
    private lateinit var classList: List<SchoolClassResponseDTO>
    private lateinit var currentClass: SchoolClassResponseDTO
    private lateinit var classAdapter: ClassAdapter

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val dialogBuilder: AlertDialog.Builder? = activity?.let { AlertDialog.Builder(it) }

        binding = ClassManagerFragmentBinding.inflate(inflater, container, false)
        binding.classVM = classViewModel
        binding.lifecycleOwner = this

        // Die anfänglichen Daten zeigen
        requestClasses()

        // SetOnClickListeners
        binding.apply {
            addBtn.setOnClickListener { addClass() }
            changeBtn.setOnClickListener { updateClass() }
            deleteBtn.setOnClickListener {
                dialogBuilder!!.setMessage(getString(R.string.deleteClassConfirmation))
                    .setTitle(R.string.deleteDialogTitle)
                    .setPositiveButton(R.string.yesIAm) { _, _ -> deleteClass() }
                    .setNegativeButton(R.string.cancel) { _, _ -> }.create().show()
            }
        }

        //RecyclerView aufbauen
        classAdapter = ClassAdapter(this)
        binding.recyclerClassView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = classAdapter
            setHasFixedSize(true)
        }

        return binding.root
    }

    /**
     * Eine neue Klasse wird erstellt.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    private fun addClass() {
        val name = binding.newClassEdit.text.toString()

        if (name.isEmpty()) {
            Snackbar.make(requireView(), getString(R.string.enterNameToast), Snackbar.LENGTH_LONG).show()
        } else {
            lifecycleScope.launch {
                classViewModel.createClass(name)

                requestClasses()

                resetFields()
                closeKeyBoard(binding.newClassEdit)
            }
        }
    }

    /**
     * Eine Klasse wird geändert.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    private fun updateClass() {
        val newName = binding.updateClassEdit.text.toString()
        val selectedClass = binding.selectedClass.text.toString()

        when {
            selectedClass == getString(R.string.none) -> {
                Snackbar.make(requireView(), getString(R.string.selectClassToast), Snackbar.LENGTH_LONG).show()
            }
            newName.isEmpty()                         -> {
                Snackbar.make(requireView(), getString(R.string.enterNameToast), Snackbar.LENGTH_LONG).show()
            }
            else                                      -> {
                lifecycleScope.launch {
                    classViewModel.updateClass(currentClass.id, newName)

                    requestClasses()

                    resetFields()
                    closeKeyBoard(binding.updateClassEdit)
                }
            }
        }
    }

    /**
     * Eine Klasse wird gelöscht.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    private fun deleteClass() {
        val selectedClass = binding.selectedClass.text.toString()

        if (selectedClass == getString(R.string.none)) {
            Snackbar.make(requireView(), getString(R.string.selectClassToast), Snackbar.LENGTH_LONG).show()
        } else {
            lifecycleScope.launch {
                classViewModel.deleteClass(currentClass.id)

                requestClasses()

                resetFields()
                closeKeyBoard(binding.updateClassEdit)
            }
        }
    }

    /**
     * Eine Liste mit allen Klassen wird vom Server angefordert,
     * alphabetisch sortiert und in der Recyclerview des Spinners gezeigt.
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun requestClasses() {
        lifecycleScope.launch {
            classViewModel.getAllClasses()
            classList = classViewModel.classes.value!!.sortedBy { it.name.lowercase() }
            classAdapter.classes = classList.toMutableList()
            classAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Der Name der ausgewählten Klasse wird auf der Ansicht im entsprechenden Feld gezeigt.
     */
    override fun onItemClick(position: Int) {
        lifecycleScope.launch {

            currentClass = classList[position]
            classViewModel.getClass(currentClass.id)

            binding.selectedClass.text = classViewModel.classes.value!![0].name
            binding.updateClassEdit.setText(classViewModel.classes.value!![0].name)
        }
    }

    /**
     * Diese Methode schließt die Tastatur.
     */
    private fun closeKeyBoard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /**
     *Die Felder für Klassenbearbeitung und -aktualisierung zurücksetzen.
     */
    private fun resetFields() {
        binding.newClassEdit.apply {
            text.clear()
            clearFocus()
        }
        binding.updateClassEdit.apply {
            text.clear()
            clearFocus()
        }
        binding.selectedClass.text = getString(R.string.none)
    }
}