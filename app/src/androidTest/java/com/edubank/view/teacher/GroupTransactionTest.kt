package com.edubank.view.teacher

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.studentName
import com.edubank.view.TestData.Companion.testSubject
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.AccountAdapter
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers
import org.junit.*
import kotlin.random.Random

class GroupTransactionTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Lehrer wird für die Tests eingeloggt und die Fachansicht geöffnet.
     */
    @Before
    fun setUpTeacher() {
        // login as teacher
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.teacherUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.teacherPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        // wait for server to respond
        waitForServer()

        // open test subject first
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItem<SubjectAdapterTeacher.SubjectViewHolder>(
                hasDescendant(withText(testSubject)), click()
            )
        )
        waitForServer()

        // open Event Manager
        onView(withId(R.id.btn_subject_eventManager)).perform(click())

        // wait for server to respond
        waitForServer()
    }

    /**
     * Öffnet den Event-Manager und überprüft, ob die Ansicht übereinstimmt.
     * TF111
     */
    @Test
    fun test_openEventManager() {
        // check if event manager view is displayed as result
        onView(withId(R.id.lbl_eventManager_title)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_eventManager_students)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_eventManager_createEvent)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_eventManager_students)).check(matches(isDisplayed()))
        onView(withId(R.id.cb_eventManager_selectAll)).check(matches(isDisplayed()))
        onView(withId(R.id.cb_eventManager_deposit)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_eventManager_amount)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_eventManager_amount)).check(matches(isDisplayed()))
        onView(withId(R.id.txt_eventManager_eventName)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_eventManager_eventName)).check(matches(isDisplayed()))
    }

    /**
     * Erstellt eine Event Abbuchen-Transaktion als Lehrer und überprüft, ob die Transaktion erfolgreich war.
     * Es wird der Test-Schüler ausgewählt.
     * TF112-a-single
     */
    @Test
    fun test_createEventTransactionOne() {
        val eventID = "TSingleTx_${Random.Default.nextInt(100000, 999999)}"
        val amount = Random.Default.nextInt(250)

        // name Event and enter amount
        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$amount"))

        // pick test student
        onView(Matchers.allOf(withId(R.id.cb_studentItem_item), withText(studentName))).perform(
            click()
        )
        // create tx event
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())

        waitForServer()

        // check if event was created
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.successfulEventCreation)))

        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions.actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(studentName)), click()
            )
        )

        waitForServer()

        onView(Matchers.allOf(withId(R.id.transactionTitle), withText(eventID))).check(
            matches(
                isDisplayed()
            )
        )
        onView(Matchers.allOf(withId(R.id.saldo), withText("${amount * -1}.0€"))).check(
            (matches(
                isDisplayed()
            ))
        )


    }


    /**
     * Erstellt eine Event Abbuchen-Transaktion als Lehrer und überprüft, ob die Transaktion erfolgreich war.
     * Es werden alle Schüler ausgewählt.
     * TF112-a-all
     */
    @Test
    fun test_createEventTransactionAll() {
        val eventID = "TGroupTx_${Random.Default.nextInt(100000, 999999)}"
        val amount = Random.Default.nextInt(250)

        // name Event and enter amount
        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$amount"))

        // pick all students
        onView(withId(R.id.cb_eventManager_selectAll)).perform(click())

        // create group tx event
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())

        waitForServer()

        // check if event was created
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.successfulEventCreation)))

        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions.actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(studentName)), click()
            )
        )

        waitForServer()

        onView(Matchers.allOf(withId(R.id.transactionTitle), withText(eventID))).check(
            matches(
                isDisplayed()
            )
        )
        onView(Matchers.allOf(withId(R.id.saldo), withText("${amount * -1}.0€"))).check(
            (matches(
                isDisplayed()
            ))
        )
    }

    /**
     * Erstellt eine Event Einzahlen-Transaktion als Lehrer und überprüft, ob die Transaktion erfolgreich war.
     * Es wird der Test-Schüler ausgewählt.
     * TF112-b-single
     */
    @Test
    fun test_createEventDepositSingle() {
        val eventID = "TSingleDeposit_${Random.Default.nextInt(1000000, 9999999)}"
        val amount = Random.Default.nextInt(250)


        // name Event and enter amount
        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$amount"))

        // pick all students, deposit checkmark
        // pick test student
        onView(Matchers.allOf(withId(R.id.cb_studentItem_item), withText(studentName))).perform(
            click()
        )

        // pick deposit, create event
        onView(withId(R.id.cb_eventManager_deposit)).perform(click())
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())

        waitForServer()

        // check if event was created
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.successfulEventCreation)))

        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions.actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(studentName)), click()
            )
        )

        waitForServer()

        onView(Matchers.allOf(withId(R.id.transactionTitle), withText(eventID))).check(
            matches(
                isDisplayed()
            )
        )
        onView(Matchers.allOf(withId(R.id.saldo), withText("${amount}.0€"))).check(
            (matches(
                isDisplayed()
            ))
        )
    }


    /**
     * Erstellt eine Event Einzahlen-Transaktion als Lehrer und überprüft, ob die Transaktion erfolgreich war.
     * Es werden alle Schüler ausgewählt.
     * TF112-b-all
     */
    @Test
    fun test_createEventDepositAll() {
        val eventID = "TGroupDeposit_${Random.Default.nextInt(1000000, 9999999)}"
        val amount = Random.Default.nextInt(250)

        // name Event and enter amount
        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$amount"))

        // pick all students, deposit checkmark
        onView(withId(R.id.cb_eventManager_selectAll)).perform(click())
        onView(withId(R.id.cb_eventManager_deposit)).perform(click())
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())

        waitForServer()

        // check if event was created
        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.successfulEventCreation)))

        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions.actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(studentName)), click()
            )
        )

        waitForServer()

        onView(Matchers.allOf(withId(R.id.transactionTitle), withText(eventID))).check(
            matches(
                isDisplayed()
            )
        )
        onView(Matchers.allOf(withId(R.id.saldo), withText("${amount}.0€"))).check(
            (matches(
                isDisplayed()
            ))
        )
    }


    @After
    fun logout() {
        // wait just in case
        waitForServer()

        //Select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // wait for server to respond
        waitForServer()

        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}