package com.edubank.view.student

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DisplaySubjectAccountTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        // Login as student
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.studentUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.studentPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        TestData.waitForServer()
    }

    @Test
    fun displaySubjectAccount() {
        //Click on the first subject from the recycler view
        onView(withId(R.id.recyclerSubjectView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )

        //Wait for server response
        TestData.waitForServer()

        //Check if the due fragment is displayed
        onView(withId(R.id.subject_account_fragment)).check(matches(isDisplayed()))

        //Check if all elements for the activity are displayed
        onView(withId(R.id.subjectName)).check(matches(isDisplayed()))
        onView(withId(R.id.headerRoleName)).check(matches(isDisplayed()))
        onView(withId(R.id.balanceCaption)).check(matches(isDisplayed()))
        onView(withId(R.id.history)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerTransactionView)).check(matches(isDisplayed()))
    }

}