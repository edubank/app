package com.edubank.view.shared

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.edubank.R
import com.edubank.view.admin.ClassOverview
import com.edubank.view.admin.UserOverview

/**
 * Die Klasse MainActivity erzeugt das Gerüst der Benutzeroberfläche und das Menü. Sie ist der Eintrittspunkt der App.
 *
 * @author Team EduBank
 */
class MainActivity : AppCompatActivity() {

    //Contains information about the visibility of the menu and its items
    private var isMenuVisible = false
    private var userIsAdmin = false

    /**
     * Einstiegspunkt der App. Die Methode tauscht die aktuelle Ansicht mit der Login-Ansicht aus.
     *
     * @param savedInstanceState Die gespeicherten Zustände, falls die App bereits initialisiert wurde und Daten
     * gespeichert hat.
     */
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace<Login>(R.id.main_container)
            }
        }
    }

    /**
     * Fügt der App das gegebene Menü hinzu.
     *
     * @param menu Das Menü, das hinzugefügt werden soll.
     * @return true, in jedem Fall.
     */
    override fun onCreateOptionsMenu(menu : Menu?) : Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    /**
     * Realisiert einen onClick-Listener für das Menü. Hier wird einem Menüeintrag Funktionalität verliehen. Das Menü
     * hat folgende Einträge: Einstellungen, Logout und zusätzlich für einen Administrator Klassenübersicht und
     * Benutzerübersicht.
     *
     * Wird ein Eintrag angeklickt, so wird die entsprechende Ansicht (Settings, Login, ClassOverview oder
     * UserOverview) angezeigt. Bevor der Login-Screen angezeigt wird, wird der angemeldete Benutzer ausgeloggt.
     *
     * @param item Das MenuItem, das ausgewählt wurde.
     * @return true, wenn die gewählte Operation durchgeführt werden konnte.
     */
    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        val displayedFragment = supportFragmentManager.findFragmentById(R.id.main_container)
        when (item.itemId) {
            R.id.settings       -> {
                if (displayedFragment !is Settings) {
                    supportFragmentManager.beginTransaction().replace(R.id.main_container, Settings())
                        .addToBackStack(null).commit()
                    Log.i("Menu", "Settings selected")
                    return true
                }
            }
            R.id.logout         -> {
                toggleMenuVisibility()
                supportFragmentManager.beginTransaction().replace(R.id.main_container, Login()).commit()
                Log.i("Menu", "Logout selected")
                return true
            }
            R.id.user_overview  -> {
                if (displayedFragment !is UserOverview) {
                    supportFragmentManager.beginTransaction().replace(R.id.main_container, UserOverview())
                        .addToBackStack(null).commit()
                    Log.i("Menu", "UserOverview selected")
                    return true
                }
            }
            R.id.class_overview -> {
                if (displayedFragment !is ClassOverview) {
                    supportFragmentManager.beginTransaction().replace(R.id.main_container, ClassOverview())
                        .addToBackStack(null).commit()
                    Log.i("Menu", "ClassOverview selected")
                    return true
                }
            }
        }
        return false
    }

    /**
     * Hier kann das Menü verändert/aktualisiert werden. Wenn isMenuVisible wahr ist, so wird das Menü angezeigt. Ist
     * zusätzlich noch userIsAdmin wahr (der angemeldete Benutzer ist also ein Administrator), so werden auch die
     * Menüeinträge angezeigt, die nur für diese Rolle sichtbar sind.
     *
     * @param menu Das Menü, das angezeigt/aktualisiert werden soll.
     * @return true, als Bestätigung.
     */
    override fun onPrepareOptionsMenu(menu : Menu?) : Boolean {
        menu?.let {
            it.findItem(R.id.settings).isVisible = isMenuVisible
            it.findItem(R.id.logout).isVisible = isMenuVisible
            it.findItem(R.id.class_overview).isVisible = userIsAdmin && isMenuVisible
            it.findItem(R.id.user_overview).setVisible(userIsAdmin && isMenuVisible)
        }
        return true
    }

    /**
     * Wechselt die Sichtbarkeit des Menüs. Das heißt falls das Menü aktuell sichtbar ist, ist das nach dem Aufruf
     * nicht mehr der Fall und umgekehrt. Die Ansicht wird dementsprechend bei jedem Aufruf auch aktualisiert.
     *
     * @see onPrepareOptionsMenu
     */
    fun toggleMenuVisibility() {
        isMenuVisible = !isMenuVisible
        invalidateOptionsMenu()
    }

    /**
     * Setzt den aktuellen Status des angemeldeten Benutzers. Die Sicht wird bei Änderungen entsprechend aktualisiert.
     *
     * @param isAdmin Entscheidet, ob der angemeldete Benutzer Zugriff auf Administrator-Ansichten bekommt.
     * @see onPrepareOptionsMenu
     */
    fun setAdminStatus(isAdmin : Boolean) {
        userIsAdmin = isAdmin
        invalidateOptionsMenu()
    }

    companion object {

        /**
         * Überprüft, ob der gegebene String eine E-Mail-Adresse sein kann.
         *
         * @return true, wenn der String eine E-Mail-Adresse sein kann.
         */
        fun isEmail(email : String) : Boolean {
            return Regex(".+@.+\\...+").matches(email)
        }
    }
}