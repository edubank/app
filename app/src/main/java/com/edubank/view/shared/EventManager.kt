package com.edubank.view.shared

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.EventManagerFragmentBinding
import com.edubank.model.dto.request.EventRequestDTO
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.viewmodel.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment stellt den Screen zum Anlegen eines Events mit den benötigten Funktionalitäten zur Verfügung.
 *
 * Wichtig: Beim Aufruf des EventManagers muss das entsprechende Subject im SubjectViewModel bei subjects an erster
 * Stelle stehen.
 *
 * @author EduBank
 */
class EventManager : Fragment() {

    private val transactionVM: TransactionViewModel by activityViewModels()
    private val subjectVM: SubjectViewModel by activityViewModels()
    private val accountVM: AccountViewModel by activityViewModels()
    private val classVM: ClassViewModel by activityViewModels()
    private val userVM: UserViewModel by activityViewModels()

    private val studentAdapter = StudentAdapter(this)

    private lateinit var binding: EventManagerFragmentBinding
    private lateinit var accountsMap: MutableMap<Int, AccountResponseDTO>

    private var selectedAccountIDs = mutableListOf<Int>()
    private var subjectID = -1

    /**
     * Die Methode wird aufgerufen, wenn das Fragment neu gebaut wird. Hier werden die Listener für die UI-Elemente
     * festgelegt und die View an das ViewModel per DataBinding angebunden.
     */
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = EventManagerFragmentBinding.inflate(inflater, container, false)
        binding.transactionVM = transactionVM
        binding.accountVM = accountVM
        binding.subjectVM = subjectVM
        binding.classVM = classVM
        binding.userVM = userVM
        binding.lifecycleOwner = this

        val subject = subjectVM.subjects.value!![0]
        subjectID = subject.id

        binding.apply {
            val subjectName = "${subject.schoolClass.name} - ${subject.name}"
            lblEventManagerSubject.text = subjectName

            btnEventManagerCreateEvent.setOnClickListener {
                lifecycleScope.launch {
                    createEvent()
                }
            }

        }

        binding.rvEventManagerStudents.apply {
            layoutManager = LinearLayoutManager(this@EventManager.context)
            adapter = studentAdapter
        }
        updateStudentList()

        binding.cbEventManagerSelectAll.setOnClickListener {
            val isChecked = binding.cbEventManagerSelectAll.isChecked
            if (isChecked) {
                selectedAccountIDs = accountsMap.keys.toMutableList()
            } else {
                selectedAccountIDs.clear()
            }

            studentAdapter.selectedAllStudents = isChecked
            studentAdapter.notifyDataSetChanged()
        }

        return binding.root
    }

    /**
     * Die Methode verwendet das TransactionViewModel, um ein neues Event zu erstellen.
     *
     * @see TransactionViewModel.addEvent
     */
    private suspend fun createEvent() {
        var eventName = binding.txtEventManagerEventName.text.toString()
        var amount = binding.txtEventManagerAmount.text.toString().toDoubleOrNull()
        val deposit = binding.cbEventManagerDeposit.isChecked

        if (binding.cbEventManagerSelectAll.isChecked) {
            selectedAccountIDs = accountsMap.keys.toMutableList()
        }

        if ((eventName.isBlank() && !deposit) || amount == null || amount == 0.0 || selectedAccountIDs.isEmpty()) {
            Snackbar.make(requireView(), R.string.invalidData, Snackbar.LENGTH_LONG).show()
            return
        }

        if (deposit) {
            if (eventName.isBlank()) eventName = getString(R.string.deposit)
        } else {
            amount *= -1
        }

        transactionVM.addEvent(EventRequestDTO(eventName, selectedAccountIDs, amount))
        Snackbar.make(requireView(), R.string.successfulEventCreation, Snackbar.LENGTH_SHORT).show()

        requireActivity().supportFragmentManager.popBackStack() //removes the EventManager
        requireActivity().supportFragmentManager.beginTransaction().replace(
            R.id.main_container, Subject()).addToBackStack(null).commit()
    }


    /**
     * Die Funktion wird vom Adapter der RecyclerView aufgerufen um die Schüler zu markieren, die ausgewählt wurden,
     * bzw. wieder abzuwählen.
     *
     * @see StudentAdapter
     */
    fun wasClicked(accountID: Int, isChecked: Boolean) {
        if (isChecked && !selectedAccountIDs.contains(accountID)) {
            selectedAccountIDs.add(accountID)
            Log.d("EventManager:", "Added account with ID $accountID")

            if (selectedAccountIDs == accountsMap.keys.toMutableList()) binding.cbEventManagerSelectAll.isChecked = true

        } else if (!isChecked) {
            selectedAccountIDs.remove(accountID)
            binding.cbEventManagerSelectAll.isChecked = false
            Log.d("EventManager:", "Added account with ID $accountID")
        }
    }

    /**
     * Lädt die Schüler des entsprechenden Fachs für die Anzeige.
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun updateStudentList() {
        lifecycleScope.launch {
            accountVM.getSubjectAccounts(subjectID)
            val accMap = mutableMapOf<Int, AccountResponseDTO>()

            for (account in accountVM.accounts.value!!) {
                accMap[account.id] = account
            }

            accountsMap = accMap
            studentAdapter.accounts = accountsMap.values.sortedBy { it.student.lastName }.toMutableList()
            studentAdapter.notifyDataSetChanged()
        }
    }

}