package com.edubank.model.retrofitclient

import com.edubank.model.dto.request.SubjectRequestDTO
import com.edubank.model.dto.response.AccountResponseView
import com.edubank.model.dto.response.SubjectResponseDTO
import retrofit2.Call
import retrofit2.http.*

/**
 * Scnittstellen-Beschreibung für den Retrofit-Service SubjectService.
 * Die Schnittstelle beschreibt die Fächer-Anfragen an den Server.
 *
 * @author Team EduBank
 */
interface SubjectService {

    /**
     * Die Funktion getAllSubjects frägt die Daten aller Faches an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @return Call-Objekt-Liste mit den Daten aller Fächer
     */
    @GET("subjects")
    fun getAllSubjects() : Call<List<SubjectResponseDTO>>

    /**
     * Die Funktion getSubjectsOfTeacher frägt alle Fächer eines Lehrers an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param teacherId Die ID des Lehrers, dessen Fächer-Daten angefragt werden sollen.
     * @return Call-Objekt-Liste mit den Fächer-Daten
     */
    @GET("subjects")
    fun getSubjectsOfTeacher(@Query("teacher_id") teacherId : Int) : Call<List<SubjectResponseDTO>>

    /**
     * Die Funktion getSubjectById frägt die Daten eines Faches an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subjectId Die ID des Faches, welches angefragt werden soll.
     * @return Call-Objekt mit den Fach-Daten
     */
    @GET("subjects/{id}")
    fun getSubjectById(@Path("id") subjectId : Int) : Call<SubjectResponseDTO>

    /**
     * Die Funktion getSubjectByClass frägt die Fächerdaten einer Klasse beim Server an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param classId Die Klassen-ID, dessen Fächer angefragt werden sollen.
     * @return Call-Objekt mit einer Fach-Daten-Liste.
     */
    @GET("subjects")
    fun getSubjectByClass(@Query("class_id") classId : Int) : Call<List<SubjectResponseDTO>>

    /**
     * Die Funktion createSubject erstellt ein Fach auf dem Server.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subject Die Fach-Daten des Faches das neu erstellt werden soll.
     * @return Call-Objekt mit den Fach-Daten
     */
    @POST("subjects")
    fun createSubject(@Body subject : SubjectRequestDTO) : Call<SubjectResponseDTO>

    /**
     * Die Funktion updateSubject aktualisert ein Fach auf dem Server.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subjectId Die ID des Faches, das aktualisert werden soll.
     * @param subject Die aktualisierten Fach-Daten.
     * @return Call-Objekt mit den Fach-Daten
     */
    @PUT("subjects/{id}")
    fun updateSubject(@Path("id") subjectId : Int, @Body subject : SubjectRequestDTO) : Call<SubjectResponseDTO>

    /**
     * Die Funktion addStudents fügt einem Fach Schüler hinzu und erhält als Antwort vom Server
     * die Konten aller Schüler in dem Fach.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subjectId Die ID des Faches, dem die Schüler hinzugefügt werden sollen.
     * @param studentIds Liste mit den Benutzer-IDs der Schüler.
     * @return Call-Objekt mit einer AccountResponseView-Liste mit den Konten der Schüler des Faches.
     */
    @POST("subjects/{id}/students")
    fun addStudent(@Path("id") subjectId : Int, @Body studentIds : List<Int>) : Call<List<AccountResponseView>>

    /**
     * Die Funktion dropStudents entfernt Schüler aus einem Fach und erhält als Antwort vom Server
     * die Konten der verbleibenden Schüler in dem Fach.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subjectId Die ID des Faches, aus dem die Schüler entfernt werden sollen.
     * @param studentIds Liste mit den Benutzer-IDs der Schüler.
     * @return Call-Objekt mit einer AccountResponseView-Liste mit den Konten der Schüler des Faches.
     */
    @HTTP(method = "DELETE", path = "subjects/{id}/students", hasBody = true)
    fun dropStudent(@Path("id") subjectId : Int, @Body studentIds : List<Int>) : Call<List<AccountResponseView>>

    /**
     * Die Funktion deleteSubject löscht ein Fach.
     *
     * @param subjectId Die ID des Faches, das gelöscht werden soll.
     * @return Call-Objekt mit einem Boolean im Body, welcher den Erfolg der Löschanfrage angibt.
     */
    @DELETE("subjects/{id}")
    fun deleteSubject(@Path("id") subjectId : Int) : Call<Boolean>

}