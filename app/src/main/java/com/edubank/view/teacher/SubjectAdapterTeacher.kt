package com.edubank.view.teacher

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.SubjectItemTeacherBinding
import com.edubank.model.dto.response.SubjectResponseDTO

/**
 * Adapter für die Anzeige von Fächern im SubjectOverviewTeacher-Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see SubjectOverviewTeacher
 * @property subjects Liste mit alle Fächer die in der RecyclerView gezeigt werden
 * @author Team EduBank
 */
class SubjectAdapterTeacher(private val listener: OnItemClickListener):  RecyclerView.Adapter<SubjectAdapterTeacher.SubjectViewHolder>(){

    var subjects = mutableListOf<SubjectResponseDTO>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SubjectViewHolder {

        // Erstellt eine neue Ansicht, die die Benutzeroberfläche des Listenelements definiert
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.subject_item_teacher, viewGroup, false)

        return SubjectViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager)
     */
    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {

        // Hole das Element an dieser Position
        // und ersetze den Inhalt der Ansicht durch dieses Element
        val currentClass = subjects[position].schoolClass
        val currentSubject = subjects[position]

        holder.className.text = currentClass.name
        holder.subjectName.text = currentSubject.name
    }

    /**
     * Gibt die Anzahl der Fächer zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = subjects.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class SubjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding = SubjectItemTeacherBinding.bind(itemView)

        val className = binding.className
        val subjectName = binding.subjectName

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onSubjectClick(position)
        }
    }

    interface OnItemClickListener {
        fun onSubjectClick(position: Int)
    }
}