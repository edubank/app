package com.edubank.view.scenarios

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers.allOf
import org.junit.*
import kotlin.random.Random

/**
 * Beinhaltet Tests zu den Lehrer-Szenarien aus dem Pflichtenheft im Abschnitt 8.2.
 *
 * @author Team EduBank
 */
class TeacherScenariosTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Meldet den Test Lehrer an.
     */
    @Before
    fun login() { // login as teacher
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.teacherUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.teacherPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()
    }

    /**
     * Prüft, ob das Szenario "Schüler einem Konto hinzufügen" funktioniert.
     */
    @Test
    fun test_addStudentToSubject() { // select the first subject
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click())
        )

        // click "Fach bearbeiten"
        onView(withId(R.id.btn_subject_editSubject)).perform(click())

        // add the test student for this
        onView(withId(R.id.addTab)).perform(click())

        waitForServer()

        onView(
            allOf(withId(R.id.cb_studentItem_item), withText("${TestData.addStudent} (${TestData.addStudentUsername})"))
        ).perform(click())
        onView(withId(R.id.confirmButton)).perform(click())

        // return to subject
        pressBack()

        // check if student has been added
        onView(allOf(withId(R.id.lbl_accountItem_name), withText(TestData.addStudent))).check(matches(isDisplayed()))

        // remove the student
        onView(withId(R.id.btn_subject_editSubject)).perform(click())
        onView(withId(R.id.deleteTab)).perform(click())

        waitForServer()

        onView(
            allOf(withId(R.id.cb_studentItem_item), withText("${TestData.addStudent} (${TestData.addStudentUsername})"))
        ).perform(click())
        waitForServer(2000) // wait for the snackbar to disappear
        onView(withId(R.id.confirmButton)).perform(click())
    }

    /**
     * Prüft, ob das Szenario "Geldeinzug: Lektüren Anschaffung" funktioniert.
     */
    @Test
    fun test_createEventAndDeposit() {
        val eventID = "E${Random.Default.nextInt(1000000, 9999999)}"
        val eventAmount = Random.Default.nextInt(100)
        val depositID = "D${Random.Default.nextInt(1000000, 9999999)}"
        val depositAmount = Random.Default.nextInt(100)

        // select the first subject
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click())
        )

        // click "event manager"
        onView(withId(R.id.btn_subject_eventManager)).perform(click())

        // create event for all students
        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$eventAmount"))
        onView(withId(R.id.cb_eventManager_selectAll)).perform(click())
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())

        waitForServer()

        // select first account
        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click())
        )

        waitForServer()

        // check event has been added
        onView(allOf(withId(R.id.transactionTitle), withText(eventID))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.saldo), withText("${eventAmount * -1}.0€"))).check((matches(isDisplayed())))

        // make deposit
        onView(withId(R.id.transactionButton)).perform(click())
        onView(withId(R.id.transactionName)).perform(replaceText(depositID))
        onView(withId(R.id.transactionAmount)).perform(replaceText("$depositAmount"))
        onView(withId(R.id.cb_transaction_deposit)).perform(click())
        onView(withId(R.id.commitButton)).perform(click())

        waitForServer()

        // check if deposit has been added
        onView(allOf(withId(R.id.transactionTitle), withText(depositID))).check(
            matches(
                isDisplayed()
            )
        )
        onView(allOf(withId(R.id.saldo), withText("$depositAmount.0€"))).check(
            (matches(
                isDisplayed()
            ))
        )

        // logout
        Espresso.openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}