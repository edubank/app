package com.edubank.view.shared

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import org.junit.*

/**
 * Beinhaltet allgemeine UI-Tests für ForgotPassword.
 *
 * @see ForgotPassword
 * @author Team EduBank
 */
class ForgotPasswordTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Navigiert zum Fragment ForgotPassword.
     */
    @Before
    fun setUp() {
        onView(withId(R.id.btn_login_forgotPassword)).perform(click())
    }

    /**
     * Prüft, ob das Fragment richtig angezeigt wird.
     */
    @Test
    fun test_fragmentIsDisplayedCorrectly() { //check if all elements are displayed
        onView(withId(R.id.lbl_forgotPassword_title)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.lbl_forgotPassword_subtitle)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.txt_forgotPassword_email)).check(matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.btn_forgotPassword_requestPassword)).check(matches(ViewMatchers.isDisplayed()))
    }

    /**
     * Prüft, ob beim Anfordern eines neuen Passworts ohne eine E-Mail-Adresse die Snackbar mit dem richtigen Text
     * angezeigt wird.
     */
    @Test
    fun test_noEmail() {
        onView(withId(R.id.btn_forgotPassword_requestPassword)).perform(click())

        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.invalidEmail)))
    }

    /**
     * Prüft, ob beim Anfordern eines neuen Passworts ohne eine richtige E-Mail-Adresse die Snackbar mit dem richtigen
     * Text angezeigt wird.
     */
    @Test
    fun test_incorrectEmail() {
        onView(withId(R.id.txt_forgotPassword_email)).perform(replaceText("test@edubank"))
        onView(withId(R.id.btn_forgotPassword_requestPassword)).perform(click())

        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.invalidEmail)))
    }

    /**
     * Prüft, ob beim Anfordern eines neuen Passworts mit einer richtigen E-Mail-Adresse die Snackbar mit dem richtigen
     * Text angezeigt wird.
     */
    @Test
    fun test_correctEmail() {
        onView(withId(R.id.txt_forgotPassword_email)).perform(replaceText("test@edubank.de"))
        onView(withId(R.id.btn_forgotPassword_requestPassword)).perform(click())

        onView(withId(R.id.snackbar_text)).check(matches(withText(R.string.newPasswordSent)))
    }
}

