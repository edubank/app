package com.edubank.model.dto.response

/**
 * SchoolClassResponseDTO ist eine Daten-Klasse für Klassen-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id der Klasse
 * @property name Name des Klasse
 * @property subjects Fächer der Klasse
 */
data class SchoolClassResponseDTO (
    val id : Int,
    val name : String,
    val subjects : List<SubjectResponseView>
)
