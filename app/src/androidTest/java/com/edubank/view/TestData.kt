package com.edubank.view

/**
 * Dient ausschließlich zum Bereitstellen eines companion objects mit Werten und Methoden, die zum Testen benötigt werden.
 */
class TestData {

    /**
     * Stellt Methoden und Werte zum Testen bereit.
     */
    companion object {

        const val adminName = "Admin"
        const val adminUsername = "admin"
        const val adminPassword = "admin"
        const val adminEmail = "admin@edubank.de"
        const val adminNewPassword = "admin1"
        const val adminNewEmail = "admin@edubank.com"

        const val teacherFirstName = "Test"
        const val teacherLastName = "Lehrer"
        const val teacherName = "$teacherFirstName $teacherLastName"
        const val teacherUsername = "tudgt"
        const val teacherPassword = "tudgt"
        const val teacherEmail = "teacher@edubank.de"

        const val studentFirstName = "Test"
        const val studentLastName = "Schüler"
        const val studentName = "$studentFirstName $studentLastName"
        const val studentUsername = "srijd"
        const val studentPassword = "srijd"
        const val studentEmail = "student@kit.edu"

        const val testClass = "Test Klasse"
        const val deleteClass = "0K"
        const val changedClass = "0C"

        const val testSubject = "Test Fach"
        const val deleteSubject = "0F"

        const val addStudent = "Test 0First"
        const val addStudentUsername = "sttom"

        const val deleteStudentFirstName = "Delete"
        const val deleteStudentLastName = "Student"
        const val deleteStudentLastNameChanged = "ThisStudent"


        /**
         * Lässt den ausführenden Thread für ein gewisse Zeit warten, bis die Ausführung fortgesetzt wird.
         * Der Wert kann und muss entsprechend der Verbindung zum Server angepasst werden.
         *
         * @param time die Zeit, die gewartet werden soll in Millisekunden.
         */
        fun waitForServer(time : Long = 500) {
            Thread.sleep(time)
        }
    }
}