package com.edubank.model.dto.response

import java.util.*

/**
 * TransactionResponseDTO ist eine Daten-Klasse für Transaktions-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id der Transaktion
 * @property account Konto auf dem die Transaktion durchgeführt wird.
 * @property title Titel der Transaktion
 * @property amount Betrag der Transaktion
 * @property initiator Initiator der Transaktion
 * @property createdAt Zeitpunkt der Durchführung der Transaktion
 */
data class TransactionResponseDTO (
    val id : Int,
    val account : AccountResponseDTO,
    val title : String,
    val amount : Double,
    val initiator : UserResponseDTO,
    val createdAt : Date
)
