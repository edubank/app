package com.edubank.model.dto.request

/**
 * EventRequestDTO ist eine Daten-Klasse für Event-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property title Titel des Events
 * @property accountIds Die Konto-Ids des Schüler, die am Event beteiligt sind.
 * @property amount Der Überweisungsbetrag
 */
data class EventRequestDTO (
    val title : String,
    val accountIds : List<Int>,
    val amount : Double
)
