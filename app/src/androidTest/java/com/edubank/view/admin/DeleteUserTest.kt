package com.edubank.view.admin

import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DeleteUserTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        // Login
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())


        // Select "Manage users"
        TestData.waitForServer()
        onView(withId(R.id.userManagerButton)).perform(click())

        //Add User to delete
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText("To"))
        onView(withId(R.id.lastNameEdit)).perform(replaceText("Delete"))
        onView(withId(R.id.addBtn)).perform(click())

        // Select User
        TestData.waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
    }

    // Delete User
    @Test
    fun deleteUser() {
        onView(withId(R.id.deleteUserBtn)).perform(click())
        // Dialog click confirm
        onView(withId(android.R.id.button1)).perform(click())

        // Check if user is deleted
        TestData.waitForServer()
        onView(withId(R.id.recyclerUserView)).perform(
            actionOnItemAtPosition<ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.lastNameEdit)).check(matches(withText(not(containsString("Delete")))))
    }
}

