package com.edubank.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edubank.model.dto.request.SubjectRequestDTO
import com.edubank.model.dto.response.AccountResponseView
import com.edubank.model.dto.response.SubjectResponseDTO
import com.edubank.model.retrofitclient.ServiceBuilder
import com.edubank.model.retrofitclient.SubjectService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

/**
 * Das ViewModel liefert die Funktionalität, die für die Verwaltung eines Faches gebraucht werden.
 * Dazu gehört die Kommunkation mit dem Server, um Fächer anzufragen oder zu bearbeiten.
 *
 * @author Team EduBank
 * @property service Dem ViewModel kann ein SubjectService übergeben werden., wenn ein spezieller Service gebraucht wird (Andernfalls wird der Default-Service verwendet).
 * @property subjects Die Read-Only Fächerdaten können durch subjects gelesen werden.
 * @see SubjectResponseDTO Die Fächerdaten des ViewModels werden in einer Liste vom Typ SubjectResponseDTO gehalten.
 * @see SubjectService Retrofit-Service, der für die Kommunikation mit dem Server verwendet wird.
 */
class SubjectViewModel : ViewModel() {

    /**
     * Daten-Container für die Fächer-Daten. Nur _subjects kann modifiziert werden.
     * subjects macht die Fächer-Daten nach außen sichtbar und enthält die gleichen Daten wie _subjects.
     */
    private var _subjects : MutableLiveData<List<SubjectResponseDTO>> = MutableLiveData<List<SubjectResponseDTO>>()
    var subjects : LiveData<List<SubjectResponseDTO>> = _subjects
    /**
     * Retrofit-Service für Fächer-Anfragen.
     */
    private var requestService = ServiceBuilder.buildService(SubjectService::class.java)

    /**
     * Die setService-Funktion setzt den Subject-Service. Wenn setService nicht aufgerufen wird,
     * dann wird der Default-Service gesetzt. Sonst service.
     * Das Initialisieren mit einem spezifischen Service wird für die Tests benötigt.
     */
    fun setService(service : SubjectService){
        requestService = service
    }

    /**
     * Die Funktion getSubjectById frägt die Daten eines Faches an.
     * Das Fach wird in subjects gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält subjects eine leere Liste.
     *
     * @param subjectId Die ID des Faches, welches angefragt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getSubjectById(subjectId : Int) : Int {
        var responseCode = 0
        val call = requestService.getSubjectById(subjectId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _subjects.value = listOfNotNull(response.body())
            } else {
                _subjects.value = mutableListOf()
            }
            Log.d("SubjectVM:getSubjById", response.message())
        } catch (error : Throwable) {
            _subjects.value = mutableListOf()
            error.message?.let { Log.e("SubjectVM:getSubjById", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getAllSubjects frägt alle Fächerdaten beim Server an.
     * Die Facher werden in subjects gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält subjects eine leere Liste.
     *
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getAllSubjects() : Int {
        var responseCode = 0
        val call = requestService.getAllSubjects()
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _subjects.value = response.body()
            } else {
                _subjects.value = mutableListOf()
            }
            Log.d("SubjectVM:getAllSubject", response.message())
        } catch (error : Throwable) {
            _subjects.value = mutableListOf()
            error.message?.let { Log.e("SubjectVM:getAllSubject", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getSubjectsOfTeacher frägt die Fächerdaten des Lehrers mit der
     * Id teacherId beim Server an.
     * Die Facher werden in subjects gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält subjects eine leere Liste.
     *
     * @param teacherId Die Benutzer-ID, des Lehrers, dessen Fächer-Daten angefragt werden sollen.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getSubjectsOfTeacher(teacherId : Int) : Int {
        var responseCode = 0
        val call = requestService.getSubjectsOfTeacher(teacherId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _subjects.value = response.body()
            } else {
                _subjects.value = mutableListOf()
            }
            Log.d("SubjectVM:getSubOfTeach", response.message())
        } catch (error : Throwable) {
            _subjects.value = mutableListOf()
            error.message?.let { Log.e("SubjectVM:getSubOfTeach", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getSubjectByClass frägt die Fächerdaten einer Klasse beim Server an.
     * Die Facher werden in subjects gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält subjects eine leere Liste.
     *
     * @param classId Die Klassen-ID, dessen Fächer angefragt werden sollen.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getSubjectByClass(classId : Int) : Int {
        var responseCode = 0
        val call = requestService.getSubjectByClass(classId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _subjects.value = response.body()
            } else {
                _subjects.value = mutableListOf()
            }
            Log.d("SubjectVM:getSubByClass", response.message())
        } catch (error : Throwable) {
            _subjects.value = mutableListOf()
            error.message?.let { Log.e("SubjectVM:getSubByClass", it) }
            error.printStackTrace()
        }
        return responseCode
    }


    /**
     * Die Funktion createSubject erstellt ein Fach auf dem Server.
     * Das neu erstellte Fach wird in subjects gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält subjects eine leere Liste.
     *
     * @param subject Das Fach, das erstellt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     * @see SubjectRequestDTO Datenobjekt für die REST-Anfrage.
     */
    suspend fun createSubject(subject : SubjectRequestDTO) : Int {
        var responseCode = 0
        val call = requestService.createSubject(subject)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _subjects.value = listOfNotNull(response.body())
            } else {
                _subjects.value = mutableListOf()
            }
            Log.d("SubjectVM:createSubject", response.message())
        } catch (error : Throwable) {
            _subjects.value = mutableListOf()
            error.message?.let { Log.e("SubjectVM:createSubject", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion updateSubject aktualisert ein Fach auf dem Server.
     * Das aktualliserte Fach wird in subjects gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält subjects eine leere Liste.
     *
     * @param subjectId Die ID des Faches, das aktualisert werden soll.
     * @param subject Das aktualisierte Fach.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     * @see SubjectRequestDTO Datenobjekt für die REST-Anfrage.
     */
    suspend fun updateSubject(subjectId : Int, subject : SubjectRequestDTO) : Int {
        var responseCode = 0
        val call = requestService.updateSubject(subjectId, subject)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _subjects.value = listOfNotNull(response.body())
            } else {
                _subjects.value = mutableListOf()
            }
            Log.d("SubjectVM:updateSubject", response.message())
        } catch (error : Throwable) {
            _subjects.value = mutableListOf()
            error.message?.let { Log.e("SubjectVM:updateSubject", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion addStudents fügt einem Fach Schüler hinzu und erhält als Antwort vom Server
     * die Konten aller Schüler in dem Fach. Wenn das Fach mit der Id subjectId in subjects gespeichert ist,
     * dann werden die Änderungen dem Fach hinzugefügt
     *
     * @param subjectId Die ID des Faches, dem die Schüler hinzugefügt werden sollen.
     * @param studentIds Liste mit den Benutzer-IDs der Schüler.
     * @return AccountResponseView-Liste mit den Konten der Schüler wenn die Anfrage erfolgreich war, sonst null.
     * @see AccountResponseView Konten der Schüler.
     */
    suspend fun addStudents(subjectId : Int, studentIds : List<Int>) : List<AccountResponseView>? {
        var accounts : List<AccountResponseView>? = null
        val call = requestService.addStudent(subjectId, studentIds)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            if (response.isSuccessful) {
                accounts = response.body()
                //Finde Fach mit der Id subjectId und füge die Änderungen dem Fach hinzu
                var subject = _subjects.value?.filter { it.id == subjectId }
                subject?.let {
                    if (accounts != null) {
                        it[0].accounts = accounts
                    }
                }
            }
            Log.d("SubjectVM:addStudent", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("SubjectVM:addStudent", it) }
            error.printStackTrace()
        }
        return accounts
    }

    /**
     * Die Funktion dropStudents entfernt Schüler aus einem Fach und erhält als Antwort vom Server
     * die Konten der verbleibenden Schüler in dem Fach. Wenn das Fach mit der Id subjectId in subjects gespeichert ist,
     * dann werden die Änderungen dem Fach hinzugefügt
     *
     * @param subjectId Die ID des Faches, aus dem die Schüler entfernt werden sollen.
     * @param studentIds Liste mit den Benutzer-IDs der Schüler.
     * @return AccountResponseView-Liste mit den Konten der Schüler wenn die Anfrage erfolgreich war, sonst null.
     * @see AccountResponseView Konten der Schüler.
     */
    suspend fun dropStudents(subjectId : Int, studentIds : List<Int>) : List<AccountResponseView>? {
        var accounts : List<AccountResponseView>? = null
        val call = requestService.dropStudent(subjectId, studentIds)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            if (response.isSuccessful) {
                accounts = response.body()
                //Finde Fach mit der Id subjectId und füge die Änderungen dem Fach hinzu
                var subject = _subjects.value?.filter { it.id == subjectId }
                subject?.let {
                    if (accounts != null) {
                        it[0].accounts = accounts
                    }
                }
            }
            Log.d("SubjectVM:dropStudent", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("SubjectVM:dropStudent", it) }
            error.printStackTrace()
        }
        return accounts
    }

    /**
     * Die Funktion deleteSubject löscht ein Fach.
     *
     * @param subjectId Die ID des Faches, das gelöscht werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun deleteSubject(subjectId : Int) : Int {
        var responseCode = 0
        val call = requestService.deleteSubject(subjectId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            Log.d("SubjectVM:deleteSubject", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("SubjectVM:deleteSubject", it) }
            error.printStackTrace()
        }
        return responseCode
    }
}