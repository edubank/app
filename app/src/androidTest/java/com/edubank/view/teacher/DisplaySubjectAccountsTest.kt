package com.edubank.view.teacher

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.studentName
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DisplaySubjectAccountsTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun loginTeacher() {
        // login as teacher
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.teacherUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.teacherPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        // wait for server to respond
        waitForServer()
    }

    /**
     * Zeigt die Teilnehmerliste eines Fachs an, öffnet dazu die Ansicht und überprüft, ob
     * die RecyclerView der Schüler angezeigt wird.
     * TF120
     */
    @Test
    fun test_showSubjectAccounts() {
        // open subject view
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0, click()
            )
        );

        // wait for server to respond
        waitForServer()

        // check if subject view is opened
        onView(withId(R.id.rv_subject_students)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_students)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_class)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_subject)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_subject_editSubject)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_subject_eventManager)).check(matches(isDisplayed()))

        // check if there is a student item displayed in the recycler view
        onView(withId(R.id.rv_subject_students)).perform(
            RecyclerViewActions
                .scrollToPosition<RecyclerView.ViewHolder>(0)
        ).check(matches(isDisplayed()))

        // check if test student is displayed
        onView(Matchers.allOf(withId(R.id.lbl_accountItem_name), withText(studentName))).check(
            matches(
                isDisplayed()
            )
        )
    }


    @After
    fun logout(){
        // wait for server to respond
        waitForServer()

        //Select logout from action bar
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}