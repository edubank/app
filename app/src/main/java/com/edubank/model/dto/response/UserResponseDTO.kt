package com.edubank.model.dto.response

/**
 * UserResponseDTO ist eine Daten-Klasse für Benutzer-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Benutzers
 * @property username Benutzername
 * @property passwordGen Hash des Passwortes
 * @property role Benutzertyp des Benutzers
 * @property firstName Vorname des Benutzers
 * @property lastName Nachname des Benutzers
 * @property email Email-Adresse des benutzers
 */
data class UserResponseDTO (
    val id : Int,
    val username : String,
    val passwordGen : String,
    val role : UserRole,
    val firstName : String,
    val lastName : String,
    val email : String
)
