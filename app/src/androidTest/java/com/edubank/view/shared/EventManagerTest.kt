package com.edubank.view.shared

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItem
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.admin.ClassAdapter
import com.edubank.view.admin.SubjectAdapterAdmin
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import kotlin.random.Random

/**
 * Beinhaltet allgemeine UI-Tests für EventManager.
 *
 * @see EventManager
 * @author Team EduBank
 */
class EventManagerTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Admin ein und navigiert zum Fragment EventManager vom "Test Fach" in der "Test Klasse".
     */
    private fun setUpAdmin() {
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()

        onView(withId(R.id.recyclerClassView)).perform(
            actionOnItem<ClassAdapter.ClassViewHolder>(
                hasDescendant(withText(TestData.testClass)), click()
            )
        )

        waitForServer()

        onView(withId(R.id.recyclerSubjectView)).perform(
            actionOnItem<SubjectAdapterAdmin.SubjectViewHolder>(
                hasDescendant(withText(TestData.testSubject)), click()
            )
        )

        waitForServer()

        onView(withId(R.id.btn_subject_eventManager)).perform(click())

        waitForServer()
    }

    /**
     * Testet die Erstellung eines Events.
     */
    @Test
    fun test_admin_createEvent() {
        setUpAdmin()
        val eventID = "E${Random.Default.nextInt(1000000, 9999999)}"
        val amount = Random.Default.nextInt(100)


        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$amount"))
        onView(allOf(withId(R.id.cb_studentItem_item), withText(TestData.studentName))).perform(
            click()
        )
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())

        waitForServer()

        onView(withId(R.id.rv_subject_students)).perform(
            actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(TestData.studentName)), click()
            )
        )

        waitForServer()

        onView(allOf(withId(R.id.transactionTitle), withText(eventID))).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )
        onView(allOf(withId(R.id.saldo), withText("${amount * -1}.0€"))).check(
            (ViewAssertions.matches(
                isDisplayed()
            ))
        )
    }

    /**
     * Testet die Erstellung eines Events als Einzahlung.
     */
    @Test
    fun test_admin_createEventDeposit() { //SETUP
        setUpAdmin()
        val eventID = "E${Random.Default.nextInt(1000000, 9999999)}"
        val amount = Random.Default.nextInt(100)


        //EXECUTE
        onView(withId(R.id.txt_eventManager_eventName)).perform(replaceText(eventID))
        onView(withId(R.id.txt_eventManager_amount)).perform(replaceText("$amount"))
        onView(allOf(withId(R.id.cb_studentItem_item), withText(TestData.studentName))).perform(
            click()
        )
        onView(withId(R.id.cb_eventManager_deposit)).perform(click())
        onView(withId(R.id.btn_eventManager_createEvent)).perform(click())


        //CHECK
        waitForServer()

        onView(withId(R.id.rv_subject_students)).perform(
            actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(TestData.studentName)), click()
            )
        )

        waitForServer()

        onView(allOf(withId(R.id.transactionTitle), withText(eventID))).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )
    }
}