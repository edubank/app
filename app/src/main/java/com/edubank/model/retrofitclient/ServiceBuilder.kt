package com.edubank.model.retrofitclient

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieHandler
import java.net.CookieManager
import java.util.concurrent.TimeUnit


/**
 * ServiceBuilder ist verantwortlich für die Verwaltung der Retrofit-Instanz.
 * Die Retrofit-Instanz kann dazu genutzt werden Services zu generieren
 * und damit REST-Anfragen an den Server zu senden.
 *
 * @author Team EduBank
 */
object ServiceBuilder {

    //Die URL-Adresse zum Web-Server
    private const val baseURL: String = "http://2b8a8e8a-89f7-4044-9312-f6375f282c7a.ka.bw-cloud-instance.org:8080/"

    //Cookie-Handler für die Autorisierung
    private var cookieHandler : CookieHandler = CookieManager()

    //OkHttpClient-Instanz für die Konfiguration der Retrofit-Instanz.
    private val httpClient =
        OkHttpClient.Builder()
            .cookieJar(JavaNetCookieJar(cookieHandler))
            .readTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .build()

    //Gson-Instanz: Konfiguruert den Gson-Konverter
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    //Retrofit-Instanz
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseURL)
        .client(httpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    /**
     * Die Funktion buildService generiert aus einem Retrofit-Service-Interfaces
     * einen ausführbaren RetrofitService und gibt ihn zurück.
     *
     * @param service Eine Java-Class-Instanz eines Retrofit-Service-Interfaces
     * @return Den aus service generierten Retrofit-Service
     */
    fun <T> buildService(service : Class<T>) : T {
        return retrofit.create(service)
    }
}
