package com.edubank.model.dto.response

/**
 * SubjectResponseView ist eine Daten-Klasse für Fächer-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Faches
 * @property name Name des Faches
 * @property teacher Lehrer des Faches
 */
data class SubjectResponseView (
    val id : Int,
    val name : String,
    val teacher : UserResponseDTO
)
