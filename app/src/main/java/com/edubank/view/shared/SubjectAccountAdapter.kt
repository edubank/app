package com.edubank.view.shared

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.SubjectAccountItemBinding
import com.edubank.model.dto.response.TransactionResponseDTO
import java.text.SimpleDateFormat
import java.util.*

/**
 * Adapter für die Anzeige von Transaktionen im SubjectAccount Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see SubjectAccount
 * @author Team EduBank
 */
class SubjectAccountAdapter(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<SubjectAccountAdapter.SubjectAccountViewHolder>() {

    private var transactions = mutableListOf<TransactionResponseDTO>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubjectAccountAdapter.SubjectAccountViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.subject_account_item, parent, false)
        return SubjectAccountViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager). Setzt die Farbe
     * von dem Transaktionsbetrag, das Datum und die Beschriftung jeweils.
     */
    override fun onBindViewHolder(
        holder: SubjectAccountAdapter.SubjectAccountViewHolder,
        position: Int
    ) {

        val currentTransaction = transactions[position]
        val balance = currentTransaction.amount.toString() + "€"

        // Farbe des Betrags setzen rot - grün
        when {
            currentTransaction.amount < 0 -> holder.balanceTextView.setTextColor(Color.parseColor("#ca5e5e"))
            else -> holder.balanceTextView.setTextColor(Color.parseColor("#4CAF50"))

        }

        // setze Überschrift und Datum
        holder.balanceTextView.text = balance
        val date = currentTransaction.createdAt
        val pattern = "dd.MM.yyyy HH:mm"
        val simpleDateFormat = SimpleDateFormat(pattern, Locale.GERMANY)
        holder.dateTextView.text = simpleDateFormat.format(date)
        holder.transactionTitleTextView.text = currentTransaction.title

    }

    /**
     * Die Methode überschreibt transactions und benachrichtigt Android,
     * dass sich die Daten geändert haben.
     *
     * @param updatedTransactions neue Liste der Transaktionen
     */
    @SuppressLint("NotifyDataSetChanged")
    fun onTransactionsChanged(updatedTransactions: MutableList<TransactionResponseDTO>) {
        transactions = updatedTransactions
        notifyDataSetChanged()
    }

    /**
     * Diese Method gibt die Anzahl Transaktionen von der Schülerliste zurück.
     *
     */
    override fun getItemCount() = transactions.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class SubjectAccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        private val binding = SubjectAccountItemBinding.bind(itemView)

        val transactionTitleTextView = binding.transactionTitle
        val dateTextView = binding.dateText
        val balanceTextView = binding.saldo

        init {
            transactionTitleTextView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onItemClick(position)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}