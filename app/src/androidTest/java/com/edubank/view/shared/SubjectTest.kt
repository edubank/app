package com.edubank.view.shared

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.admin.ClassAdapter
import com.edubank.view.admin.SubjectAdapterAdmin
import org.junit.*

/**
 * Beinhaltet allgemeine UI-Tests für Subject.
 *
 * @see Subject
 * @author Team EduBank
 */
class SubjectTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Admin ein und navigiert zum Fragment Subject von "Test Fach" in der "Test Klasse".
     */
    @Before
    fun setUp() { // login with admin
        onView(withId(R.id.txt_login_username)).perform(ViewActions.replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(ViewActions.replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(ViewActions.click())

        TestData.waitForServer()

        // select the test class
        onView(withId(R.id.recyclerClassView)).perform(
            RecyclerViewActions.actionOnItem<ClassAdapter.ClassViewHolder>(
                hasDescendant(withText(TestData.testClass)), ViewActions.click()
            )
        )

        TestData.waitForServer()

        // select the test subject
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItem<SubjectAdapterAdmin.SubjectViewHolder>(
                hasDescendant(withText(TestData.testSubject)), ViewActions.click()
            )
        )

        TestData.waitForServer()
    }

    /**
     * Prüft, ob das Fach auch richtig angezeigt wird.
     */
    @Test
    fun test_subjectIsCorrectlyDisplayed() {
        onView(withId(R.id.subject)).check(matches(isDisplayed()))
        onView(withId(R.id.lbl_subject_class)).check(matches(withText(TestData.testClass)))
        onView(withId(R.id.lbl_subject_subject)).check(
            matches(withText("${TestData.testSubject} - ${TestData.teacherName} (${TestData.teacherUsername})"))
        )
        onView(withId(R.id.btn_subject_editSubject)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_subject_eventManager)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_subject_students)).check(matches(isDisplayed()))
    }
}