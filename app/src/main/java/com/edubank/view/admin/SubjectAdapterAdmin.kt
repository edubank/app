package com.edubank.view.admin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.SubjectItemAdminBinding
import com.edubank.model.dto.response.SubjectResponseDTO

/**
 * Adapter für die Anzeige von Fächern im SubjectOverviewAdmin-Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see SubjectOverviewAdmin
 * @property subjects Liste mit alle Fächer die in der RecyclerView gezeigt werden
 * @author Team EduBank
 */
class SubjectAdapterAdmin(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<SubjectAdapterAdmin.SubjectViewHolder>() {

    var subjects = mutableListOf<SubjectResponseDTO>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SubjectViewHolder {

        // Erstellt eine neue Ansicht, die die Benutzeroberfläche des Listenelements definiert
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.subject_item_admin, viewGroup, false)

        return SubjectViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager)
     */
    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {

        // Hole das Element an dieser Position
        // und ersetze den Inhalt der Ansicht durch dieses Element
        val currentSubject = subjects[position]
        val currentTeacher = currentSubject.teacher
        val teacherName = "${currentTeacher.firstName} ${currentTeacher.lastName} (${currentTeacher.username})"

        holder.subjectName.text = currentSubject.name
        holder.teacherName.text = teacherName
    }

    /**
     * Gibt die Anzahl der Fächer zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = subjects.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class SubjectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding = SubjectItemAdminBinding.bind(itemView)

        val subjectName = binding.subjectName
        val teacherName = binding.teacherName

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onSubjectClick(position)
        }
    }

    interface OnItemClickListener {

        fun onSubjectClick(position: Int)
    }
}