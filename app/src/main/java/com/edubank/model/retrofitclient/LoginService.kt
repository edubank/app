package com.edubank.model.retrofitclient

import com.edubank.model.dto.request.ForgotPasswordRequestDTO
import com.edubank.model.dto.request.LoginRequestDTO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Scnittstellen-Beschreibung für den Retrofit-Service LoginService.
 * Die Schnittstelle beschreibt die Login/Logout-Anfragen an den Server.
 *
 * @author Team EduBank
 */
interface LoginService {

    /**
     * Die Funktion login führt den Login des Benutzers durch.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param loginRequest Login-Daten
     * @return Call-Objekt
     */
    @POST("login")
    fun login(@Body loginRequest : LoginRequestDTO) : Call<Unit>

    /**
     * Die Funktion logout führt das Logout des Benutzers durch.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @return Call-Objekt
     */
    @POST("logout")
    fun logout() : Call<Unit>

    /**
     * Die Funktion forgotPassword fragt für den Nutzer mit der E-Mail-Adresse email
     * ein neues Passwort an. Das Passwort wird den Nutzer per Mail zugeschickt.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param forgotPasswordRequest Daten-Objekt für eine "Passwort vergessen"-Aktion
     * @return Call-Objekt
     */
    @POST("forgot-password")
    fun forgotPassword(@Body forgotPasswordRequest : ForgotPasswordRequestDTO) : Call<Unit>

}