package com.edubank.view.admin

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.studentEmail
import com.edubank.view.TestData.Companion.studentFirstName
import com.edubank.view.TestData.Companion.studentLastName
import com.edubank.view.TestData.Companion.studentName
import com.edubank.view.TestData.Companion.studentUsername
import com.edubank.view.TestData.Companion.teacherEmail
import com.edubank.view.TestData.Companion.teacherFirstName
import com.edubank.view.TestData.Companion.teacherLastName
import com.edubank.view.TestData.Companion.teacherName
import com.edubank.view.TestData.Companion.teacherUsername
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.random.Random

/**
 * Testet das Anzeigen von Benutzerdaten
 * TF160
 */
class DisplayUserDataTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUpAdmin() {
        // login as admin
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        waitForServer()

        // open user overview
        onView(withId(R.id.userManagerButton)).perform(click())
        waitForServer()

    }

    /**
     * Testet das Anzeigen von detaillierten Benutzerdaten (Test Schüler)
     * TF160
     */
    @Test
    fun test_showStudentDetails(){
        // open event manager of test user
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItem<UserAdapter.UserViewHolder>
                (hasDescendant(withText("$studentName (${studentUsername})")), click())
        )
        waitForServer()

        // check user detailed data name, email, role
        onView(withId(R.id.userName)).check(matches(withText("$studentName (${studentUsername})")))
        onView(withId(R.id.firstNameEdit)).check(matches(withText(studentFirstName)))
        onView(withId(R.id.lastNameEdit)).check(matches(withText(studentLastName)))
        onView(withId(R.id.emailEdit)).check(matches(withText(studentEmail)))
        onView(withId(R.id.roleSpinner)).check(matches(withSpinnerText("Schüler")))
        onView(withId(R.id.userRole)).check(matches(withText("Schüler")))
    }

    /**
     * Testet das Anzeigen von detaillierten Benutzerdaten (Test Lehrer)
     * TF160
     */
    @Test
    fun test_showTeacherDetails(){
        // open event manager of test user
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItem<UserAdapter.UserViewHolder>
                (hasDescendant(withText("$teacherName (${teacherUsername})")), click())
        )
        waitForServer()

        // check user detailed data name, email, role
        onView(withId(R.id.userName)).check(matches(withText("$teacherName (${teacherUsername})")))
        onView(withId(R.id.firstNameEdit)).check(matches(withText(teacherFirstName)))
        onView(withId(R.id.lastNameEdit)).check(matches(withText(teacherLastName)))
        onView(withId(R.id.emailEdit)).check(matches(withText(teacherEmail)))
        onView(withId(R.id.roleSpinner)).check(matches(withSpinnerText("Lehrer")))
        onView(withId(R.id.userRole)).check(matches(withText("Lehrer")))
    }

    /**
     * Testet das Anzeigen von detaillierten Benutzerdaten (Random tap)
     * Es sollten 4 Benutzer im System eingetragen sein.
     * TF160
     */
    @Test
    fun test_showUserDetails(){
        val randomId = Random.Default.nextInt(4)

        // open event manager of test user
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<UserAdapter.UserViewHolder>(randomId, click())
        )
        waitForServer()

        // check user detailed data name, email, role
        onView(withId(R.id.userName)).check(matches(isDisplayed()))
        onView(withId(R.id.firstNameEdit)).check(matches(isDisplayed()))
        onView(withId(R.id.lastNameEdit)).check(matches(isDisplayed()))
        onView(withId(R.id.emailEdit)).check(matches(isDisplayed()))
        onView(withId(R.id.userRole)).check(matches(isDisplayed()))
    }


    @After
    fun logout() {
        // wait just in case
        waitForServer()

        //Select logout from action bar
        Espresso.openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // wait for server to respond
        waitForServer()

        //Check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }
}