package com.edubank.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edubank.model.dto.request.ForgotPasswordRequestDTO
import com.edubank.model.dto.request.LoginRequestDTO
import com.edubank.model.retrofitclient.LoginService
import com.edubank.model.retrofitclient.ServiceBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

/**
 * Das ViewModel liefert die Funktionalität, die für das Login/Logout benötigt wird.
 * Dazu gehört die Kommunkation mit dem Server. Weiter kann das Passwort zurückgesetzt werden.
 *
 * @author Team EduBank
 * @property service Dem ViewModel kann ein LoginService übergeben werden, wenn ein spezieller Service gebraucht wird (Andernfalls wird der Default-Service verwendet).
 * @see LoginService Retrofit-Service, der für die Kommunikation mit dem Server verwendet wird.
 */
class LoginViewModel : ViewModel() {

    /**
     * Retrofit-Service für Login-Anfragen.
     */
    private var requestService = ServiceBuilder.buildService(LoginService::class.java)

    /**
     * Die setService-Funktion setzt den Login-Service. Wenn setService nicht aufgerufen wird,
     * dann wird der Default-Service gesetzt. Sonst service.
     * Das Initialisieren mit einem spezifischen Service wird für die Tests benötigt.
     */
    fun setService(service : LoginService){
        requestService = service
    }

    /**
     * Die Funktion login führt den Login des Benutzers durch.
     *
     * @param username Benutzername des Benutzers.
     * @param password Passwort des Benutzers.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun login(username : String, password : String) : Int {
        var responseCode = 0
        var loginBody = LoginRequestDTO(username, password)
        var call = requestService.login(loginBody)
        var asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            Log.d("LoginVM:login", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("LoginVM:login", it) }
            error.printStackTrace()
        }

        return responseCode
    }

    /**
     * Die Funktion logout führt das Logout des Benutzers durch.
     *
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun logout() : Int {
        var responseCode = 0
        var call = requestService.logout()
        var asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            var response = asyncResponse.await()
            responseCode = response.code()
            Log.d("LoginVM:logout", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("LoginVM:logout", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion forgotPassword fragt für den Nutzer mit der E-Mail-Adresse email
     * ein neues Passwort an. Das Passwort wird den Nutzer per Mail zugeschickt.
     *
     * @param email E-Mail-Adresse des Nutzers.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun forgotPassword(email : String) : Int {
        var responseCode = 0
        var call = requestService.forgotPassword(ForgotPasswordRequestDTO(email))
        var asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            var response = asyncResponse.await()
            responseCode = response.code()
            Log.d("LoginVM:forgotPassword", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("LoginVM:forgotPassword", it) }
            error.printStackTrace()
        }
        return responseCode
    }
}