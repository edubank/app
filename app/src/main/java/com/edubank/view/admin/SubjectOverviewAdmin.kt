package com.edubank.view.admin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectOverviewAdminFragmentBinding
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.model.dto.response.SubjectResponseDTO
import com.edubank.view.shared.Subject
import com.edubank.view.shared.SubjectOverview
import com.edubank.viewmodel.ClassViewModel
import com.edubank.viewmodel.SubjectViewModel
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine Übersicht mit allen Fächern der ausgewählten Klasse.
 *
 * @author Team EduBank
 */
class SubjectOverviewAdmin : SubjectOverview(), SubjectAdapterAdmin.OnItemClickListener {
    private val classViewModel: ClassViewModel by activityViewModels()
    private val subjectViewModel: SubjectViewModel by activityViewModels()
    private lateinit var binding : SubjectOverviewAdminFragmentBinding
    private lateinit var subjectList: List<SubjectResponseDTO>
    private lateinit var subjectAdapter: SubjectAdapterAdmin
    private lateinit var currentClass: SchoolClassResponseDTO

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {
        binding = SubjectOverviewAdminFragmentBinding.inflate(inflater, container, false)
        binding.classVM = classViewModel
        binding.subjectVM = subjectViewModel
        binding.lifecycleOwner = this

        // Die anfänglichen Daten zeigen
        showClassName()
        requestSubjects()

        // SetOnClickListeners
        binding.editClassBtn.setOnClickListener {
            openSubjectOverviewManager()
        }

        // RecyclerView aufbauen
        subjectAdapter = SubjectAdapterAdmin(this)
        binding.recyclerSubjectView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = subjectAdapter
            setHasFixedSize(true)
        }

        return binding.root
    }

    /**
     * Der Name der ausgewählten Klasse wird gezeigt.
     */
    private fun showClassName() {
        currentClass = classViewModel.classes.value?.get(0)!!
        binding.className.text = currentClass.name
    }

    /**
     * Eine Liste mit allen Fächern wird vom Server angefordert,
     * alphabetisch sortiert und in der Recyclerview gezeigt.
     */
    override fun requestSubjects() {
        lifecycleScope.launch {
            subjectViewModel.getSubjectByClass(currentClass.id)
            subjectList = subjectViewModel.subjects.value!!.sortedBy { it.name.lowercase() }
            subjectAdapter.subjects = subjectList.toMutableList()
            subjectAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der Subject-Ansicht aus.
     */
    override fun showSubject() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, Subject())
            .addToBackStack(null).commit()
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der SubjectOverviewManager-Ansicht aus.
     */
    private fun openSubjectOverviewManager() {
        lifecycleScope.launch {
            classViewModel.getClass(currentClass.id)
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.main_container, SubjectOverviewManager())
                .addToBackStack(null).commit()
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der Subject-Ansicht
     * mit den Daten des ausgewählten Fachs aus.
     */
    override fun onSubjectClick(position: Int) {
        lifecycleScope.launch {
            subjectViewModel.getSubjectById(subjectList[position].id)
            showSubject()
        }
    }
}