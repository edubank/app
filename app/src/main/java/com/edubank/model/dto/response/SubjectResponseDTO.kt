package com.edubank.model.dto.response

import com.google.gson.annotations.SerializedName

/**
 * SubjectResponseDTO ist eine Daten-Klasse für Fächer-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Faches
 * @property name Name des Faches
 * @property schoolClass Klasse des Faches
 * @property teacher Lehrer des Faches
 * @property accounts Konten des Faches
 */
data class SubjectResponseDTO (
    val id : Int,
    val name : String,
    @SerializedName("class")
    val schoolClass : SchoolClassResponseView,
    val teacher : UserResponseDTO,
    var accounts : List<AccountResponseView>
)
