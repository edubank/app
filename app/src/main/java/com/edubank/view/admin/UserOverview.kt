package com.edubank.view.admin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.UserOverviewFragmentBinding
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.UserViewModel
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine Übersicht mit allen Benutzern.
 *
 * @author Team EduBank
 */
class UserOverview : Fragment(), UserAdapter.OnItemClickListener {

    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var binding : UserOverviewFragmentBinding
    private lateinit var userList: MutableList<UserResponseDTO>
    private lateinit var sortedList: MutableList<UserResponseDTO>
    private lateinit var userAdapter: UserAdapter

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = UserOverviewFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userViewModel
        binding.lifecycleOwner = this

        // init sortedList so that switching to dark mode won't crash the app
        sortedList = mutableListOf()

        // Die anfänglichen Daten zeigen
        requestUsers()

        // SetOnClickListeners
        binding.apply {
            openUserManagerBtn.setOnClickListener { openUserManager() }
            filterBtn.setOnClickListener {
                toggleFilterGroup()
                hideSortGroup()
            }
            sortBtn.setOnClickListener {
                toggleSortGroup()
                hideFilterGroup()
            }
            firstName.setOnClickListener {
                sortedList = userList.sortedBy { it.firstName.lowercase() }.toMutableList()
                userAdapter.users = sortedList
                userAdapter.notifyDataSetChanged()
                toggleSortGroup()
            }
            lastName.setOnClickListener {
                sortedList = userList.sortedBy { it.lastName.lowercase() }.toMutableList()
                userAdapter.users = sortedList
                userAdapter.notifyDataSetChanged()
                toggleSortGroup()
            }
            chronological.setOnClickListener {
                sortedList = userList.reversed().toMutableList()
                userAdapter.users = sortedList
                userAdapter.notifyDataSetChanged()
                toggleSortGroup()
            }
            all.setOnClickListener {
                lifecycleScope.launch {
                    userViewModel.getAllUsers()
                    userList = userViewModel.users.value!!.toMutableList()
                    sortedList = userList.reversed().toMutableList()
                    userAdapter.users = sortedList
                    userAdapter.notifyDataSetChanged()
                }
                toggleFilterGroup()
                clearSearchBox()
            }
            students.setOnClickListener {
                filterUsers(UserRole.ROLE_STUDENT)
                toggleFilterGroup()
                clearSearchBox()
            }
            teachers.setOnClickListener {
                filterUsers(UserRole.ROLE_TEACHER)
                toggleFilterGroup()
                clearSearchBox()
            }
            admins.setOnClickListener {
                filterUsers(UserRole.ROLE_ADMIN)
                toggleFilterGroup()
                clearSearchBox()
            }
        }

        //RecyclerView aufbauen
        userAdapter = UserAdapter(this)
        binding.recyclerUserView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = userAdapter
            setHasFixedSize(true)
        }

        // Search bar aufbauen
        binding.userSearch.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                searchUsers(newText!!)
                return false
            }
        })
        return binding.root
    }

    /**
     * Eine Liste mit allen Benutzern wird vom Server angefordert,
     * chronologisch sortiert und in der Recyclerview gezeigt.
     */
    private fun requestUsers() {
        lifecycleScope.launch {
            userViewModel.getAllUsers()

            userList = userViewModel.users.value!!.toMutableList()
            sortedList = userList.reversed().toMutableList()
            userAdapter.users = sortedList
            userAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der UserManager-Ansicht aus.
     */
    private fun openUserManager() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, UserManager())
            .addToBackStack(null).commit()
    }

    /**
     * Die Optionen für Filtern umschalten.
     */
    private fun toggleFilterGroup() {
        binding.filterGroup.isInvisible = !binding.filterGroup.isInvisible
    }

    /**
     * Die Optionen für Sortieren umschalten.
     */
    private fun toggleSortGroup() {
        binding.sortGroup.isInvisible = !binding.sortGroup.isInvisible
    }

    /**
     * Die Optionen für Filtern verstecken.
     */
    private fun hideFilterGroup() {
        binding.filterGroup.isInvisible = true
    }

    /**
     * Die Optionen für Sortieren verstecken.
     */
    private fun hideSortGroup() {
        binding.sortGroup.isInvisible = true
    }

    /**
     * Die Benutzer werden nach Rolle filtriert (die Benutzer mit der ausgewählten
     * Rolle werden in dem Recyclerview gezeigt).
     */
    private fun filterUsers(role: UserRole) {
        lifecycleScope.launch {
            userViewModel.getUserByRole(role.toString())
            userList = userViewModel.users.value!!.toMutableList()
            sortedList = userList.reversed().toMutableList()
            userAdapter.users = sortedList
            userAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Diese Methode filtert die Benutzerliste nach der Eingabe, anhand des Prefix,
     * und teilt die Änderungen dem Adapter mit, um die neue gefilterte Liste anzuzeigen.
     *
     * @param searchString Die Eingabe als String
     */
    private fun searchUsers(searchString: String) {
        lifecycleScope.launch {
            val filteredList = mutableListOf<UserResponseDTO>()
            for (user in sortedList) {
                if (user.firstName.lowercase()
                        .startsWith(searchString.lowercase()) || user.lastName.lowercase()
                        .startsWith(searchString.lowercase())
                ) {
                    filteredList.add(user)
                }
            }
            if (searchString == "") {
                userAdapter.updateList(sortedList)
            } else {
                userAdapter.updateList(filteredList)
            }
        }
    }

    /**
     * Das SearchBox wird zurückgesetzt.
     */
    private fun clearSearchBox() {
        binding.userSearch.setQuery("", false)
        binding.userSearch.clearFocus()
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der User-Ansicht aus.
     */
    private fun showUser() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, User())
            .addToBackStack(null).commit()
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der User-Ansicht
     * des ausgewählten Benutzers aus.
     */
    override fun onItemClick(position: Int) {
        lifecycleScope.launch {
            userViewModel.getUserById(userAdapter.users[position].id)
            clearSearchBox()
            showUser()
        }
    }
}