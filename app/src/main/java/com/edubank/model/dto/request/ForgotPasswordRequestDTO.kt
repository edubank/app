package com.edubank.model.dto.request

/**
 * ForgotPasswordRequestDTO ist eine Daten-Klasse für die Daten einer "Passwort vergessen"-Aktion
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property email EMail-Adresse des Benutzers.
 */
data class ForgotPasswordRequestDTO(
    val email : String
)