package com.edubank.view.shared

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.StudentItemBinding
import com.edubank.model.dto.response.UserResponseDTO

/**
 * Adapter für die Anzeige von Accounts oder Usern im SubjectManager Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see SubjectManager
 * @author Team EduBank
 */
class SubjectManagerAdapter(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<SubjectManagerAdapter.SubjectManagerViewHolder>() {

    private var students = mutableListOf<UserResponseDTO>()

    /**
     * Die modifizierte Liste, die der Adapter anzeigt. Bestimmt, welche Schüler ausgewählt sind.
     */
    var modifyList = mutableListOf<Int>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SubjectManagerAdapter.SubjectManagerViewHolder {
        // Erstellt eine neue Ansicht, die die Benutzeroberfläche des Listenelements definiert
        val view = LayoutInflater.from(parent.context).inflate(R.layout.student_item, parent, false)
        return SubjectManagerViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager) Überprüft, ob
     * Schüler bereits ausgewählt sind und setzt diese gegebenenfalls.
     */
    override fun onBindViewHolder(holder: SubjectManagerViewHolder, position: Int) {
        val currentStudent = students[position]
        val studentName =
            "${currentStudent.firstName} ${currentStudent.lastName} (${students[position].username})"
        holder.studentNameTextView.text = studentName

        // setze beim Bind die ausgewählten Schüler für die Auswahlboxen
        when {
            modifyList.isEmpty() -> holder.studentNameTextView.isChecked = false
            else -> holder.studentNameTextView.isChecked = modifyList.contains(currentStudent.id)
        }

    }


    /**
     * Diese Method gibt die Anzahl Listenelemenete von der Schülerliste zurück.
     *
     */
    override fun getItemCount(): Int = students.size


    /**
     * Die Methode überschreibt subjects und benachrichtigt Android,
     * dass sich die Daten geändert haben.
     *
     * @param updatedSubjects neue Liste der Fächer
     */
    fun onStudentsChanged(updatedStudents: MutableList<UserResponseDTO>) {
        students = updatedStudents
        notifyDataSetChanged()
    }


    /**
     * Diese Methode überschreibt subjects und modifyList des Adapters und setzt diese auf
     * leere Listen zurück.
     *
     */
    fun reset() {
        modifyList = mutableListOf()
        students = mutableListOf()
        notifyDataSetChanged()
    }

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class SubjectManagerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        private val binding = StudentItemBinding.bind(itemView)
        val studentNameTextView = binding.cbStudentItemItem

        init {
            studentNameTextView.setOnClickListener(this)
        }

        /**
         * Diese Methode wird ausgeführt, wenn ein Schüler in der Liste gesetzt wird.
         * Dieser Schüler wird in die modifyList aufgenommen oder von dieser entfernt.
         *
         */
        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onItemClick(position)

            when {
                binding.cbStudentItemItem.isChecked -> modifyList.add(students[position].id)
                else -> modifyList.remove(students[position].id)
            }
        }
    }


    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

}