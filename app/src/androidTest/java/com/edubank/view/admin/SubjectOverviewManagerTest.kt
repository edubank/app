package com.edubank.view.admin

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers
import org.junit.*

/**
 * Beinhaltet allgemeine UI-Tests für SubjectOverviewManager.
 *
 * @see SubjectOverviewManager
 * @author Team EduBank
 */
class SubjectOverviewManagerTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Administrator ein und navigiert zum Fragment SubjectOverviewManager.
     */
    @Before
    fun setUp() { //Login with admin
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        //Select the test class
        onView(withId(R.id.recyclerClassView)).perform(
            RecyclerViewActions.actionOnItem<ClassAdapter.ClassViewHolder>(
                hasDescendant(withText(TestData.testClass)), click()
            )
        )

        TestData.waitForServer()

        //Choose to edit class
        onView(withId(R.id.editClassBtn)).perform(click())

        TestData.waitForServer()
    }

    /**
     * Prüft, dass das Fragment richtig angezeigt wird.
     */
    @Test
    fun test_fragmentIsDisplayedCorrectly() {
        onView(withId(R.id.subjectOverviewManager)).check(matches(isDisplayed()))
        onView(withId(R.id.className)).check(matches(withText(TestData.testClass)))
        onView(withId(R.id.addBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.selectedSubjectTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.selectedSubject)).check(matches(isDisplayed()))
        onView(withId(R.id.newNameTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.newNameEdit)).check(matches(isDisplayed()))
        onView(withId(R.id.changeTeacherSpinner)).check(matches(isDisplayed()))
    }

    /**
     * Erstellt ein Fach und löscht dieses wieder.
     */
    @Test
    fun test_createAndDeleteSubject() { //create subject
        onView(withId(R.id.addBtn)).perform(click())
        onView(withId(R.id.newSubjectEdit)).perform(replaceText(TestData.deleteSubject))
        onView(withId(R.id.newTeacherSpinner)).perform(click())
        onView(withId(R.id.recyclerTeacherView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<TeacherAdapter.TeacherViewHolder>(
                0, click()
            )
        )
        onView(withId(R.id.confirmBtn)).perform(click())

        //check if subject has been created
        onView(
            Matchers.allOf(withId(R.id.subjectName), withText(TestData.deleteSubject))
        ).check(
            matches(
                isDisplayed()
            )
        )

        //delete subject
        onView(withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ClassAdapter.ClassViewHolder>(
                0, click()
            )
        )
        onView(withId(R.id.deleteBtn)).perform(click())
        onView(withId(android.R.id.button1)).perform(click())

        //check if subject has been deleted
        onView(
            Matchers.allOf(withId(R.id.subjectName), withText(TestData.deleteSubject))
        ).check(
            doesNotExist()
        )
    }
}