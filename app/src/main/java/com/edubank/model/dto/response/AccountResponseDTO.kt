package com.edubank.model.dto.response

/**
 * AccountResponseDTO ist eine Daten-Klasse für Konto-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id des Kontos
 * @property subject Fach zu dem das Konto gehört
 * @property student Eigentümer des Kontos
 * @property balance Kontostand des Kontos
 */
data class AccountResponseDTO (
    val id : Int,
    val subject : SubjectClassResponseView,
    val student : UserResponseDTO,
    val balance : Double
)
