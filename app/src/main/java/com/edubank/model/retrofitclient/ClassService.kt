package com.edubank.model.retrofitclient

import com.edubank.model.dto.request.SchoolClassRequestDTO
import com.edubank.model.dto.response.SchoolClassResponseDTO
import retrofit2.Call
import retrofit2.http.*

/**
 * Scnittstellen-Beschreibung für den Retrofit-Service ClassService.
 * Die Schnittstelle beschreibt die Klassen-Anfragen an den Server.
 *
 * @author Team EduBank
 */
interface ClassService {

    /**
     * Die Funktion getAllClasses frägt die Daten aller Klassen an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @return Call-Objekt mit einer Klassen-Daten-Liste.
     */
    @GET("classes")
    fun getAllClasses() : Call<List<SchoolClassResponseDTO>>

    /**
     * Die Funktion getClass frägt die Daten einer Klasse an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param classId Die Klassen-ID der Klasse, die angefragt werden sollen.
     * @return Call-Objekt mit den Klassen-Daten.
     */
    @GET("classes/{id}")
    fun getClass(@Path("id") classId : Int) : Call<SchoolClassResponseDTO>

    /**
     * Die Funktion createClass erstellt eine Klasse auf dem Server.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param schoolClass Daten-Objekt der neu zu erzeugenden Klasse.
     * @return Call-Objekt mit den Klassen-Daten.
     */
    @POST("classes")
    fun createClass(@Body schoolClass : SchoolClassRequestDTO) : Call<SchoolClassResponseDTO>

    /**
     * Die Funktion updateClass aktualisiert die Klassendaten einer Klasse auf dem Server.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param classId Die Klassen-ID der Klasse, welche aktualisert werden soll.
     * @param schoolClass Aktualisiertes Daten-Objekt der Klasse.
     * @return Call-Objekt mit den Klassen-Daten.
     */
    @PUT("classes/{id}")
    fun updateClass(@Path("id") classId : Int, @Body schoolClass : SchoolClassRequestDTO) : Call<SchoolClassResponseDTO>

    /**
     * Die Funktion deleteClass löscht die Daten einer Klasse auf dem Server.
     *
     * @param classId Die Klassen-ID der Klasse, die gelöscht werden sollen.
     * @return Call-Objekt mit einem Boolean im Body, welcher den Erfolg der Löschanfrage angibt.
     */
    @DELETE("classes/{id}")
    fun deleteClass(@Path("id") classId : Int) : Call<Boolean>

}