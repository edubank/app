package com.edubank.model.dto.response

/**
 * UserRole definiert alle Benutzertypen der Anwendung
 */
enum class UserRole {
    ROLE_STUDENT, ROLE_TEACHER, ROLE_ADMIN
}
