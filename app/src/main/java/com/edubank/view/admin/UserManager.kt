package com.edubank.view.admin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.edubank.R
import com.edubank.databinding.UserManagerFragmentBinding
import com.edubank.model.dto.request.UserRequestDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine "Benutzer-Bearbeiten" Ansicht.
 *
 * @author Team EduBank
 */
class UserManager : Fragment(){

    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var binding: UserManagerFragmentBinding
    private lateinit var role: UserRole

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = UserManagerFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userViewModel
        binding.lifecycleOwner = this

        binding.addBtn.setOnClickListener { addUser() }

        setupSpinner()

        return binding.root
    }

    /**
     * Ein neuer Benutzer wird erstellt.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    fun addUser() {
        val firstName = binding.firstNameEdit.text.toString()
        val lastName = binding.lastNameEdit.text.toString()
        val email = binding.emailEdit.text.toString()

        val userData = UserRequestDTO(role.toString(), firstName, lastName, email)

        lifecycleScope.launch {
            val response = userViewModel.createUser(userData)
            if (response == 200) {
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, UserOverview()).commit()
            } else {
                when {
                    firstName.isEmpty() ->
                        Snackbar.make(requireView(), getString(R.string.emptyFirstNameToast), Snackbar.LENGTH_LONG).show()
                    lastName.isEmpty() ->
                        Snackbar.make(requireView(), getString(R.string.emptyLastNameToast), Snackbar.LENGTH_LONG).show()
                    else ->
                        Snackbar.make(requireView(), getString(R.string.invalidEmail), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    /**
     * Die Rolle eines Benutzers vom Textformat zu UserRole-Format
     * (schon definierte Parameter im Projekt) umwandeln.
     */
    private fun textToRole(text: String): UserRole {
        return when (text) {
            resources.getStringArray(R.array.roles)[0] -> UserRole.ROLE_STUDENT
            resources.getStringArray(R.array.roles)[1] -> UserRole.ROLE_TEACHER
            else                                       -> UserRole.ROLE_ADMIN
        }
    }

    /**
     * Das Spinner für Auswählen der Benutzerrolle aufbauen.
     */
    private fun setupSpinner() {

        val roleSpinner = binding.roleSpinner
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.roles))
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        roleSpinner.adapter = adapter

        roleSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                role = textToRole(parent?.getItemAtPosition(position).toString())
            }
        }
    }
}
