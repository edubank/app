package com.edubank.view.teacher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectOverviewTeacherFragmentBinding
import com.edubank.model.dto.response.SubjectResponseDTO
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.view.shared.Subject
import com.edubank.view.shared.SubjectOverview
import com.edubank.viewmodel.SubjectViewModel
import com.edubank.viewmodel.UserViewModel
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine Übersicht mit allen Fächern, die der konkrete Lehrer unterrichtet.
 *
 * @author Team EduBank
 */
class SubjectOverviewTeacher : SubjectOverview(), SubjectAdapterTeacher.OnItemClickListener {
    private val userViewModel: UserViewModel by activityViewModels()
    private val subjectViewModel: SubjectViewModel by activityViewModels()
    private lateinit var binding : SubjectOverviewTeacherFragmentBinding
    private lateinit var subjectList: List<SubjectResponseDTO>
    private lateinit var subjectAdapter: SubjectAdapterTeacher
    private lateinit var currentTeacher: UserResponseDTO

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {
        binding = SubjectOverviewTeacherFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userViewModel
        binding.subjectVM = subjectViewModel
        binding.lifecycleOwner = this

        // Die anfänglichen Daten zeigen
        lifecycleScope.launch {
            getTeacherName()
            requestSubjects()
        }

        // RecyclerView aufbauen
        subjectAdapter = SubjectAdapterTeacher(this)
        binding.recyclerSubjectView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = subjectAdapter
            setHasFixedSize(true)
        }

        return binding.root
    }

    /**
     * Fordert die Benutzerdaten des angemeldeten Lehrers vom Server an und setzt diese.
     */
    private suspend fun getTeacherName() {
        userViewModel.getMyUser()
        currentTeacher = userViewModel.users.value?.get(0)!!
        val teacherName = "${currentTeacher.firstName} ${currentTeacher.lastName} (${currentTeacher.username})"
        binding.teacherName.text = teacherName
    }

    /**
     * Eine Liste mit allen Fächern des Lehrers wird vom Server angefordert,
     * alphabetisch sortiert und in der Recyclerview gezeigt.
     */
    override fun requestSubjects() {
       lifecycleScope.launch {
           subjectViewModel.getSubjectsOfTeacher(currentTeacher.id)
           subjectList = subjectViewModel.subjects.value!!.sortedBy { it.schoolClass.name.lowercase() }
           subjectAdapter.subjects = subjectList.toMutableList()
           subjectAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der Subject-Ansicht aus.
     */
    override fun showSubject() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, Subject())
            .addToBackStack(null).commit()
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der Subject-Ansicht
     * des ausgewählten Fachs aus.
     */
    override fun onSubjectClick(position: Int) {
        lifecycleScope.launch {
            subjectViewModel.getSubjectById(subjectList[position].id)
            showSubject()
        }
    }
}