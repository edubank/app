package com.edubank.view.admin

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectOverviewManagerFragmentBinding
import com.edubank.model.dto.request.SubjectRequestDTO
import com.edubank.model.dto.response.SchoolClassResponseDTO
import com.edubank.model.dto.response.SubjectResponseDTO
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.ClassViewModel
import com.edubank.viewmodel.SubjectViewModel
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine Übersicht, in der die Fächer der ausgewählten
 * Klasse bearbeitet werden können.
 *
 * @author Team EduBank
 */
class SubjectOverviewManager : Fragment(), SubjectAdapterAdmin.OnItemClickListener, TeacherAdapter.OnItemClickListener {

    private val classViewModel: ClassViewModel by activityViewModels()
    private val subjectViewModel: SubjectViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var binding : SubjectOverviewManagerFragmentBinding
    private lateinit var subjectAdapter: SubjectAdapterAdmin
    private lateinit var teacherAdapter: TeacherAdapter
    private lateinit var teacherList: List<UserResponseDTO>
    private lateinit var subjectList: List<SubjectResponseDTO>
    private lateinit var currentClass: SchoolClassResponseDTO
    private lateinit var selectedTeacher: UserResponseDTO
    private lateinit var selectedSubject: SubjectResponseDTO
    private var isNewSubject = true

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val dialogBuilder: AlertDialog.Builder? = activity?.let { AlertDialog.Builder(it) }

        binding = SubjectOverviewManagerFragmentBinding.inflate(inflater, container, false)
        binding.subjectVM = subjectViewModel
        binding.lifecycleOwner = this

        // Die anfänglichen Daten zeigen
        showClassName()
        requestSubjects()
        requestTeachers()

        // SetOnClickListeners
        binding.apply {
            addBtn.setOnClickListener {
                toggleAddSubjectLayout()
                toggleEditSubjectLayout()
            }
            confirmBtn.setOnClickListener { addSubject() }
            cancelBtn.setOnClickListener {
                toggleAddSubjectLayout()
                toggleEditSubjectLayout()
                resetAddSubjectLayout()
            }
            newTeacherSpinner.setOnClickListener {
                if (!isNewSubject) {
                    binding.recyclerTeacherLayout.isGone = false
                    isNewSubject = true
                } else {
                    binding.recyclerTeacherLayout.isGone = !binding.recyclerTeacherLayout.isGone
                }
                changeTeacherRecyclerViewConstraint(binding.addSubjectTexts.id)
            }
            changeTeacherSpinner.setOnClickListener {
                if (isNewSubject) {
                    binding.recyclerTeacherLayout.isGone = false
                    isNewSubject = false
                } else {
                    binding.recyclerTeacherLayout.isGone = !binding.recyclerTeacherLayout.isGone
                }
                changeTeacherRecyclerViewConstraint(binding.changeTeacherSpinner.id)
            }
            changeBtn.setOnClickListener { updateSubject() }

            deleteBtn.setOnClickListener {
                dialogBuilder!!.setMessage(getString(R.string.deleteSubjectConfirmation))
                    .setTitle(R.string.deleteDialogTitle)
                    .setPositiveButton(R.string.yesIAm) { _, _ -> deleteSubject() }
                    .setNegativeButton(R.string.cancel) { _, _ -> }.create().show()
            }
        }

        // RecyclerView für die Fächer aufbauen
        subjectAdapter = SubjectAdapterAdmin(this)
        binding.recyclerSubjectView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = subjectAdapter
            setHasFixedSize(true)
        }

        // RecyclerView für die Lehrer aufbauen
        teacherAdapter = TeacherAdapter(this)
        binding.recyclerTeacherView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = teacherAdapter
            setHasFixedSize(true)
        }
        return binding.root
    }

    /**
     * Ein neues Fach wird erstellt.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    private fun addSubject() {
        val name = binding.newSubjectEdit.text.toString()

        if (name.isEmpty()) {
            Snackbar.make(requireView(), getString(R.string.enterNameToast), Snackbar.LENGTH_LONG).show()
        } else if (!isNewSubject || !::selectedTeacher.isInitialized){
            Snackbar.make(requireView(), getString(R.string.selectTeacherToast), Snackbar.LENGTH_LONG).show()
        } else {
            lifecycleScope.launch {
                val subjectDTO = SubjectRequestDTO(name, currentClass.id, selectedTeacher.id)
                subjectViewModel.createSubject(subjectDTO)

                requestSubjects()

                toggleAddSubjectLayout()
                resetAddSubjectLayout()
                resetEditSubjectLayout()
                toggleEditSubjectLayout()
            }
        }
    }

    /**
     * Ein Fach wird geändert.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    private fun updateSubject() {
        val newName = binding.newNameEdit.text.toString()

        when {
            !::selectedSubject.isInitialized -> {
                Snackbar.make(requireView(), getString(R.string.selectSubjectToast), Snackbar.LENGTH_LONG).show()
            }
            isNewSubject || !::selectedTeacher.isInitialized -> {
                Snackbar.make(requireView(), getString(R.string.selectTeacherToast), Snackbar.LENGTH_LONG).show()
            }
            else -> {
                lifecycleScope.launch {
                    val subjectDTO = SubjectRequestDTO(newName, currentClass.id, selectedTeacher.id)
                    subjectViewModel.updateSubject(selectedSubject.id, subjectDTO)

                    requestSubjects()

                    resetEditSubjectLayout()
                }
            }
        }
    }

    /**
     * Ein Fach wird gelöscht.
     * Bei unvollständigen Daten wird eine Fehlermeldung gezeigt.
     */
    private fun deleteSubject() {
        if (!::selectedSubject.isInitialized) {
            Snackbar.make(requireView(), getString(R.string.selectSubjectToast), Snackbar.LENGTH_LONG).show()
        } else {
            lifecycleScope.launch {
                subjectViewModel.deleteSubject(selectedSubject.id)

                requestSubjects()

                resetEditSubjectLayout()
            }
        }
    }

    /**
     * Der Name der ausgewälten Klasse wird gezeigt.
     */
    private fun showClassName() {
        currentClass = classViewModel.classes.value?.get(0)!!
        binding.className.text = currentClass.name
    }

    /**
     * Eine Liste mit allen Fächern wird vom Server angefordert,
     * alphabetisch sortiert und in der Recyclerview gezeigt.
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun requestSubjects() {
        lifecycleScope.launch {
            subjectViewModel.getSubjectByClass(currentClass.id)
            subjectList = subjectViewModel.subjects.value!!.sortedBy { it.name.lowercase() }
            subjectAdapter.subjects = subjectList.toMutableList()
            subjectAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Eine Liste mit allen Lehrern wird vom Server angefordert,
     * alphabetisch sortiert und in der Recyclerview des Spinners gezeigt.
     */
    @SuppressLint("NotifyDataSetChanged")
    private fun requestTeachers() {
        lifecycleScope.launch {
            userViewModel.getUserByRole(UserRole.ROLE_TEACHER.toString())
            teacherList = userViewModel.users.value!!.sortedBy { it.firstName.lowercase() }
            teacherAdapter.teachers = teacherList.toMutableList()
            teacherAdapter.notifyDataSetChanged()
        }
    }

    /**
     * Der Name des ausgewählten Fachs wird auf der Ansicht gezeigt.
     */
    override fun onSubjectClick(position: Int) {
        selectedSubject = subjectList[position]
        selectedTeacher = selectedSubject.teacher
        isNewSubject = false

        binding.selectedSubject.text = selectedSubject.name
        binding.newNameEdit.setText(selectedSubject.name)

        binding.changeTeacherSpinner.text = selectedTeacherName()
    }

    /**
     * Der Name des ausgewählten Lehrers wird auf der Ansicht gezeigt.
     */
    override fun onTeacherClick(position: Int) {
        selectedTeacher = teacherList[position]
        updateSpinnerName(selectedTeacherName())
        binding.recyclerTeacherLayout.isGone = true
    }

    /**
     * Der Name des Lehrers wird in dem Spinner aktuallisiert.
     */
    private fun updateSpinnerName(name: String) {
        when (isNewSubject) {
            true -> binding.newTeacherSpinner.text = name
            false -> binding.changeTeacherSpinner.text = name
        }
    }

    /**
     * Diese Methode schließt die Tastatur.
     */
    private fun closeKeyBoard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /**
     * Der Name des Lehrers in bestimmten Format zurückgeben.
     */
    private fun selectedTeacherName(): String{
        return "${selectedTeacher.firstName} ${selectedTeacher.lastName} (${selectedTeacher.username})"
    }

    /**
     * Die Elemente für das Hinzufügen eines Fachs umschalten.
     */
    private fun toggleAddSubjectLayout() {
        binding.apply {
            addSubjectTexts.isGone = !binding.addSubjectTexts.isGone
            addSubjectButtons.isGone = !binding.addSubjectButtons.isGone
            addBtn.isGone = !binding.addBtn.isGone
        }
    }

    private fun toggleEditSubjectLayout() {
        binding.apply {
            editSubjectLayout.isGone = !editSubjectLayout.isGone
        }
    }
    /**
     * Die Elemente für das Hinzufügen eines Fachs zurücksetzen.
     */
    private fun resetAddSubjectLayout() {
        binding.newSubjectEdit.text.clear()
        binding.newSubjectEdit.clearFocus()
        binding.newTeacherSpinner.text = getString(R.string.selectTeacher)
        closeKeyBoard(binding.newSubjectEdit)
    }

    /**
     * Die Elemente für die Bearbeitung eines Fachs zurücksetzen.
     */
    private fun resetEditSubjectLayout() {
        binding.selectedSubject.text = getString(R.string.none)
        binding.newNameEdit.text.clear()
        binding.newNameEdit.clearFocus()
        binding.changeTeacherSpinner.text = getString(R.string.selectTeacher)
        closeKeyBoard(binding.newNameEdit)
    }

    /**
     * Das TeacherRecyclerView wird an der entsprechenden Stelle verschoben.
     */
    private fun changeTeacherRecyclerViewConstraint(id: Int) {
        val params = binding.recyclerTeacherLayout.layoutParams as ConstraintLayout.LayoutParams
        params.topToBottom = id
        binding.recyclerTeacherLayout.requestLayout()
    }
}