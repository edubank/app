package com.edubank.view.admin

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DisplayClassDetailsTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        // Login
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())
        TestData.waitForServer()

        // Add Class
        onView(withId(R.id.classManagerButton)).perform(click())
        onView(withId(R.id.newClassEdit)).perform(replaceText(" - Test Class"))
        onView(withId(R.id.addBtn)).perform(click())
        pressBack()

        // Select Class
        TestData.waitForServer()
        onView(withId(R.id.recyclerClassView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
    }

    @Test
    fun testClassDetailsAreDisplayed() {
        // Check if view elements are shown
        onView(withId(R.id.className)).check(matches(isDisplayed()))
        onView(withId(R.id.editClassBtn)).check(matches(isDisplayed()))
        onView(withId(R.id.subjectsTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerSubjectView)).check(matches(isDisplayed()))

        // Check if data is correct
        onView(withId(R.id.className)).check(matches(ViewMatchers.withText(" - Test Class")))
        pressBack()
    }

    @After
    fun tearDown() {
        // Go to ClassManager
        onView(withId(R.id.classManagerButton)).perform(click())
        // Select class to delete
        onView(withId(R.id.recyclerClassView)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        // Delete class
        onView(withId(R.id.deleteBtn)).perform(click())
        // Dialog click confirm
        onView(withId(android.R.id.button1)).perform(click())
    }
}

