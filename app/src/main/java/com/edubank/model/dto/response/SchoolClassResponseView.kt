package com.edubank.model.dto.response

/**
 * SchoolClassResponseView ist eine Daten-Klasse für Klassen-Daten.
 * Die Klasse ist eine Antwort-Klasse des Servers.
 *
 * @property id Id der Klasse
 * @property name Name der Klasse
 */
data class SchoolClassResponseView (
    val id : Int,
    val name : String
)