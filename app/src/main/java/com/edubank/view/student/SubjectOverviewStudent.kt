package com.edubank.view.student

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.edubank.R
import com.edubank.databinding.SubjectOverviewStudentFragmentBinding
import com.edubank.model.dto.response.AccountResponseDTO
import com.edubank.model.dto.response.SubjectClassResponseView
import com.edubank.view.shared.SubjectAccount
import com.edubank.view.shared.SubjectOverview
import com.edubank.viewmodel.AccountViewModel
import com.edubank.viewmodel.UserViewModel
import kotlinx.coroutines.launch

/**
 * Das Fragment SubjectOverviewStudent stellt die Fachübersicht des Schülers dar.
 *
 * @author Team EduBank
 */
class SubjectOverviewStudent : SubjectOverview(), SubjectAdapterStudent.OnItemClickListener {
    private val accountViewModel: AccountViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()

    private lateinit var binding: SubjectOverviewStudentFragmentBinding
    private lateinit var subjectAdapter: SubjectAdapterStudent

    private lateinit var accountList: List<AccountResponseDTO>
    private lateinit var subjectListResView: MutableList<SubjectClassResponseView>
    private var balance = mutableListOf<Double>()
    private var userId = -1

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SubjectOverviewStudentFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userViewModel
        binding.accountVM = accountViewModel
        binding.lifecycleOwner = this

        // RecyclerView aufbauen
        subjectAdapter = SubjectAdapterStudent(this)
        binding.recyclerSubjectView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = subjectAdapter
            setHasFixedSize(true)
        }

        // Die anfänglichen Daten zeigen
        requestSubjects()
        showHeadlines()
        return binding.root
    }

    /**
     * Fordert die Benutzerdaten (Benutzer und Accounts)
     * des angemeldeten Schülers vom Server an und setzt diese.
     */
    private suspend fun getAccountData() {
        // fordere accounts an
        userViewModel.getMyUser()
        userId = userViewModel.users.value?.get(0)?.id!!
        accountViewModel.getStudentAccounts(userId)
        accountList = accountViewModel.accounts.value!!.sortedBy { it.subject.name.lowercase() }
    }

    /**
     * Zeigt den Schülernamen, Benutzernamen und die Klasse als Überschrift an,
     * falls der Schüler eingeteilt wurde und der Schüler nicht in mehreren Klassen eingeteilt ist.
     *
     */
    private fun showHeadlines() {
        lifecycleScope.launch {
            getAccountData()

            // setze bindings des Schülernamen und Klassennamen
            val user = userViewModel.users.value!!
            val studentFirstName = user[0].firstName
            val studentLastName = user[0].lastName
            val studentUsername = user[0].username
            val studentName = "$studentFirstName $studentLastName ($studentUsername)"

            // setze Klassennamen nur, wenn es eine Klasse für diesen Schüler gibt
            // sonst setze es auf leer
            when {
                accountList.isNotEmpty() -> {
                    val name = accountList[0].subject.schoolClass.name
                    binding.classNameOfStudent.text = name
                    for (accounts in accountList) {
                        when {
                            name != accounts.subject.schoolClass.name -> binding.classNameOfStudent.text =
                                ""
                        }
                    }
                }
                else -> binding.classNameOfStudent.text = getString(R.string.notAssigned)
            }
            //Schülername
            binding.studentName.text = studentName
        }
    }


    /**
     * Fügt Fächer, die den Accounts des Benutzers zugeteilt sind in eine Liste (subjectListResView)
     * ein und zeigt diese in der RecyclerView an.
     */
    override fun requestSubjects() {
        lifecycleScope.launch {

            getAccountData()
            subjectListResView = mutableListOf()
            if (accountList.isNotEmpty()) {
                for (account in accountList) {
                    subjectListResView.add(account.subject)
                    balance.add(account.balance)
                }
            }
            subjectAdapter.onSubjectsChanged(subjectListResView, balance)
        }
    }

    /**
     * Wird aufgerufen wenn ein Fach angeklickt wird.
     * Öffnet die Ansicht für das angeklickte Fach.
     *
     * @see SubjectAccount
     */
    override fun showSubject() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, SubjectAccount()).addToBackStack(null).commit()
    }


    /**
     * Die Methode setzt im AccountViewModel das ausgewählte Fach des Schülers an erster Stelle.
     * Danach wird das Fragment ausgetauscht.
     *
     * @param position Position des Fachs
     */
    override fun onSubjectClick(position: Int) {
        lifecycleScope.launch {
            val clickedAccount = accountList[position]
            accountViewModel.getAccount(clickedAccount.id)
            showSubject()
        }

    }
}