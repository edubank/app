package com.edubank.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.edubank.model.dto.request.SubjectRequestDTO
import com.edubank.model.dto.response.AccountResponseView
import com.edubank.model.dto.response.SubjectResponseDTO
import com.edubank.model.retrofitclient.SubjectService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SubjectViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()
    private lateinit var subjectVM : SubjectViewModel
    @get:Rule
    private val mockWebServer = MockWebServer()
    private val subjectAPI by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(SubjectService::class.java)
    }
    private val httpClient = OkHttpClient.Builder()
        .readTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .build()
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Before
    fun setup() {
        mockWebServer.start()
        subjectVM = SubjectViewModel()
        subjectVM.setService(subjectAPI)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getSubjectById_success() = runBlocking {
        val jsonData = getDataObject("subject_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
            .setResponseCode(200)
            .setBody(jsonData))

        assertTrue(subjectVM.getSubjectById(1) == 200)
        assertTrue(subjectVM.subjects.value == listOf(gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)))
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectById_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
            .setResponseCode(400))

        assertTrue(subjectVM.getSubjectById(1) == 400)
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectByClass_success() = runBlocking {
        val jsonData = getDataObject("subject_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(subjectVM.getSubjectByClass(1) == 200)
        assertTrue(subjectVM.subjects.value == gson.fromJson<List<SubjectResponseDTO>>(
            jsonData, object : TypeToken<List<SubjectResponseDTO>>() {}.type))
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 2)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectByClass_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(subjectVM.getSubjectByClass(1) == 400)
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getAllSubjects_success() = runBlocking {
        val jsonData = getDataObject("subject_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(subjectVM.getAllSubjects() == 200)
        assertTrue(subjectVM.subjects.value == gson.fromJson<List<SubjectResponseDTO>>(
            jsonData, object : TypeToken<List<SubjectResponseDTO>>() {}.type))
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 2)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getAllSubjects_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(subjectVM.getAllSubjects() == 400)
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectsOfTeacher_success() = runBlocking {
        val jsonData = getDataObject("subject_response_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))

        assertTrue(subjectVM.getSubjectsOfTeacher(1) == 200)
        assertTrue(subjectVM.subjects.value == gson.fromJson<List<SubjectResponseDTO>>(
            jsonData, object : TypeToken<List<SubjectResponseDTO>>() {}.type))
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 2)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun getSubjectsOfTeacher_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))

        assertTrue(subjectVM.getSubjectsOfTeacher(1) == 400)
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun createSubject_success() = runBlocking {
        val jsonData = getDataObject("subject_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val respDTO = gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)
        val requestDTO = SubjectRequestDTO(respDTO.name, respDTO.schoolClass.id, respDTO.teacher.id)

        assertTrue(subjectVM.createSubject(requestDTO) == 200)
        assertTrue(subjectVM.subjects.value == listOf(gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)))
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun createSubject_failed() = runBlocking {
        val jsonData = getDataObject("subject_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))
        val respDTO = gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)
        val requestDTO = SubjectRequestDTO(respDTO.name, respDTO.schoolClass.id, respDTO.teacher.id)

        assertTrue(subjectVM.createSubject(requestDTO) == 400)
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateSubject_success() = runBlocking {
        val jsonData = getDataObject("subject_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val respDTO = gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)
        val requestDTO = SubjectRequestDTO(respDTO.name, respDTO.schoolClass.id, respDTO.teacher.id)

        assertTrue(subjectVM.updateSubject(1, requestDTO) == 200)
        assertTrue(subjectVM.subjects.value == listOf(gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)))
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 1)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun updateSubject_failed() = runBlocking {
        val jsonData = getDataObject("subject_response_item.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))
        val respDTO = gson.fromJson<SubjectResponseDTO>(
            jsonData, object : TypeToken<SubjectResponseDTO>() {}.type)
        val requestDTO = SubjectRequestDTO(respDTO.name, respDTO.schoolClass.id, respDTO.teacher.id)

        assertTrue(subjectVM.updateSubject(1, requestDTO) == 400)
        if (subjectVM.subjects.value != null) {
            assertTrue(subjectVM.subjects.value!!.size == 0)
        } else {
            assertTrue(false)
        }
    }

    @Test
    fun addStudents_success() = runBlocking {
        val jsonData = getDataObject("account_response_view_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val response = subjectVM.addStudents(1, listOf(1, 2))

        assertTrue(response == gson.fromJson<List<AccountResponseView>>(
            jsonData, object : TypeToken<List<AccountResponseView>>() {}.type))
    }

    @Test
    fun addStudents_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))
        val response = subjectVM.addStudents(1, listOf(1, 2))
        assertTrue(response == null)
    }

    @Test
    fun dropStudents_success() = runBlocking {
        val jsonData = getDataObject("account_response_view_list.json")
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(jsonData))
        val response = subjectVM.dropStudents(1, listOf(1, 2))

        assertTrue(response == gson.fromJson<List<AccountResponseView>>(
            jsonData, object : TypeToken<List<AccountResponseView>>() {}.type))
    }

    @Test
    fun dropStudents_failed() = runBlocking {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(400))
        val response = subjectVM.dropStudents(1, listOf(1, 2))
        assertTrue(response == null)
    }

    @Test
    fun deleteSubject_success() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(200)
            .setBody("\"true\""))
        assertTrue(subjectVM.deleteSubject(1) == 200)
    }

    @Test
    fun deleteSubject_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setResponseCode(400)
            .setBody("\"false\""))
        assertTrue(subjectVM.deleteSubject(1) == 400)
    }
}