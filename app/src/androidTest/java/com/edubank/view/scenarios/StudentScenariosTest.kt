package com.edubank.view.scenarios

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.shared.MainActivity
import org.junit.Rule
import org.junit.Test

/**
 * Beinhaltet Tests zu den Schüler-Szenarien aus dem Pflichtenheft im Abschnitt 8.1.
 *
 * @author Team EduBank
 */
class StudentScenariosTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Prüft, ob das Szenario "Anschauen des Transaktionsverlaufs" funktioniert.
     */
    @Test
    fun test_CheckTransactions() { // login as student
        onView(ViewMatchers.withId(R.id.txt_login_username)).perform(replaceText(TestData.studentUsername))
        onView(ViewMatchers.withId(R.id.txt_login_password)).perform(replaceText(TestData.studentPassword))
        onView(ViewMatchers.withId(R.id.btn_login_login)).perform(click())

        TestData.waitForServer()

        // click on the first subject from the recycler view
        onView(ViewMatchers.withId(R.id.recyclerSubjectView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0, click()
            )
        )

        TestData.waitForServer()

        // check if the correct fragment is displayed
        onView(ViewMatchers.withId(R.id.subject_account_fragment)).check(
            ViewAssertions.matches(ViewMatchers.isDisplayed())
        )

        // check if all elements for the fragment are displayed
        onView(ViewMatchers.withId(R.id.subjectName)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.headerRoleName)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.balanceCaption)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.history)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.recyclerTransactionView)).check(
            ViewAssertions.matches(ViewMatchers.isDisplayed())
        )

        // logout
        Espresso.openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(ViewMatchers.withText("Ausloggen")).perform(click())

        // check for successful logout
        onView(ViewMatchers.withId(R.id.login_fragment)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}