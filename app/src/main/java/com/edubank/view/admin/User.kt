package com.edubank.view.admin

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.edubank.R
import com.edubank.databinding.UserFragmentBinding
import com.edubank.model.dto.request.UserRequestDTO
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet eine Übersicht von einem konkreten (ausgewählten) Benutzer.
 *
 * @author Team EduBank
 */
class User : Fragment() {

    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var binding : UserFragmentBinding
    private lateinit var currentUser: UserResponseDTO
    private lateinit var currentRole: UserRole
    private lateinit var selectedRole: UserRole

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val dialogBuilder: AlertDialog.Builder? = activity?.let { AlertDialog.Builder(it) }

        binding = UserFragmentBinding.inflate(inflater, container, false)
        binding.userVM = userViewModel
        binding.lifecycleOwner = this

        // Die anfänglichen Daten zeigen
        showUserData()

        // SetOnClickListeners
        binding.apply {
            saveChangesBtn.setOnClickListener { updateUserData() }
            deleteUserBtn.setOnClickListener {
                dialogBuilder!!.setMessage(getString(R.string.deleteAccountConfirmation))
                    .setTitle(R.string.deleteDialogTitle)
                    .setPositiveButton(R.string.yesIAm) { _, _ -> deleteUser() }
                    .setNegativeButton(R.string.cancel) { _, _ -> }.create().show()
            }
            passwordGen.setOnClickListener { copyPasswordToClipboard() }
        }

        setupSpinner()

        return binding.root
    }

    /**
     * Die Daten des ausgewählten Benutzers werden auf der Ansicht gezeigt.
     * Das generierte Password wird nach der ersten Anmeldung des Benutzers nicht mehr gezeigt.
     */
    private fun showUserData() {
        currentUser = userViewModel.users.value?.get(0)!!
        val name = "${currentUser.firstName} ${currentUser.lastName} (${currentUser.username})"
        currentRole = currentUser.role

        binding.apply {
            userName.text = name
            userRole.text = roleToText(currentRole)
            firstNameEdit.setText(currentUser.firstName)
            lastNameEdit.setText(currentUser.lastName)
            emailEdit.setText(currentUser.email)
            roleSpinner.post { roleSpinner.setSelection(getRoleIndex(currentRole)) }
            passwordGen.text = currentUser.passwordGen
        }
        if (binding.passwordGen.text.isEmpty()) {
            binding.passwordGenTitle.isGone = true
        }
    }

    /**
     * Die Daten des Benutzers werden geändert.
     * Bei unvollständigen Daten, E-Mail in falschem Format oder unerlaubtem
     * Wechsel der Rolle wird eine Fehlermeldung gezeigt.
     */
    private fun updateUserData() {
        val firstName = binding.firstNameEdit.text.toString()
        val lastName = binding.lastNameEdit.text.toString()
        val email = binding.emailEdit.text.toString()
        val userDTO = UserRequestDTO(selectedRole.toString(), firstName, lastName, email)

        lifecycleScope.launch {
            val response = userViewModel.updateUser(userDTO, currentUser.id)
            if (response == 200) {
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, UserOverview()).commit()
            } else {
                when {
                    firstName.isEmpty() ->
                        Snackbar.make(requireView(), getString(R.string.emptyFirstNameToast), Snackbar.LENGTH_LONG).show()
                    lastName.isEmpty() ->
                        Snackbar.make(requireView(), getString(R.string.emptyLastNameToast), Snackbar.LENGTH_LONG).show()
                    (currentRole == UserRole.ROLE_STUDENT && selectedRole != UserRole.ROLE_STUDENT) ->
                        Snackbar.make(requireView(), getString(R.string.changeStudentRoleToast), Snackbar.LENGTH_LONG).show()
                    (currentRole != UserRole.ROLE_STUDENT && selectedRole == UserRole.ROLE_STUDENT) ->
                        Snackbar.make(requireView(), getString(R.string.changeToStudentToast), Snackbar.LENGTH_LONG).show()
                    else ->
                        Snackbar.make(requireView(), getString(R.string.invalidEmail), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    /**
     * Der ausgewählte Benutzer wird gelöscht.
     * Die aktuelle Ansicht wird mit der UserOverview-Ansicht
     * (die Ansicht mit allen Benutzern) ausgetauscht.
     */
    private fun deleteUser() {
        lifecycleScope.launch {
            val response = userViewModel.deleteUser(currentUser.id)

            if (response == 400) {
                Snackbar.make(requireView(), getString(R.string.cantDeleteUser), Snackbar.LENGTH_LONG).show()
            } else {
                requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, UserOverview())
                    .commit()
            }
        }
    }

    /**
     * Die Rolle eines Benutzers in einem passenden und lesbaren Format umwandeln.
     */
    private fun roleToText(role: UserRole): String {
        return when (role) {
            UserRole.ROLE_STUDENT -> resources.getStringArray(R.array.roles)[0]
            UserRole.ROLE_TEACHER -> resources.getStringArray(R.array.roles)[1]
            UserRole.ROLE_ADMIN -> resources.getStringArray(R.array.roles)[2]
        }
    }

    /**
     * Die Rolle eines Benutzers vom Textformat zu UserRole-Format
     * (schon definierte Parameter im Projekt) umwandeln.
     */
    private fun textToRole(text: String): UserRole {
        return when (text) {
            resources.getStringArray(R.array.roles)[0] -> UserRole.ROLE_STUDENT
            resources.getStringArray(R.array.roles)[1] -> UserRole.ROLE_TEACHER
            else -> UserRole.ROLE_ADMIN
        }
    }

    /**
     * Der Index der Rolle bekommen.
     */
    private fun getRoleIndex(role: UserRole): Int {
        return when (role) {
           UserRole.ROLE_STUDENT -> 0
           UserRole.ROLE_TEACHER -> 1
           UserRole.ROLE_ADMIN -> 2
       }
    }

    /**
     * Das Spinner für die Auswahl von der Rolle des Benutzers aufbauen.
     */
    private fun setupSpinner() {
        val roleSpinner = binding.roleSpinner
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.roles))
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        roleSpinner.adapter = adapter

        roleSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedRole = textToRole(parent?.getItemAtPosition(position).toString())
            }
        }
    }

    private fun copyPasswordToClipboard() {
        val password = binding.passwordGen.text
        val clipboardManager = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", password)
        clipboardManager.setPrimaryClip(clipData)
        Snackbar.make(requireView(), R.string.passwordCopiedToClipboard, Snackbar.LENGTH_LONG).show()
    }
}