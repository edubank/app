package com.edubank.view.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.edubank.R
import com.edubank.databinding.LoginFragmentBinding
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.dto.response.UserRole
import com.edubank.view.admin.ClassOverview
import com.edubank.view.student.SubjectOverviewStudent
import com.edubank.view.teacher.SubjectOverviewTeacher
import com.edubank.viewmodel.LoginViewModel
import com.edubank.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

/**
 * Das Fragment bildet den Login-Screen und bietet die Funktionalität zur Anmeldung.
 *
 * @author Team EduBank
 */
class Login : Fragment() {

    private val loginVM: LoginViewModel by activityViewModels()
    private val userVM: UserViewModel by activityViewModels()
    private lateinit var binding: LoginFragmentBinding

    /**
     * Legt die Listener für die UI-Elemente fest.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = LoginFragmentBinding.inflate(inflater, container, false)
        binding.loginVM = loginVM
        binding.userVM = userVM
        binding.lifecycleOwner = this

        binding.btnLoginLogin.setOnClickListener {
            login()
        }

        binding.btnLoginForgotPassword.setOnClickListener {
            forgotPassword()
        }

        return binding.root
    }

    /**
     * Initiiert den Login-Vorgang und wechselt auf die neue Ansicht, falls die Anmeldung erfolgreich war.
     *
     * @see SubjectOverviewStudent
     * @see SubjectOverviewTeacher
     * @see ClassOverview
     */
    private fun login() {
        lifecycleScope.launch {
            var user: UserResponseDTO? = null
            toggleLoginPhase()

            launch {
                val loginCode = loginVM.login(
                    binding.txtLoginUsername.text.toString(), binding.txtLoginPassword.text.toString()
                )

                if (loginCode in 200..299) {
                    userVM.getMyUser()

                    if (userVM.users.value != null && userVM.users.value?.isNotEmpty() == true) {
                        user = userVM.users.value?.get(0)
                    } else { //User successfully logged in but still no user data available -> issue with the server
                        toggleLoginPhase()
                        Snackbar.make(requireView(), getString(R.string.serverProblem), Snackbar.LENGTH_LONG).show()
                    }
                } else if (loginCode == 0) { //Server is not responding -> no internet or server is down
                    toggleLoginPhase()
                    Snackbar.make(requireView(), getString(R.string.noServerConnection), Snackbar.LENGTH_LONG).show()
                } else { //Server responded with an error -> most likely invalid login data
                    toggleLoginPhase()
                    Snackbar.make(requireView(), getString(R.string.invalidLoginData), Snackbar.LENGTH_LONG).show()
                }
            }.join()

            if (user?.passwordGen != null && user?.email.isNullOrBlank()) { //initial login
                val initialSettings = Settings(true)
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.main_container, initialSettings).commit()
            } else if (user != null) {
                (activity as MainActivity).toggleMenuVisibility() //Make menu visible
                when (user?.role) {
                    UserRole.ROLE_STUDENT -> {
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.main_container, SubjectOverviewStudent()).commit()
                    }
                    UserRole.ROLE_TEACHER -> {
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.main_container, SubjectOverviewTeacher()).commit()
                    }
                    UserRole.ROLE_ADMIN   -> {
                        (activity as MainActivity).setAdminStatus(true)
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.main_container, ClassOverview()).commit()
                    }
                }

                if (user?.passwordGen != null) Snackbar.make(
                    requireView(), R.string.changePasswordMessage, Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    /**
     * Die Methode tauscht die aktuelle Ansicht mit der ForgotPassword-Ansicht aus.
     */
    private fun forgotPassword() {
        requireActivity().supportFragmentManager.beginTransaction().replace(R.id.main_container, ForgotPassword())
            .addToBackStack(null).commit()
    }

    /**
     * Wechselt die Phase des Login-Vorgangs. Entweder wurden Daten eingegeben und eine Anfrage zum Server läuft oder
     * nicht.
     */
    private fun toggleLoginPhase() {
        binding.apply {
            if (pbLoginLogin.visibility == View.VISIBLE) {
                pbLoginLogin.visibility = View.GONE
            } else {
                pbLoginLogin.visibility = View.VISIBLE
            }

            btnLoginForgotPassword.isClickable = !btnLoginForgotPassword.isClickable
            btnLoginLogin.isClickable = !btnLoginLogin.isClickable
            txtLoginUsername.isEnabled = !txtLoginUsername.isEnabled
            txtLoginPassword.isEnabled = !txtLoginPassword.isEnabled
        }
    }
}