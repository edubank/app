package com.edubank.model.retrofitclient

import com.edubank.model.dto.request.ProfileDataRequestDTO
import com.edubank.model.dto.request.UserRequestDTO
import com.edubank.model.dto.response.UserResponseDTO
import retrofit2.Call
import retrofit2.http.*

/**
 * Scnittstellen-Beschreibung für den Retrofit-Service UserService.
 * Die Schnittstelle beschreibt die Benutzer-Anfragen an den Server.
 *
 * @author Team EduBank
 */
interface UserService {

    /**
     * Die Funktion setInitialData setzt die Nutzereinstellungen der aktuell angemeldeten Nutzers initial auf dem Server.
     * Wenn die email oder password nicht neu gesetzt werden soll, kann ein leerer String übergeben werden,
     * sodass nur ein Wert gesetzt wird.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param profileData Initiale Profil-Daten des aktuell angemeldeten Benutzers.
     * @return Call-Objekt mit den Benutzerdaten des aktuell angemeldeten Benutzers.
     */
    @POST("initial-data")
    fun setInitialData(@Body profileData : ProfileDataRequestDTO) : Call<UserResponseDTO>

    /**
     * Die Funktion getMyUsers frägt die Benutzerdaten des aktuell angemeldeten Nutzers beim Server an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @return Call-Objekt mit den Benutzer-Daten des aktuell angemeldeten Benutzers.
     */
    @GET("me")
    fun getMyUser() : Call<UserResponseDTO>

    /**
     * Die Funktion updateSettings aktualisert die Benutzerdaten des aktuell angemeldeten Nutzers auf dem Server an.
     * Wenn die email oder password nicht neu gesetzt werden soll, kann ein leerer String übergeben werden,
     * sodass nur ein Wert gesetzt wird.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param profileData Aktualisierte Profil-Daten des aktuell angemeldeten Benutzers.
     * @return Call-Objekt mit den Benutzerdaten des aktuell angemeldeten Benutzers.
     */
    @PUT("me")
    fun updateSettings(@Body profileData : ProfileDataRequestDTO) : Call<UserResponseDTO>

    /**
     * Die Funktion getAllUsers frägt alle Benutzer beim Server an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @return Call-Objekt mit einer Benutzer-Daten-Liste.
     */
    @GET("users")
    fun getAllUsers() : Call<List<UserResponseDTO>>

    /**
     * Die Funktion getUserByRole frägt die Benutzerdaten eines bestimmten Benutzertyps an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param role Benutzertyp der angefragten Benutzer.
     * @return Call-Objekt mit einer Benutzer-Daten-Liste vom Benutzertyp role.
     */
    @GET("users")
    fun getUserByRole(@Query("role") role : String) : Call<List<UserResponseDTO>>

    /**
     * Die Funktion getUserByRole frägt die Benutzerdaten eines bestimmten Benutzertyps an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param subjectId Id des Faches, dessen Schüler angefragt werden sollen.
     * @return Call-Objekt mit einer Benutzer-Daten-Liste mit Benutzers des Faches mit der Id subjectId.
     */
    @GET("users")
    fun getUserBySubjectId(@Query("subject_id") subjectId : Int) : Call<List<UserResponseDTO>>

    /**
     * Die Funktion getUserById frägt die Benutzerdaten eines Nutzers an.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param userId Id des Benutzers.
     * @return Call-Objekt mit den Benutzer-Daten.
     */
    @GET("users/{id}")
    fun getUserById(@Path("id") userId : Int) : Call<UserResponseDTO>

    /**
     * Die Funktion createUser erstellt eine Benutzer auf dem Server.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param user Die Benutzerdaten des neu anzulegenden Benutzers.
     * @return Call-Objekt mit den Benutzer-Daten des neu angelegten Benutzers.
     */
    @POST("users")
    fun createUser(@Body user : UserRequestDTO) : Call<UserResponseDTO>

    /**
     * Die Funktion updateUser aktualisert die Benutzerdaten eines Benutzers auf dem Server.
     * Wenn die Anfrage erfolgreich war, enthält das zurückgegebene Call-Objekt den ResponseCode = 2xx.
     *
     * @param userId Benutzer-ID des Benutzers, der aktualisert werden soll.
     * @param user Die aktualisierten Benutzerdaten.
     * @return Call-Objekt mit den Benutzer-Daten des aktualisierten Benutzers.
     */
    @PUT("users/{id}")
    fun updateUser(@Path("id") userId : Int, @Body user : UserRequestDTO) : Call<UserResponseDTO>

    /**
     * Die Funktion deleteUser löscht die Benutzerdaten eines Benutzers auf dem Server.
     *
     * @param userId Benutzer-ID des Benutzers, der gelöscht werden soll.
     * @return Call-Objekt mit einem Boolean im Body, welcher den Erfolg der Löschanfrage angibt.
     */
    @DELETE("users/{id}")
    fun deleteUser(@Path("id") userId : Int) : Call<Boolean>

}