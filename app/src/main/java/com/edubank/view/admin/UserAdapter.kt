package com.edubank.view.admin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edubank.R
import com.edubank.databinding.UserItemBinding
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.dto.response.UserRole

/**
 * Adapter für die Anzeige von Benutzer im UserOverview-Fragment.
 * Weitere Erklärungen zu den einzelnen Methoden finden sich in
 * der Dokumentation von RecyclerView.Adapter
 *
 * @see RecyclerView.Adapter
 * @see UserOverview
 * @property users Liste mit alle Benutzer die in der RecyclerView gezeigt werden
 * @author Team EduBank
 */
class UserAdapter(private val listener: OnItemClickListener):
    RecyclerView.Adapter<UserAdapter.UserViewHolder>(){

    var users = mutableListOf<UserResponseDTO>()

    /**
     * Erstellt neue Ansichten (Aufgerufen von LayoutManager)
     */
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): UserViewHolder {

        // Erstellt eine neue Ansicht, die die Benutzeroberfläche des Listenelements definiert
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.user_item, viewGroup, false)

        return UserViewHolder(view)
    }

    /**
     * Ersetzt den Inhalt einer Ansicht (Aufgerufen von LayoutManager)
     */
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

        // Hole das Element an dieser Position
        // und ersetze den Inhalt der Ansicht durch dieses Element
        val currentItem = users[position]
        val name = "${currentItem.firstName} ${currentItem.lastName} (${currentItem.username})"
        holder.userName.text = name
        holder.role.text = roleToText(currentItem.role)
    }

    /**
     * Gibt die Anzahl der Benutzer zurück, die der Adapter verwaltet.
     */
    override fun getItemCount() = users.size

    /**
     * Das ViewHolder wird benutzt um die Daten anzuzeigen
     * und das onClickListener zu setzen
     */
    inner class UserViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding = UserItemBinding.bind(itemView)
        val userName = binding.userName
        val role = binding.role

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = bindingAdapterPosition
            listener.onItemClick(position)
        }
    }

    /**
     * Die Rolle eines Benutzers in einem passenden und lesbaren Format umwandeln.
     */
    private fun roleToText(role: UserRole): String {
        return when (role) {
             UserRole.ROLE_STUDENT -> "Schüler"
             UserRole.ROLE_TEACHER -> "Lehrer"
             UserRole.ROLE_ADMIN -> "Administrator"
         }
    }

    /**
     * Die Liste mit allen Benutzern aktualisieren.
     */
    fun updateList(updatedList: MutableList<UserResponseDTO>) {
        users = updatedList
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}