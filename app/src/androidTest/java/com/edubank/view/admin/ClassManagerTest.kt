package com.edubank.view.admin

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers.allOf
import org.junit.*

/**
 * Beinhaltet allgemeine UI-Tests für ClassManager.
 *
 * @see ClassManager
 * @author Team EduBank
 */
class ClassManagerTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Administrator ein und navigiert zum Fragment ClassManager.
     */
    @Before
    fun setUp() {
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()

        onView(withId(R.id.classManagerButton)).perform(click())

        waitForServer()
    }

    /**
     * Erstellt eine Klasse und löscht diese anschließend wieder.
     */
    @Test
    fun test_createAndDeleteClass() { //create new class
        onView(withId(R.id.newClassEdit)).perform(replaceText(TestData.deleteClass))
        onView(withId(R.id.addBtn)).perform(click())

        // check if class has been created
        onView(allOf(withId(R.id.classItem), hasDescendant(withText(TestData.deleteClass)))).check(
            matches(isDisplayed())
        )

        // delete created class
        onView(withId(R.id.recyclerClassView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ClassAdapter.ClassViewHolder>(
                0, click()
            )
        )
        onView(withId(R.id.deleteBtn)).perform(click())
        onView(withId(android.R.id.button1)).perform(click())

        // check if class has been deleted
        onView(allOf(withId(R.id.classItem), hasDescendant(withText(TestData.deleteClass)))).check(
            doesNotExist()
        )
    }
}