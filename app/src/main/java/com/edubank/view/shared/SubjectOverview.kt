package com.edubank.view.shared

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.edubank.viewmodel.SubjectViewModel

/**
 * Das Fragment bildet den Fächerübersicht-Screen
 * und stellt die benötigten Funktionalitäten zur Verfügung.
 * Dazu müssen die spezifischen Benutzeransichten diese abstrakte Klasse implementieren.
 *
 * @author Team EduBank
 */
abstract class SubjectOverview : Fragment() {

    // View-Model
    private val viewModel: SubjectViewModel by activityViewModels()

    /**
     * Die Methode wird aufgerufen, wenn das Fragment neu gebaut wird.
     * Hier werden die Listener für die UI-Elemente
     * festgelegt. Mit Databinding wird die View an das ViewModel angebunden.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    /**
     * Die Methode wird dem SubjectAdapter als onClick-Listener übergeben, für einen Listeneintrag.
     *
     */
    abstract fun showSubject()


    /**
     * Die Methode kann verwendet werden um Daten vom Server anzufragen.
     * Die Daten zur Anzeige werden bestimmt.
     *
     */
    abstract fun requestSubjects()
}