package com.edubank.model.dto.request

/**
 * SchoolClassRequestDTO ist eine Daten-Klasse für Klassen-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property name Name des Klasse.
 */
data class SchoolClassRequestDTO(
    val name : String
)