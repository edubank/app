package com.edubank.viewmodel

import com.edubank.model.retrofitclient.LoginService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class LoginViewModelTest{

    private lateinit var loginVM : LoginViewModel
    @get:Rule
    private val mockWebServer = MockWebServer()
    private val loginAPI by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(LoginService::class.java)
    }
    private val httpClient = OkHttpClient.Builder()
        .readTimeout(5, TimeUnit.SECONDS)
        .writeTimeout(5, TimeUnit.SECONDS)
        .build()
    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    @Before
    fun setup() {
        mockWebServer.start()
        loginVM = LoginViewModel()
        loginVM.setService(loginAPI)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun login_success() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(200))
        assertTrue(loginVM.login("admin", "admin") == 200)
    }

    @Test
    fun login_no_response() = runBlocking {
        mockWebServer.enqueue(MockResponse()
            .setBodyDelay(6, TimeUnit.SECONDS)
            .setHeadersDelay(6, TimeUnit.SECONDS))
        assertTrue(loginVM.login("admin", "admin") == 0)
    }

    @Test
    fun login_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(400))
        assertTrue(loginVM.login("admin", "admin") == 400)
    }

    @Test
    fun logout_success() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(200))
        assertTrue(loginVM.logout() == 200)
    }

    @Test
    fun logout_failed() = runBlocking {
        mockWebServer.enqueue(MockResponse().setResponseCode(400))
        assertTrue(loginVM.logout() == 400)
    }

}