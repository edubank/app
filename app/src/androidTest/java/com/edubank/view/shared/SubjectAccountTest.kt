package com.edubank.view.shared

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItem
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.admin.ClassAdapter
import com.edubank.view.admin.SubjectAdapterAdmin
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import kotlin.random.Random

/**
 * Beinhaltet allgemeine UI-Tests für SubjectAccount.
 *
 * @see SubjectAccount
 * @author Team EduBank
 */
class SubjectAccountTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Admin ein und navigiert zum Fragment SubjectAccount von "Test Schüler" in "Test Fach" aus
     * "Test Klasse".
     */
    private fun setUpAdmin() { // login with admin
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()

        // select the test class
        onView(withId(R.id.recyclerClassView)).perform(
            actionOnItem<ClassAdapter.ClassViewHolder>(
                hasDescendant(withText(TestData.testClass)), click()
            )
        )

        waitForServer()

        // select the test subject
        onView(withId(R.id.recyclerSubjectView)).perform(
            actionOnItem<SubjectAdapterAdmin.SubjectViewHolder>(
                hasDescendant(withText(TestData.testSubject)), click()
            )
        )

        waitForServer()

        // select the test student
        onView(withId(R.id.rv_subject_students)).perform(
            actionOnItem<AccountAdapter.ViewHolder>(
                hasDescendant(withText(TestData.studentName)), click()
            )
        )

        waitForServer()
    }

    /**
     * Prüft, ob das Fragment für einen Admin richtig angezeigt wird.
     */
    @Test
    fun test_admin_fragmentIsDisplayedCorrectly() {
        setUpAdmin()

        onView(withId(R.id.subjectName)).check(matches(isDisplayed()))
        onView(withId(R.id.headerRoleName)).check(
            matches(withText("${TestData.testClass} - ${TestData.studentName} (${TestData.studentUsername})"))
        )
        onView(withId(R.id.balanceCaption)).check(matches(isDisplayed()))
        onView(withId(R.id.transactionButton)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerTransactionView)).check(matches(isDisplayed()))

    }

    /**
     * Prüft, ob ein Admin eine Transaktion auf einem bestimmten Konto durchführen kann.
     */
    @Test
    fun test_admin_performTransaction() {
        setUpAdmin()
        val transactionID = "T${Random.Default.nextInt(1000000, 9999999)}"
        val amount = Random.Default.nextInt(100)

        // make transaction
        onView(withId(R.id.transactionButton)).perform(click())
        onView(withId(R.id.transactionName)).perform(replaceText(transactionID))
        onView(withId(R.id.transactionAmount)).perform(replaceText("$amount"))
        onView(withId(R.id.commitButton)).perform(click())

        waitForServer()

        // check if transaction has been added
        onView(allOf(withId(R.id.transactionTitle), withText(transactionID))).check(
            matches(
                isDisplayed()
            )
        )
    }

    /**
     * Prüft, ob ein Admin eine Einzahlung auf einem bestimmten Konto durchführen kann.
     */
    @Test
    fun test_admin_performDeposit() {
        setUpAdmin()
        val depositID = "D${Random.Default.nextInt(1000000, 9999999)}"
        val amount = Random.Default.nextInt(100)

        // make deposit
        onView(withId(R.id.transactionButton)).perform(click())
        onView(withId(R.id.transactionName)).perform(replaceText(depositID))
        onView(withId(R.id.transactionAmount)).perform(replaceText("$amount"))
        onView(withId(R.id.cb_transaction_deposit)).perform(click())
        onView(withId(R.id.commitButton)).perform(click())

        waitForServer()

        // check if deposit has been added
        onView(allOf(withId(R.id.transactionTitle), withText(depositID))).check(
            matches(
                isDisplayed()
            )
        )
        onView(allOf(withId(R.id.saldo), withText("$amount.0€"))).check(
            (matches(
                isDisplayed()
            ))
        )
    }
}