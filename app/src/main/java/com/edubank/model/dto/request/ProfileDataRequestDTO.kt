package com.edubank.model.dto.request

/**
 * ProfileDataRequestDTO ist eine Daten-Klasse für Profil-Daten.
 * Die Klasse kann für REST-Anfragen genutzt werden.
 *
 * @property password Passwort des Benutzers
 * @property email Email-Adresse des Benutzers
 */
data class ProfileDataRequestDTO(
    val password : String,
    val email : String
)
