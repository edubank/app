package com.edubank.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edubank.model.dto.request.ProfileDataRequestDTO
import com.edubank.model.dto.request.UserRequestDTO
import com.edubank.model.dto.response.UserResponseDTO
import com.edubank.model.retrofitclient.ClassService
import com.edubank.model.retrofitclient.ServiceBuilder
import com.edubank.model.retrofitclient.UserService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

/**
 * Das ViewModel liefert die Funktionalität, die für die Verwaltung eines Benutzers gebraucht werden.
 * Dazu gehört die Kommunkation mit dem Server, um Benutzer anzufragen oder zu bearbeiten.
 *
 * @author Team EduBank
 * @property service Dem ViewModel kann ein UserService übergeben werden, wenn ein spezieller Service gebraucht wird (Andernfalls wird der Default-Service verwendet).
 * @property users Die Read-Only Benutzerdaten können durch users gelesen werden.
 * @see UserResponseDTO Die Benutzerdaten des ViewModels werden in einer Liste vom Typ UserResponseDTO gehalten.
 * @see ClassService Retrofit-Service, der für die Kommunikation mit dem Server verwendet wird.
 */
class UserViewModel : ViewModel() {

    /**
     * Daten-Container für die Benutzer-Daten. Nur _users kann modifiziert werden.
     * users macht die Benutzer-Daten nach außen sichtbar und enthält die gleichen Daten wie _users.
     */
    private var _users : MutableLiveData<List<UserResponseDTO>> = MutableLiveData<List<UserResponseDTO>>()
    var users : LiveData<List<UserResponseDTO>> = _users
    /**
     * Retrofit-Service für Benutzer-Anfragen.
     */
    private var requestService = ServiceBuilder.buildService(UserService::class.java)

    /**
     * Die setService-Funktion setzt den User-Service. Wenn setService nicht aufgerufen wird,
     * dann wird der Default-Service gesetzt. Sonst service.
     * Das Initialisieren mit einem spezifischen Service wird für die Tests benötigt.
     */
    fun setService(service : UserService){
        requestService = service
    }

    /**
     * Die Funktion getAllUsers frägt alle Benutzer beim Server an.
     * Die Benutzerdaten werden in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getAllUsers() : Int {
        var responseCode = 0
        val call = requestService.getAllUsers()
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = response.body()
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:getAllUser", response.message())
        } catch (error : Throwable) {
            _users.value= mutableListOf()
            error.message?.let { Log.e("UserVM:getAllUser", it) }
            error.printStackTrace()
        }
        return responseCode
    }


    /**
     * Die Funktion getMyUsers frägt die Benutzerdaten des aktuell angemeldeten Nutzers beim Server an.
     * Die Benutzerdaten werden in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getMyUser() : Int {
        var responseCode = 0
        val call = requestService.getMyUser()
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = listOfNotNull(response.body())
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:getMyUser", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:getMyUser", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion updateSettings aktualisert die Benutzerdaten des aktuell angemeldeten Nutzers auf dem Server an.
     * Wenn die email oder password nicht neu gesetzt werden soll, kann ein leerer String übergeben werden,
     * sodass nur ein Wert gesetzt wird.
     * Die aktualiserten Benutzerdaten werden in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param email Neue Email-Adresse des Nutzers.
     * @param password Neues Passwort des Nutzers.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun updateSettings(email : String, password : String) : Int {
        var responseCode = 0
        val call = requestService.updateSettings(ProfileDataRequestDTO(password, email))
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = listOfNotNull(response.body())
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:updateSettings", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:updateSettings", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getUserById frägt die Benutzerdaten eines Nutzers an.
     * Die Benutzerdaten werden in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param userId Id des Benutzers.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getUserById(userId : Int) : Int {
        var responseCode = 0
        val call = requestService.getUserById(userId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = listOfNotNull(response.body())
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:getUserById", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:getUserById", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getUserByRole frägt die Benutzerdaten eines bestimmten Benutzertyps an.
     * Die Benutzerdaten werden in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param role Benutzertyp der angefragten Benutzer.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getUserByRole(role : String) : Int {
        var responseCode = 0
        val call = requestService.getUserByRole(role)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = response.body()
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:getUserByRole", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:getUserByRole", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion getUserBySubject frägt die Benutzerdaten eines bestimmten Benutzertyps an.
     * Die Benutzerdaten werden in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param subjectId Id des Faches, dessen Schüler angefragt werden sollen.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun getUserBySubject(subjectId : Int) : Int {
        var responseCode = 0
        val call = requestService.getUserBySubjectId(subjectId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = response.body()
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:getUserBySubject", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:getUserBySubject", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion createUser erstellt eine Benutzer auf dem Server.
     * Der neue Benutzer wird in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param user Benutzer, der angelegt werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     * @see UserRequestDTO Datenobjekt für die REST-Anfrage.
     */
    suspend fun createUser(user : UserRequestDTO) : Int {
        var responseCode = 0
        val call = requestService.createUser(user)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = listOfNotNull(response.body())
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:createUser", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:createUser", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion updateUser aktualisert die Benutzerdaten eines Benutzers auf dem Server.
     * Der aktualiserte Benutzer wird in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param user Benutzer, der aktualisert werden soll.
     * @param userId Benutzer-ID des Benutzers, der aktualisert werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     * @see UserRequestDTO Datenobjekt für die REST-Anfrage.
     */
    suspend fun updateUser(user : UserRequestDTO, userId : Int) : Int {
        var responseCode = 0
        val call = requestService.updateUser(userId, user)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = listOfNotNull(response.body())
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:updateUser", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:updateUser", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion deleteUser löscht die Benutzerdaten eines Benutzers auf dem Server.
     *
     * @param userId Benutzer-ID des Benutzers, der gelöscht werden soll.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun deleteUser(userId : Int) : Int {
        var responseCode = 0
        val call = requestService.deleteUser(userId)
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            Log.d("UserVM:deleteUser", response.message())
        } catch (error : Throwable) {
            error.message?.let { Log.e("UserVM:deleteUser", it) }
            error.printStackTrace()
        }
        return responseCode
    }

    /**
     * Die Funktion setInitialData setzt die Nutzereinstellungen der aktuell angemeldeten Nutzers initial auf dem Server.
     * Wenn die email oder password nicht neu gesetzt werden soll, kann ein leerer String übergeben werden,
     * sodass nur ein Wert gesetzt wird.
     * Der aktualiserte Benutzer wird in users gespeichert, wenn die Anfrage erfolgreich war,
     * sonst enthält users eine leere Liste.
     *
     * @param email Email-Adresse des Benutzers.
     * @param password Neues Passwort des Benutzers.
     * @return Den Http-Response-Code, wenn Server auf die Anfrage antwortet, sonst 0.
     */
    suspend fun setInitialData(email : String, password : String) : Int {
        var responseCode = 0
        val call = requestService.setInitialData(ProfileDataRequestDTO(password, email))
        val asyncResponse = viewModelScope.async(Dispatchers.IO) { call.execute() }
        try {
            val response = asyncResponse.await()
            responseCode = response.code()
            if (response.isSuccessful) {
                _users.value = listOfNotNull(response.body())
            } else {
                _users.value = mutableListOf()
            }
            Log.d("UserVM:setInitialData", response.message())
        } catch (error : Throwable) {
            _users.value = mutableListOf()
            error.message?.let { Log.e("UserVM:setInitialData", it) }
            error.printStackTrace()
        }
        return responseCode
    }
}