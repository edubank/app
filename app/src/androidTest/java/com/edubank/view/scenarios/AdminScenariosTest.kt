package com.edubank.view.scenarios

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.edubank.R
import com.edubank.view.TestData
import com.edubank.view.TestData.Companion.waitForServer
import com.edubank.view.admin.ClassAdapter
import com.edubank.view.shared.MainActivity
import org.hamcrest.Matchers.allOf
import org.junit.*

/**
 * Beinhaltet Tests zu den Lehrer-Szenarien aus dem Pflichtenheft im Abschnitt 8.2.
 *
 * @author Team EduBank
 */
class AdminScenariosTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    /**
     * Loggt sich mit einem Administrator ein.
     */
    @Before
    fun setUp() {
        onView(withId(R.id.txt_login_username)).perform(replaceText(TestData.adminUsername))
        onView(withId(R.id.txt_login_password)).perform(replaceText(TestData.adminPassword))
        onView(withId(R.id.btn_login_login)).perform(click())

        waitForServer()
    }

    /**
     * Prüft, ob das Szenario "Neue Klasse hinzufügen" funktioniert.
     */
    @Test
    fun test_createAndEditClass() { // navigate to the class manager
        onView(withId(R.id.classManagerButton)).perform(click())

        waitForServer()

        // create new class
        onView(withId(R.id.newClassEdit)).perform(replaceText(TestData.deleteClass))
        onView(withId(R.id.addBtn)).perform(click())

        // check if class has been created
        onView(allOf(withId(R.id.classItem), hasDescendant(withText(TestData.deleteClass)))).check(
            matches(isDisplayed())
        )

        // select created class and change name
        onView(allOf(withId(R.id.classItem), hasDescendant(withText(TestData.deleteClass)))).perform(click())
        onView(withId(R.id.updateClassEdit)).perform(replaceText(TestData.changedClass))
        onView(withId(R.id.changeBtn)).perform(click())

        // check if name has been changed
        onView(allOf(withId(R.id.classItem), hasDescendant(withText(TestData.changedClass)))).check(
            matches(isDisplayed())
        )

        // delete created class
        onView(withId(R.id.recyclerClassView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ClassAdapter.ClassViewHolder>(
                0, click()
            )
        )
        onView(withId(R.id.deleteBtn)).perform(click())
        onView(withId(android.R.id.button1)).perform(click())

        // check if class has been deleted
        onView(allOf(withId(R.id.classItem), hasDescendant(withText(TestData.deleteClass)))).check(
            doesNotExist()
        )

        // logout
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().targetContext)
        onView(withText("Ausloggen")).perform(click())

        // check for successful logout
        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
    }

    @Test
    fun test_createAndEditStudent() { // navigate to the user manager
        onView(withId(R.id.userManagerButton)).perform(click())

        // create new student
        onView(withId(R.id.openUserManagerBtn)).perform(click())
        onView(withId(R.id.firstNameEdit)).perform(replaceText(TestData.deleteStudentFirstName))
        onView(withId(R.id.lastNameEdit)).perform(replaceText(TestData.deleteStudentLastName))
        onView(withId(R.id.addBtn)).perform(click())

        waitForServer()

        // select created student
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ClassAdapter.ClassViewHolder>(
                0, click()
            )
        )

        // check it is the correct user
        onView(withId(R.id.firstNameEdit)).check(matches(withText(TestData.deleteStudentFirstName)))
        onView(withId(R.id.lastNameEdit)).check(matches(withText(TestData.deleteStudentLastName)))

        // change last name
        onView(withId(R.id.lastNameEdit)).perform(replaceText(TestData.deleteStudentLastNameChanged))
        onView(withId(R.id.saveChangesBtn)).perform(click())

        // select changed student
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ClassAdapter.ClassViewHolder>(
                0, click()
            )
        )

        // check it is the correct user
        onView(withId(R.id.firstNameEdit)).check(matches(withText(TestData.deleteStudentFirstName)))
        onView(withId(R.id.lastNameEdit)).check(matches(withText(TestData.deleteStudentLastNameChanged)))

        // delete student
        onView(withId(R.id.deleteUserBtn)).perform(click())
        onView(withId(android.R.id.button1)).perform(click())

        // check user has been deleted
        onView(withId(R.id.recyclerUserView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ClassAdapter.ClassViewHolder>(
                0, click()
            )
        )
        onView(allOf(withId(R.id.firstNameEdit), withText(TestData.deleteStudentFirstName))).check(doesNotExist())
        onView(allOf(withId(R.id.lastNameEdit), withText(TestData.deleteStudentLastNameChanged))).check(doesNotExist())
    }
}